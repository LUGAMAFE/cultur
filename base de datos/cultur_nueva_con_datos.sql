-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 17-04-2020 a las 19:56:00
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cultur`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

DROP TABLE IF EXISTS `archivos`;
CREATE TABLE IF NOT EXISTS `archivos` (
  `id_file` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `folder_file` varchar(300) NOT NULL,
  `uuid_file` varchar(100) NOT NULL,
  `name_file` varchar(150) NOT NULL,
  `ext_file` varchar(10) NOT NULL,
  `dir_file` varchar(200) NOT NULL,
  `full_route_file` varchar(400) NOT NULL,
  `fecha_creacion_file` datetime NOT NULL,
  `fecha_modificacion_file` datetime NOT NULL,
  PRIMARY KEY (`id_file`),
  UNIQUE KEY `uuid_file_unique` (`uuid_file`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `archivos`
--

INSERT INTO `archivos` (`id_file`, `folder_file`, `uuid_file`, `name_file`, `ext_file`, `dir_file`, `full_route_file`, `fecha_creacion_file`, `fecha_modificacion_file`) VALUES
(1, 'assets/img/Cines Siglo XXI/Peliculas', 'ce4091d5-ff59-4cd2-af28-f274dca69709', 'jojo', 'jpg', 'assets/img/Cines Siglo XXI/Peliculas/ce4091d5-ff59-4cd2-af28-f274dca69709/', 'assets/img/Cines Siglo XXI/Peliculas/ce4091d5-ff59-4cd2-af28-f274dca69709/jojo.jpg', '2020-03-20 16:58:33', '2020-03-20 16:58:33'),
(2, 'assets/img/Cines Siglo XXI/Peliculas', 'f57b199e-cee2-403b-9a1b-7938948a8745', 'frozen2', 'jpeg', 'assets/img/Cines Siglo XXI/Peliculas/f57b199e-cee2-403b-9a1b-7938948a8745/', 'assets/img/Cines Siglo XXI/Peliculas/f57b199e-cee2-403b-9a1b-7938948a8745/frozen2.jpeg', '2020-03-20 16:59:13', '2020-03-20 16:59:13'),
(3, 'assets/img/Cines Siglo XXI/Peliculas', 'c365a0d7-cb1c-49c6-913a-670199f26ff7', 'mujercitas', 'jpg', 'assets/img/Cines Siglo XXI/Peliculas/c365a0d7-cb1c-49c6-913a-670199f26ff7/', 'assets/img/Cines Siglo XXI/Peliculas/c365a0d7-cb1c-49c6-913a-670199f26ff7/mujercitas.jpg', '2020-03-20 17:41:33', '2020-03-20 17:41:33'),
(4, 'assets/img/Cines Siglo XXI/Peliculas', 'c22e4a9b-d197-4c54-86ef-c81a00928e54', 'cindy', 'jpg', 'assets/img/Cines Siglo XXI/Peliculas/c22e4a9b-d197-4c54-86ef-c81a00928e54/', 'assets/img/Cines Siglo XXI/Peliculas/c22e4a9b-d197-4c54-86ef-c81a00928e54/cindy.jpg', '2020-03-20 17:42:00', '2020-03-20 17:42:00'),
(5, 'assets/img/Cines Siglo XXI/Peliculas', '1c9635b3-327e-40b5-a013-046f7129b0c7', '61id7dm4b6L._AC_SY741_', 'jpg', 'assets/img/Cines Siglo XXI/Peliculas/1c9635b3-327e-40b5-a013-046f7129b0c7/', 'assets/img/Cines Siglo XXI/Peliculas/1c9635b3-327e-40b5-a013-046f7129b0c7/61id7dm4b6L._AC_SY741_.jpg', '2020-04-15 15:32:56', '2020-04-15 15:32:56');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `archivos_vista`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `archivos_vista`;
CREATE TABLE IF NOT EXISTS `archivos_vista` (
`id_file` int(255) unsigned
,`archivo_original` varchar(361)
,`archivo_large` varchar(369)
,`archivo_medium` varchar(370)
,`archivo_small` varchar(369)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funciones_page_data`
--

DROP TABLE IF EXISTS `funciones_page_data`;
CREATE TABLE IF NOT EXISTS `funciones_page_data` (
  `id_funcion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pelicula-asociada-funcion` int(255) UNSIGNED NOT NULL,
  `sala-asociada-funcion` int(255) UNSIGNED NOT NULL,
  `idioma-funcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_funcion`),
  KEY `pelicula-asociada-funcion` (`pelicula-asociada-funcion`),
  KEY `sala-asociada-funcion` (`sala-asociada-funcion`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `funciones_page_data`
--

INSERT INTO `funciones_page_data` (`id_funcion`, `pelicula-asociada-funcion`, `sala-asociada-funcion`, `idioma-funcion`) VALUES
(5, 3, 2, 'Español'),
(6, 3, 3, 'Subtitulada'),
(7, 4, 2, 'Español'),
(10, 1, 1, 'Inglés'),
(12, 2, 1, 'Español'),
(14, 3, 3, 'Inglés'),
(17, 4, 3, 'Subtitulada'),
(18, 5, 2, 'Inglés'),
(19, 5, 1, 'Español'),
(20, 5, 2, 'Español'),
(21, 1, 3, 'Subtitulada'),
(22, 1, 3, 'Español');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funciones_page_fechas`
--

DROP TABLE IF EXISTS `funciones_page_fechas`;
CREATE TABLE IF NOT EXISTS `funciones_page_fechas` (
  `id_fecha_funcion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_funcion_assoc` int(255) UNSIGNED NOT NULL,
  `fecha_funcion` date NOT NULL,
  PRIMARY KEY (`id_fecha_funcion`),
  KEY `id_funcion_assoc` (`id_funcion_assoc`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `funciones_page_fechas`
--

INSERT INTO `funciones_page_fechas` (`id_fecha_funcion`, `id_funcion_assoc`, `fecha_funcion`) VALUES
(6, 7, '2020-04-22'),
(7, 7, '2020-04-24'),
(21, 10, '2020-04-26'),
(22, 10, '2020-04-27'),
(38, 12, '2020-04-26'),
(47, 5, '2020-04-20'),
(48, 5, '2020-04-21'),
(49, 5, '2020-04-22'),
(50, 6, '2020-04-20'),
(55, 14, '2020-05-08'),
(56, 14, '2020-04-15'),
(69, 17, '2020-04-16'),
(70, 17, '2020-04-17'),
(71, 17, '2020-04-15'),
(72, 18, '2020-04-15'),
(73, 18, '2020-04-16'),
(74, 18, '2020-04-17'),
(75, 18, '2020-04-18'),
(76, 19, '2020-04-15'),
(77, 19, '2020-04-16'),
(78, 19, '2020-04-17'),
(79, 19, '2020-04-18'),
(84, 20, '2020-04-16'),
(85, 20, '2020-04-17'),
(86, 20, '2020-04-18'),
(87, 20, '2020-04-15'),
(88, 21, '2020-04-15'),
(89, 21, '2020-04-16'),
(90, 21, '2020-04-17'),
(91, 21, '2020-04-18'),
(92, 22, '2020-04-15'),
(93, 22, '2020-04-16'),
(94, 22, '2020-04-17'),
(95, 22, '2020-04-18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funciones_page_horas`
--

DROP TABLE IF EXISTS `funciones_page_horas`;
CREATE TABLE IF NOT EXISTS `funciones_page_horas` (
  `id_hora_funcion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_funcion_assoc` int(255) UNSIGNED NOT NULL,
  `hora_funcion` time NOT NULL,
  PRIMARY KEY (`id_hora_funcion`),
  KEY `id_funcion_assoc` (`id_funcion_assoc`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `funciones_page_horas`
--

INSERT INTO `funciones_page_horas` (`id_hora_funcion`, `id_funcion_assoc`, `hora_funcion`) VALUES
(4, 7, '23:30:00'),
(15, 10, '12:30:00'),
(16, 10, '17:00:00'),
(17, 10, '22:00:00'),
(26, 12, '00:07:00'),
(30, 5, '00:00:00'),
(31, 5, '20:00:00'),
(32, 6, '01:00:00'),
(33, 6, '23:30:00'),
(43, 14, '13:49:00'),
(44, 14, '22:49:00'),
(45, 14, '18:49:00'),
(54, 17, '11:05:00'),
(55, 18, '12:31:00'),
(56, 18, '09:05:00'),
(57, 19, '00:09:00'),
(58, 19, '21:09:00'),
(60, 20, '18:01:00'),
(61, 20, '21:01:00'),
(62, 21, '18:06:00'),
(63, 22, '04:07:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas_page_data`
--

DROP TABLE IF EXISTS `peliculas_page_data`;
CREATE TABLE IF NOT EXISTS `peliculas_page_data` (
  `id_pelicula` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_pelicula` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duracion_pelicula` time NOT NULL,
  `sinopsis_pelicula` varchar(600) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trailer_pelicula` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_file_img_pelicula` int(255) UNSIGNED NOT NULL,
  `clasificacion_pelicula` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_pelicula`),
  KEY `id_file_img_pelicula` (`id_file_img_pelicula`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `peliculas_page_data`
--

INSERT INTO `peliculas_page_data` (`id_pelicula`, `nombre_pelicula`, `duracion_pelicula`, `sinopsis_pelicula`, `trailer_pelicula`, `id_file_img_pelicula`, `clasificacion_pelicula`) VALUES
(1, 'Jojo Rabbit', '04:02:00', 'Durante la Segunda Guerra Mundial, un niño que pertenece a las Juventudes Hitlerianas descubre que su madre está ocultando en el ático de su casa a una niña judía. La concepción del mundo que tienen el pequeño y su amigo imaginario, Hitler, cambia por completo con la irrupción en sus vidas de la joven hebrea.', 'https://www.youtube.com/watch?v=tL4McUzXfFI', 1, 'AA'),
(2, 'Frozen 2', '01:30:00', 'Elsa tiene un poder extraordinario: es capaz de crear hielo y nieve. Sin embargo, a pesar de lo feliz que la hacen los habitantes de Arendelle, siente que no encaja allá. Después de oír una voz misteriosa, Elsa, acompañada por Anna, Kristoff, Olaf y Sven, viaja a los bosques embrujados y los mares oscuros que se extienden más allá de los límites de su reino para descubrir quién es en realidad y por qué posee un poder tan asombroso.', 'https://www.youtube.com/watch?v=j8bLqFQ2sQI', 2, 'A'),
(3, 'Mujercitas', '02:00:00', 'Amy, Jo, Beth y Meg son cuatro hermanas que atraviesan Massachussets con su madre durante la Guerra Civil, unas vacaciones que realizan sin su padre evangelista itinerante. Durante estas vacaciones las adolescentes descubren el amor y la importancia de los lazos familiares.', 'https://www.youtube.com/watch?v=BKuDz3pxi7I', 3, 'B15'),
(4, 'Cindy la Regia', '01:20:00', 'En el último momento, Cindy decide que no quiere casarse con su novio y escapa a la Ciudad de México, donde nuevas amistades y caminos inesperados le enseñan que su talento le ofrece muchas más posibilidades en la vida de lo que ella pensaba.', 'https://www.youtube.com/watch?v=XCDRPXXTzHw', 4, 'B15'),
(5, 'Terminator', '02:08:00', 'Los Ángeles, año 2029. Las máquinas dominan el mundo. Los rebeldes que luchan contra ellas tienen como líder a John Connor, un hombre que nació en los años ochenta. Para acabar con la rebelión, las máquinas deciden enviar al pasado a un robot -Terminator- cuya misión será eliminar a Sarah Connor, la madre de John, e impedir así su nacimiento.', 'https://www.youtube.com/watch?v=oxy8udgWRmo', 5, 'B15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones_page_data`
--

DROP TABLE IF EXISTS `promociones_page_data`;
CREATE TABLE IF NOT EXISTS `promociones_page_data` (
  `id_promocion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_file_img_promocion` int(255) UNSIGNED NOT NULL,
  `json_input_extras` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_activo` int(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_promocion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones_page_info`
--

DROP TABLE IF EXISTS `promociones_page_info`;
CREATE TABLE IF NOT EXISTS `promociones_page_info` (
  `id_promocion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_activo` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_promocion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salas_page_data`
--

DROP TABLE IF EXISTS `salas_page_data`;
CREATE TABLE IF NOT EXISTS `salas_page_data` (
  `id_sala` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_sala` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_asientos_totales_sala` int(255) NOT NULL,
  `numero_asientos_disponibles_sala` int(255) NOT NULL,
  PRIMARY KEY (`id_sala`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `salas_page_data`
--

INSERT INTO `salas_page_data` (`id_sala`, `nombre_sala`, `numero_asientos_totales_sala`, `numero_asientos_disponibles_sala`) VALUES
(1, 'Sala 1', 35, 35),
(2, 'Sala 2', 25, 25),
(3, 'LEEEl', 6, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contrasena_usuario` varchar(300) NOT NULL,
  `nombre_usuario` varchar(300) NOT NULL,
  `correo_usuario` varchar(300) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `unique_nombre_usuario` (`nombre_usuario`),
  UNIQUE KEY `unique_correo_usuario` (`correo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `contrasena_usuario`, `nombre_usuario`, `correo_usuario`) VALUES
(1, '$2y$10$D51PdEvXOob5k/2Hye84QeDq7d1ZdXpXLbBonSfvx.hQ4BbdrojEu', 'lugamafe', 'luisjavier004@hotmail.com'),
(2, '$2y$10$xS00zgXpmUX13EGvRcE7F.Q/96uSQEmtynJGw9mKYxOXF9C.RFws2', 'Admin', '');

-- --------------------------------------------------------

--
-- Estructura para la vista `archivos_vista`
--
DROP TABLE IF EXISTS `archivos_vista`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `archivos_vista`  AS  select `archivos`.`id_file` AS `id_file`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),'',concat('.',`archivos`.`ext_file`))) AS `archivo_original`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (large)',concat(' (large).',`archivos`.`ext_file`))) AS `archivo_large`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (medium)',concat(' (medium).',`archivos`.`ext_file`))) AS `archivo_medium`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (small)',concat(' (small).',`archivos`.`ext_file`))) AS `archivo_small` from `archivos` ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `funciones_page_data`
--
ALTER TABLE `funciones_page_data`
  ADD CONSTRAINT `funciones_page_data_ibfk_1` FOREIGN KEY (`pelicula-asociada-funcion`) REFERENCES `peliculas_page_data` (`id_pelicula`),
  ADD CONSTRAINT `funciones_page_data_ibfk_2` FOREIGN KEY (`sala-asociada-funcion`) REFERENCES `salas_page_data` (`id_sala`);

--
-- Filtros para la tabla `funciones_page_fechas`
--
ALTER TABLE `funciones_page_fechas`
  ADD CONSTRAINT `funciones_page_fechas_ibfk_1` FOREIGN KEY (`id_funcion_assoc`) REFERENCES `funciones_page_data` (`id_funcion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `funciones_page_horas`
--
ALTER TABLE `funciones_page_horas`
  ADD CONSTRAINT `funciones_page_horas_ibfk_1` FOREIGN KEY (`id_funcion_assoc`) REFERENCES `funciones_page_data` (`id_funcion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `peliculas_page_data`
--
ALTER TABLE `peliculas_page_data`
  ADD CONSTRAINT `peliculas_page_data_ibfk_1` FOREIGN KEY (`id_file_img_pelicula`) REFERENCES `archivos` (`id_file`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
