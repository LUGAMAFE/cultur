-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 17-04-2020 a las 19:57:44
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cultur`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

DROP TABLE IF EXISTS `archivos`;
CREATE TABLE IF NOT EXISTS `archivos` (
  `id_file` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `folder_file` varchar(300) NOT NULL,
  `uuid_file` varchar(100) NOT NULL,
  `name_file` varchar(150) NOT NULL,
  `ext_file` varchar(10) NOT NULL,
  `dir_file` varchar(200) NOT NULL,
  `full_route_file` varchar(400) NOT NULL,
  `fecha_creacion_file` datetime NOT NULL,
  `fecha_modificacion_file` datetime NOT NULL,
  PRIMARY KEY (`id_file`),
  UNIQUE KEY `uuid_file_unique` (`uuid_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `archivos_vista`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `archivos_vista`;
CREATE TABLE IF NOT EXISTS `archivos_vista` (
`id_file` int(255) unsigned
,`archivo_original` varchar(361)
,`archivo_large` varchar(369)
,`archivo_medium` varchar(370)
,`archivo_small` varchar(369)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funciones_page_data`
--

DROP TABLE IF EXISTS `funciones_page_data`;
CREATE TABLE IF NOT EXISTS `funciones_page_data` (
  `id_funcion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pelicula-asociada-funcion` int(255) UNSIGNED NOT NULL,
  `sala-asociada-funcion` int(255) UNSIGNED NOT NULL,
  `idioma-funcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_funcion`),
  KEY `pelicula-asociada-funcion` (`pelicula-asociada-funcion`),
  KEY `sala-asociada-funcion` (`sala-asociada-funcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funciones_page_fechas`
--

DROP TABLE IF EXISTS `funciones_page_fechas`;
CREATE TABLE IF NOT EXISTS `funciones_page_fechas` (
  `id_fecha_funcion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_funcion_assoc` int(255) UNSIGNED NOT NULL,
  `fecha_funcion` date NOT NULL,
  PRIMARY KEY (`id_fecha_funcion`),
  KEY `id_funcion_assoc` (`id_funcion_assoc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funciones_page_horas`
--

DROP TABLE IF EXISTS `funciones_page_horas`;
CREATE TABLE IF NOT EXISTS `funciones_page_horas` (
  `id_hora_funcion` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_funcion_assoc` int(255) UNSIGNED NOT NULL,
  `hora_funcion` time NOT NULL,
  PRIMARY KEY (`id_hora_funcion`),
  KEY `id_funcion_assoc` (`id_funcion_assoc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas_page_data`
--

DROP TABLE IF EXISTS `peliculas_page_data`;
CREATE TABLE IF NOT EXISTS `peliculas_page_data` (
  `id_pelicula` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_pelicula` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duracion_pelicula` time NOT NULL,
  `sinopsis_pelicula` varchar(600) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trailer_pelicula` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_file_img_pelicula` int(255) UNSIGNED NOT NULL,
  `clasificacion_pelicula` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_pelicula`),
  KEY `id_file_img_pelicula` (`id_file_img_pelicula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones_page_data`
--

DROP TABLE IF EXISTS `promociones_page_data`;
CREATE TABLE IF NOT EXISTS `promociones_page_data` (
  `id_promocion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_file_img_promocion` int(255) UNSIGNED NOT NULL,
  `json_input_extras` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_activo` int(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_promocion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones_page_info`
--

DROP TABLE IF EXISTS `promociones_page_info`;
CREATE TABLE IF NOT EXISTS `promociones_page_info` (
  `id_promocion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_activo` tinyint(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_promocion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salas_page_data`
--

DROP TABLE IF EXISTS `salas_page_data`;
CREATE TABLE IF NOT EXISTS `salas_page_data` (
  `id_sala` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre_sala` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_asientos_totales_sala` int(255) NOT NULL,
  `numero_asientos_disponibles_sala` int(255) NOT NULL,
  PRIMARY KEY (`id_sala`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contrasena_usuario` varchar(300) NOT NULL,
  `nombre_usuario` varchar(300) NOT NULL,
  `correo_usuario` varchar(300) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `unique_nombre_usuario` (`nombre_usuario`),
  UNIQUE KEY `unique_correo_usuario` (`correo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `contrasena_usuario`, `nombre_usuario`, `correo_usuario`) VALUES
(1, '$2y$10$D51PdEvXOob5k/2Hye84QeDq7d1ZdXpXLbBonSfvx.hQ4BbdrojEu', 'lugamafe', 'luisjavier004@hotmail.com'),
(2, '$2y$10$xS00zgXpmUX13EGvRcE7F.Q/96uSQEmtynJGw9mKYxOXF9C.RFws2', 'Admin', '');

-- --------------------------------------------------------

--
-- Estructura para la vista `archivos_vista`
--
DROP TABLE IF EXISTS `archivos_vista`;

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `archivos_vista`  AS  select `archivos`.`id_file` AS `id_file`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),'',concat('.',`archivos`.`ext_file`))) AS `archivo_original`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (large)',concat(' (large).',`archivos`.`ext_file`))) AS `archivo_large`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (medium)',concat(' (medium).',`archivos`.`ext_file`))) AS `archivo_medium`,concat(`archivos`.`dir_file`,`archivos`.`name_file`,if((`archivos`.`ext_file` = ''),' (small)',concat(' (small).',`archivos`.`ext_file`))) AS `archivo_small` from `archivos` ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `funciones_page_data`
--
ALTER TABLE `funciones_page_data`
  ADD CONSTRAINT `funciones_page_data_ibfk_1` FOREIGN KEY (`pelicula-asociada-funcion`) REFERENCES `peliculas_page_data` (`id_pelicula`),
  ADD CONSTRAINT `funciones_page_data_ibfk_2` FOREIGN KEY (`sala-asociada-funcion`) REFERENCES `salas_page_data` (`id_sala`);

--
-- Filtros para la tabla `funciones_page_fechas`
--
ALTER TABLE `funciones_page_fechas`
  ADD CONSTRAINT `funciones_page_fechas_ibfk_1` FOREIGN KEY (`id_funcion_assoc`) REFERENCES `funciones_page_data` (`id_funcion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `funciones_page_horas`
--
ALTER TABLE `funciones_page_horas`
  ADD CONSTRAINT `funciones_page_horas_ibfk_1` FOREIGN KEY (`id_funcion_assoc`) REFERENCES `funciones_page_data` (`id_funcion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `peliculas_page_data`
--
ALTER TABLE `peliculas_page_data`
  ADD CONSTRAINT `peliculas_page_data_ibfk_1` FOREIGN KEY (`id_file_img_pelicula`) REFERENCES `archivos` (`id_file`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
