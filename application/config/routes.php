<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['cultur'] = 'home/cultur';
$route['centro-convenciones-sigloxxi'] = 'home/centroConvenciones';
$route['centro-convenciones-sigloxxi/salon/(:any)'] = 'home/salon/$1';
$route['cines-sigloxxi'] = 'home/cines';
$route['paradores'] = 'home/paradoresTuristicos';
$route['parador'] = 'home/parador';
$route['paradores-turisticos/(:any)'] = 'home/parador/$1';
$route['eventos'] = 'home/eventos';
$route['politicas-privacidad'] = 'home/politicas';
$route['cuenta-publica'] = 'home/cuenta';
$route['pelicula/(:any)/(:any)'] = 'home/pelicula/$1/$2';



//DASHBOARD ADMIN
$route['administracion/cines'] = 'adminCines/dashboard';

$route['administracion/cines/promociones'] = 'adminCines/promociones';
$route['administracion/cines/promociones/archivosEntrada'] = 'adminCines/promociones/archivosEntrada';
$route['administracion/cines/promociones/subirArchivos'] = 'adminCines/promociones/subirArchivos';
$route['administracion/cines/promociones/subirArchivos/(:any)'] = 'adminCines/promociones/subirArchivos';
$route['administracion/cines/promociones/modificar'] = 'adminCines/promociones/modificar';


//Peliculas
$route['administracion/cines/peliculas'] = "adminCines/peliculas";
$route['administracion/cines/peliculas/eliminar/(:any)'] = "adminCines/peliculas/eliminar/$1";

$route['administracion/cines/peliculas/crear'] = "adminCines/peliculas/crear";
$route['administracion/cines/peliculas/crear/formInfo'] = "adminCines/peliculas/crearEntrada";
$route['administracion/cines/peliculas/crear/archivosEntrada'] = "adminCines/peliculas/sinImagenes";
$route['administracion/cines/peliculas/crear/subirArchivos'] = "adminCines/peliculas/subirArchivos";
$route['administracion/cines/peliculas/crear/subirArchivos/(:any)'] = "adminCines/peliculas/subirArchivos";

$route['administracion/cines/peliculas/(:any)'] = "adminCines/peliculas/modificar/$1";
$route['administracion/cines/peliculas/(:any)/formInfo'] = "adminCines/peliculas/modificarEntrada/$1";
$route['administracion/cines/peliculas/(:any)/archivosEntrada'] = "adminCines/peliculas/archivosEntrada/$1";
$route['administracion/cines/peliculas/(:any)/subirArchivos'] = "adminCines/peliculas/subirArchivos";
$route['administracion/cines/peliculas/(:any)/subirArchivos/(:any)'] = "adminCines/peliculas/subirArchivos";

//Salas
$route['administracion/cines/salas'] = "adminCines/salas";
$route['administracion/cines/salas/eliminar/(:any)'] = "adminCines/salas/eliminar/$1";

$route['administracion/cines/salas/crear'] = "adminCines/salas/crear";
$route['administracion/cines/salas/crear/formInfo'] = "adminCines/salas/crearEntrada";

$route['administracion/cines/salas/(:any)'] = "adminCines/salas/modificar/$1";
$route['administracion/cines/salas/(:any)/formInfo'] = "adminCines/salas/modificarEntrada/$1";

//Funciones
$route['administracion/cines/funciones'] = "adminCines/funciones";
$route['administracion/cines/funciones/eliminar/(:any)'] = "adminCines/funciones/eliminar/$1";

$route['administracion/cines/funciones/crear'] = "adminCines/funciones/crear";
$route['administracion/cines/funciones/crear/formInfo'] = "adminCines/funciones/crearEntrada";

$route['administracion/cines/funciones/(:any)'] = "adminCines/funciones/modificar/$1";
$route['administracion/cines/funciones/(:any)/formInfo'] = "adminCines/funciones/modificarEntrada/$1";




