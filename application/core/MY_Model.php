<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Representa a una tabla de la base de datos y las acciones permitidas.
 * @property string $table Nombre de la tabla sobre la cual se realizan acciones.
 * @property string $table_prefix Especifica el prefijo de la base de datos en los queries SQL.
 * @property string $llave_primaria Atributo de la base de datos que funciona como llave primaria.
 * @property array  $atributosMetadata arreglo con todos los atributos de los campos de una tabla.
 */
class MY_Model extends CI_Model{

    public $table;
    private $table_prefix; 
    public $llave_primaria;
    private $atributosMetadata;
    private $lastQuery;
    private $expectedResultType = "array";

    const STR_BD_TB = 'information_schema.COLUMNS';

    /**
     * Constructor que determina el nombre de la tabla de la tabla a utilizar para las consultas.
     * Tambien obtiene informacion importante de la tabla como su llave primaria al igual que metadatos de la tabla.
     * @param string $table nombre de la tabla a utilizar
     */
    public function __construct(string $table) {
        parent::__construct();
        $this->load->database();
        $this->table = $table;
        $this->llave_primaria = $this->get_llave_primaria();
        $this->atributosMetadata = $this->get_atributos_BD();
    }

    /**
     * Método para obtener la llave primaria de la tabla.
     * @return string cadena con el nombre de la llave primaria
     */
    public function get_llave_primaria(): string{
        $database = $this->db->database;
        $strSQL = 'SELECT ' . self::STR_BD_TB . '.COLUMN_NAME' .
                    ' FROM '. self::STR_BD_TB .
                    ' WHERE ' . self::STR_BD_TB . '.COLUMN_KEY = \'PRI\' AND ' .
                        self::STR_BD_TB . ".TABLE_NAME = '$this->table' AND ".
                        self::STR_BD_TB . '.TABLE_SCHEMA = \'' . $database."'";
        //var_dump($strSQL);
        $this->query($strSQL);
        $result = $this->lastQuery->row_array();
        return (string) $result['COLUMN_NAME'];
    }

    /**
    * Método para recuperar los atributos y su información para construir un
    * formulario genérico.
    * @return array $result array asociativo de longitud variable.
    */    
    public function get_atributos_BD(): array{
        $database = $this->db->database;
        $strSQL = 'SELECT ' . self::STR_BD_TB . '.COLUMN_NAME,' .
                    self::STR_BD_TB . '.COLUMN_KEY,' . 
                    self::STR_BD_TB . '.IS_NULLABLE,' . 
                    self::STR_BD_TB . '.DATA_TYPE,' .
                    self::STR_BD_TB . '.NUMERIC_PRECISION,' .
                    self::STR_BD_TB . '.COLUMN_COMMENT,' .
                    self::STR_BD_TB . '.COLUMN_DEFAULT,' .
                    self::STR_BD_TB . '.CHARACTER_MAXIMUM_LENGTH' .

                    ' FROM ' .self::STR_BD_TB .
                        ' WHERE ' .self::STR_BD_TB . ".TABLE_NAME = '$this->table' AND ".
                        self::STR_BD_TB . ".TABLE_SCHEMA = '$database'";
        //var_dump($strSQL);
        return $this->query($strSQL);
    }

    /**
     * Otiene el nombre de columna de la tabla que identifica la llave primaria.
     * @return string nombre de la columna de llave primaria.
     */
    public function getLlavePrimaria(): string {
        return $this->llave_primaria;
    }

    /**
     * Cambia el tipo de forma en que se devolveran los resultados de las consultas
     * "object" - si quieres que los resultados sean un array de objetos o un objeto
     * "array" - si quieres que los resultados sean un array de arrays o un array.
     * @param string $type el tipo de resultado esperados.
     * @return boolean determina si el cambio de tipo fue exitoso o no.
     */
    public function changeExectedResultType(string $type): bool{
        $type = strtolower($type);
        switch ($type) {
            case 'array':
            case 'object':
                $this->expectedResultType = $type;
                return TRUE;
                break;
            default:
                return FALSE;
                break;
        }
    }

    /**
     * Este método devuelve el resultado de una consulta verificando el tipo de resultado que espera el usuario.
     * En caso de opcion "array" - Devuelve el resultado como un array de objetos o un array vacío en caso de fallo.
     * En caso de opcion "object" - Devuelve el resultado como un array de arrays o un array vacío en caso de fallo.
     *
     * @param CI_DB_mysqli_result $result consulta a ejecutar su resultado.
     * @return array resultado de la consulta como un array.
     */
    public function result(CI_DB_mysqli_result $result): array{
        switch ($this->expectedResultType) {
            case 'array':
                return $result->result_array();
                break;
            case 'object':
                return $result->result();
                break;
        }
    }

    /**
     * Este método devuelve una única fila de resultados. Si su consulta tiene más de una fila, devuelve solo la primera fila. 
     * El resultado se devuelve como un objeto o array. Dependiendo de la verificacion del tipo de resultado que espera el usuario.
     * En caso de opcion "array" - Devuelve el resultado como un array o nulo en caso de no encontrarse un resultado.
     * En caso de opcion "object" - Devuelve el resultado como un objeto o nulo en caso de no encontrarse un resultado.
     *
     * @param CI_DB_mysqli_result $result consulta a ejecutar su resultado de una solo fila.
     * @param integer $numRow Indice de la fila de resultados de la consulta que se devolverá.
     * @return mixed resultado de la consulta como un array u objeto, nulo en caso de no tener resultado.
     */
    public function row(CI_DB_mysqli_result $result, int $numRow = 0){
        switch ($this->expectedResultType) {
            case 'array':
                return $result->row_array();
                break;
            case 'object':
                return $result->row();
                break;
        }
    }

    /**
     * Consulta generica para ejecutar un query
     *
     * @param string $query query a ejecutar.
     * @return array resultado de la ejecucion.
     */
    public function query(string $query){
        $this->lastQuery = $this->db->query($query);
        return $this->result($this->lastQuery);
    }

    /**
     * Consulta para obtener el ultimo error que ha ocurrido en las ejecuciones sql.
     * @return array
     */
    public function getError(){
        return $this->db->error();
    }

    /**
     * Obtiene el resultado textual de la ultimna consulta ejecutada.
     *
     * @return string resultado textual del ultimo query ejecutado.
     */
    public function getQueryBuild() {
        return $this->db->last_query();
    }

    /* --------------------------------------------------------------
    *
    *
    * CRUD METHODS
    *
    *
    * ------------------------------------------------------------ */


    /* --------------------------------------------------------------
    * SELECT METHODS
    * ------------------------------------------------------------ */

    /**
    * Metodo que prepara una consulta de seleccion.
    * @param  mixed $values a single (value|id) or array of (values|ids) (optional).
    * @param  string $column search column instead of primary key (optional).
    * @param  mixed $order_by string or array for make a order by query in the selection (optional).
    * @return void
    */
    private function prepareSelect($values = null, string $column = "", $order_by = null): void {
        $this->db->select('*');
        $this->db->from($this->table);

        if (!is_null($values)) {			
            if (!is_array($values)) {
                $value = array($values);
            }
            if(empty($column)) {
                $this->db->where_in($this->llave_primaria, $value);
            } else {
                $this->db->where_in($column, $value);
            }
        }

        if(is_null($order_by)){
            $this->db->order_by($this->llave_primaria, "ASC");
        }else if(is_string($order_by)){
            $this->db->order_by($order_by);
        }else if(is_array($order_by)) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        } 
    }

    /**
    * Metodo que prepara una consulta de seleccion y entrega el resultado
    * @param  mixed  $values a single (value|id) or array of (values|ids) (optional).
    * @param  string $column search column instead of primary key (optional).
    * @param  mixed  $order_by string or array for make a order by query in the selection (optional).
    * @return array resultado de la consulta de seleccion.
    */
    public function select($values = null, $column = "", $order_by = null): array {
        $this->prepareSelect($values, $column, $order_by);
        $this->lastQuery = $this->db->get();
        $count = $this->db->count_all_results();
        return $this->result($this->lastQuery);
    }

    /**
     * Consulta generica para seleccionar un registro de la tabla 
     * por su identificador unico de llave primaria y entregar su resultado.
     *
     * @param  mixed $id id del registro que se seleccionara.
     * @return mixed retorna el registro con el id seleccionado nulo en caso de no ser encontrado.
     */
    public function selectById($id) {
        $this->prepareSelect($id);
        $this->lastQuery = $this->db->get();
        return $this->row($this->lastQuery);
    }

    /**
     * Alias de selectById($id).
     * Consulta generica para seleccionar un registro de la tabla 
     * por su identificador unico de llave primaria y entregar su resultado.
     * 
     * @param mixed $id id del registro que se seleccionara.
     * @return mixed retorna el registro con el id seleccionado nulo en caso de no ser encontrado.
     */
    public function getById($id) {
        return $this->selectById($id);
    }

    /**
     * Consulta generica para recuperar todos los registros de la tabla.
     * @return array resultado de la consulta de seleccion.
     */
    public function getAll(): array {
        return $this->select();
    }

    /**
     * Consulta generica para recuperar el primer registro de la tabla
     * @return mixed resultado de la consulta como un array u objeto, nulo en caso de no tener resultado.
     */
    public function getFirst() {
        $this->prepareSelect();
        $this->db->limit(1);
        $this->lastQuery = $this->db->get();
        return $this->row($this->lastQuery);
    }

    /**
     * Consulta generica para recuperar el ultimo registro de la tabla
     * @return mixed resultado de la consulta como un array u objeto, nulo en caso de no tener resultado.
     */
    public function getLast() {
        $this->prepareSelect(null, "", array($this->llave_primaria => "DESC"));
        $this->db->limit(1);
        $this->lastQuery = $this->db->get();
        return $this->row($this->lastQuery);
    }

    /**
     * Consulta generica para recuperar un registro identificado por su llave primaria id.
     * @param mixed id del registro que se buscara.
     * @return boolean TRUE en caso de encontrarse el id FALSO en caso contario.
     */
    public function existId($id): bool {
        $this->selectById($id);
        $exists = $this->lastQuery->num_rows();
        return $exists === 1 ? TRUE : FALSE;
    }

    /**
     * Consulta generica para obtener el numero de filas de la tabla.
     * @return int numero de filas en la tabla.
     */
    public function count(): int{
        $this->db->select('COUNT(*)');
        $this->db->from($this->table);
        $query = $this->db->get();
        $resultado = $this->row($query);
        return (int) $resultado['COUNT(*)'];
    }

    /* --------------------------------------------------------------
    * INSERT METHODS
    * ------------------------------------------------------------ */

    /**
	 * Inserts a new row on the table.
	 * @param  array $data info to insert.
	 * @return mixed returns the inserted id on sucess or FALSE otherwise.
	 */
	public function insert(array $data) {
		if (count($data) > 0) {
			$this->db->insert($this->table, $data);
			$id = $this->db->insert_id();
			return $id;
		} else {
			return FALSE;
		}
    }

    /**
     * Inserta o actualiza un registro dependiendo de si un id proporcionado existe.
     *
     * @param array $data info to be updated or inserted.
     * @param mixed $values a single (value|id) or array of (values|ids) (optional).
     * @return mixed returns the inserted id on insert, returns a integer number of affected rows in case of update.
     */
    public function insertUpdate(array $data, $values = null) { 
        if(!is_null($values) && $this->existId($values)) {
            return $this->update($data,  $values);
        } else {
            return $this->insert($data);
        }
    }

    /* --------------------------------------------------------------
    * UPDATE METHODS
    * ------------------------------------------------------------ */

    /**
	 * Update table data. It can be chainable with 'where' query helper.
	 * 
	 * @param  array $data 	info to be updated
	 * @param  mixed $values a single (value|id) or array of (values|ids) (optional)
     * @param  string $column search column instead of primary key
	 * @return int returns a integer number of affected rows, 0 in case of error.
	 */
    public function update(array $data, $values = null, string $column = "") : int {
        if (count($data) > 0) {

            if (!is_null($values)) {			
                if (!is_array($values)) {
                    $value = array($values);
                }
                if(empty($column)) {
                    $this->db->where_in($this->llave_primaria, $value);
                } else {
                    $this->db->where_in($column, $value);
                }
            }

			$this->db->update($this->table, $data);
			$rows = $this->db->affected_rows();
			return $rows;
		} else {
			return 0;
		}
    }

    /* --------------------------------------------------------------
    * DELETE METHODS
    * ------------------------------------------------------------ */

    /**
	 * Delete rows from table using primary key as reference 
     * if no other search column is given.
	 * 
	 * @param  mixed  $values a single (value|id) or array of (values|ids) 
     * @param  string $column search column instead of primary key (optional).
	 * @return int    returns a integer number of affected rows, 0 in case of error.
	 */
	
	public function delete($values, string $column = "") : int {
		if (!is_null($values)) {			
			if (!is_array($values)) {
				$value = array($values);
			}
			if(empty($column)) {
				$this->db->where_in($this->llave_primaria, $value);
			} else {
				$this->db->where_in($column, $value);
            }

            $resultado = $this->db->delete($this->table);
            $rows = $this->db->affected_rows();

            if($resultado && $this->count() <= 0){
                $this->resetAutoIncrement();
            }
            
            return $rows;
        }
        return 0;
    }

    /**
     * Consulta generica para eliminar un registro identificado por su llave primaria.
     * @param mixed $id id a ser eliminado.
     * @return boolean return TRUE on success, FALSE on failure.
     */
    public function deleteById($id) : bool {
        return $this->delete($id) == 1 ? TRUE : FALSE;
    }
    
    /**
     * Genera una cadena de SQL truncada y ejecuta la consulta.
     * Produce: TRUNCATE mytable
     * 
     * Debido a que una operación truncada provoca una confirmación implícita, por lo tanto, no se puede revertir.
     * La instrucción TRUNCATE TABLE restablece el valor de la columna AUTO_INCREMENT a su valor inicial si la tabla tiene una columna AUTO_INCREMENT.
     * @return boolean TRUE en caso de éxito, FALSE en caso de error.
     */
    public function truncate() : bool {
        return $this->db->truncate($this->table);
    }

    /**
     * Alias de truncate(), Genera una cadena de SQL truncada y ejecuta la consulta.
     * Produce: TRUNCATE mytable
     * Debido a que una operación truncada provoca una confirmación implícita, por lo tanto, no se puede revertir.
     * La instrucción TRUNCATE() restablece el valor de la columna AUTO_INCREMENT a su valor inicial, si la tabla tiene alguna columna AUTO_INCREMENT.
     *
     * @return boolean TRUE en caso de éxito, FALSE en caso de error.
     */
    public function resetTable() : bool {
        return $this->truncate();
    }

    /**
     * Genera una consulta que borra todas las filas (registros) de la tabla.
     * Produce: DELETE FROM mytable
     * La instrucción emptyTable() NO restablece el valor de la columna AUTO_INCREMENT a su valor inicial, si la tabla tiene alguna columna AUTO_INCREMENT.
     *
     * @return boolean TRUE on success, FALSE on failure
     */
    public function emptyTable(): bool {
        return $this->db->empty_table($this->table);
    }
    /**
     * Metodo que restablece el autoincrement de la tabla.
     *
     * @return void
     */
    public function resetAutoIncrement(): void {
        $this->db->query("ALTER TABLE $this->table AUTO_INCREMENT =  1");
    }
}
?>