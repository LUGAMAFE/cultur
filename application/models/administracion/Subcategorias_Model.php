<?php
class Subcategorias_Model extends MY_Model {

    public function __construct(){
        parent::__construct("subcategorias");	
    }

    //Funciones De Grupos_Model que antes se usaban y se pasaron a Subcategorias_Model
    //guardarGrupos()
    //eliminarGrupos()
    //getGrupos()
    //getGruposPendientes()
    //existeGrupo()

    public function guardarSubcategorias($subcategorias){
        foreach ($subcategorias as $subcategoria) {
            $dato = ["nombre_subcategoria" => ucwords($subcategoria)];
            $this->db->insert($this->table, $dato);
        }
    }

    public function eliminarSubcategorias($subcategorias){
        foreach ($subcategorias as $subcategoria) {
            $dato = ["nombre_subcategoria" => ucwords($subcategoria)];
            $this->db->delete('subcategorias', $dato);
        }
    }

    public function getSubcategorias(){
        $this->db->select('nombre_subcategoria');
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSubcategoriasPendientes(){
        $this->db->select("$this->llave_primaria, nombre_subcategoria");
        $this->db->from($this->table);
        $this->db->join('categorias_subcategorias', 'subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria', "left");
        $this->db->where("categorias_subcategorias.subcategorias_id_subcategoria IS null");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function existeSubcategoria($subcategoria){
        $nombreSubcategoria = $subcategoria["nombreSubcategoria"];
        $idSubcategoria = $subcategoria["idSubcategoria"];
        $this->db->select('*');
        $this->db->from('subcategorias');
        $this->db->where("nombre_subcategoria", $nombreSubcategoria);
        $this->db->where("id_subcategoria", $idSubcategoria);
        $query = $this->db->get();
        $existeSubcategoria = $query->num_rows();
        return $existeSubcategoria === 1 ? TRUE : FALSE;
    }
}