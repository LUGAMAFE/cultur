<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tendencias_Model extends MY_Model {

	public function __construct(){
        parent::__construct("tendencias");	
        $this->load->database();
    }

    public function obtenerProductosRelacionados($listaIdsMcp){
        if($listaIdsMcp == null){
            $query = [];
        }else{
            $implodeIds = implode("','", $listaIdsMcp);
            $query = $this->db->query("select productos.id_prod AS id_prod, productos.id_mcp AS id_mcp, ifnull(categorias.nombre_categoria,'Sin Categorizar') AS nombre_categoria, subcategorias.nombre_subcategoria AS nombre_subcategoria, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.desc_mcp AS desc_mcp, (case when (productos.visible_prod = 1) then 'Visible' when (productos.visible_prod = 0) then 'Oculto' end) AS `visible_prod`, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
            from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria
            left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
            left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
            left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto'
            left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
            WHERE id_mcp IN ('$implodeIds');");
            $query = $query->result_array();
        }
        return json_encode($query);
    }


    //GET allEspecificacionTendencias
    // retorna todas las especificaciones de las tendencias que son puestas en el slider 
    public function getAllEspecificacionTendencias(){

        $query =$this->db->query("SELECT nombre_tendencia, especificacion_tendencia FROM tendencias"); 

        $tendencias = $query->result_array();
        
        return $tendencias;
    }

    //getUnaEspecificacionTendencia
    //retorna la especificacion deseada (requerido $id de la tendencia requerida)
    private function getUnaEspecificacionTendencia($id){

        $query = "SELECT nombre_tendencia , especificacion_tendencia FROM tendencias WHERE id_tendencia = " . $id; 

        $tendencia = $this->query($query);
        return  $tendencia;

    }

    // falta cambiar los datos de la tabla
    public function getImagenesTendencias($id){
        $query = "SELECT archivos.* FROM archivos
        LEFT JOIN imagenes_ambiente_tendencias ON imagenes_ambiente_tendencias.id_file_asoc_amb = archivos.id_file 
        WHERE imagenes_ambiente_tendencias.id_tendencia_asoc = '$id'";
        $row = $this->query($query);
        return $row[0];
    }

    // 
    public function guardarTendencia($data, $update, $id = NULL){
        if($update){
            $this->db->where($this->llave_primaria, $id);
            $resultado = $this->db->update($this->table, $data);
        }else{
            $resultado = $this->db->insert($this->table, $data);
        }
        return $resultado;
    }

    public function emptyImagenesAmbiente($id){
        $this->db->where('id_tendencia_asoc', $id);
        $this->db->delete('imagenes_ambiente_tendencias');
    }

    public function guardarImagenesAmbiente($id, $ids){
        foreach ($ids as $key => $value) {
            $datos = array(
                "id_tendencia_asoc" => $id,
                "id_file_asoc_amb" => $value,
            );
            $this->db->insert("imagenes_ambiente_tendencias", $datos);
        }
    }

    public function eliminarEntradaPorId($id){
        $this->db->where('id_tendencia', $id);
        $resultado = $this->db->delete('tendencias');
        return $resultado;
    }

    public function existeEntradaId($idTendencia){
        $this->db->select('*');
        $this->db->from('tendencias');
        $this->db->where("id_tendencia = '$idTendencia'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function existeEntrada($datos, $id = NULL){
        $nombre_autor = $datos["nombre"];
        $this->db->select('*');
        $this->db->from('tendencias');
        $this->db->where("id_tendencia='$id'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function obtenerEntradasTabla(){
        $this->db->select('*');
        $this->db->from('tendencias');
        $this->db->order_by("id_tendencia", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoEntradaModificarPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('tendencias');
        $this->db->where("id_tendencia = '$idEntrada'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countTendencias(){
        $this->db->select('COUNT(*)');
        $this->db->from('tendencias');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetEntradas(){
        $query = $this->db->query("ALTER TABLE tendencias AUTO_INCREMENT =  1");
    }

}    