<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funciones_Model extends MY_Model {

	public function __construct(){
        parent::__construct("funciones_page_data");	
        $this->load->database();
    }

    //GET allCatalogos
    //trae el nombre de los catalogos 
    public function getAllCatalogos(){
        $query =$this->db->query("SELECT * FROM archivos LEFT JOIN  catalogos_page_data ON catalogos_page_data.id_file_img_catalogo = archivos.id_file");
        $catalogos = $query->result_array();  
        return $catalogos;
    }

    public function timeToText($time){
        $duracion = getdate(strtotime($time));
        $horas = $duracion["hours"] == 1 ? "hora" : "horas" ;
        $minutos = $duracion["minutes"] == 1 ? "minuto" : "minutos" ;
        return "{$duracion['hours']} $horas con {$duracion['minutes']} $minutos";
    }

    public function getFuncionesDeHoy($idPelicula = null) {
        $this->db->select('id_funcion, pelicula-asociada-funcion, sala-asociada-funcion, idioma-funcion');
        $this->db->from("funciones_page_fechas");
        $fechaHoy = date('Y-m-d');
        $this->db->join('funciones_page_data', 'funciones_page_data.id_funcion = funciones_page_fechas.id_funcion_assoc');
        $this->db->where("fecha_funcion", $fechaHoy);

        if( !is_null($idPelicula) && is_numeric($idPelicula) ) {
            $this->db->where("pelicula-asociada-funcion", $idPelicula);
        }
        
        $this->db->order_by('pelicula-asociada-funcion', 'ASC');
        $this->db->order_by('idioma-funcion', 'ASC');
        $query = $this->db->get();
        $funciones = $query->result_array();  
        for ($i=0; $i < count($funciones); $i++) { 
            $funciones[$i]["datos_pelicula"] = $this->Peliculas_Model->getById($funciones[$i]["pelicula-asociada-funcion"]);
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $funciones[$i]["datos_pelicula"]["trailer_pelicula"], $matches);
            $funciones[$i]["datos_pelicula"]["id_trailer_pelicula"] = $matches[1]; 
            $funciones[$i]["datos_pelicula"]["duracion_pelicula"] = $this->timeToText($funciones[$i]["datos_pelicula"]["duracion_pelicula"]);
            $funciones[$i]["datos_pelicula"]["imagen_pelicula"] = $this->Peliculas_Model->getImagenPelicula($funciones[$i]["datos_pelicula"]["id_pelicula"]);
            $funciones[$i]["nombre_sala_funcion"] = $this->Salas_Model->getById($funciones[$i]["sala-asociada-funcion"])["nombre_sala"];
            $funciones[$i]["horas_funcion"] = $this->Funciones_Model->getHorasFuncion($funciones[$i]["id_funcion"], "24");
        }

        function buscarLlavesPorValorDeLlave($valor, $clave, $array){
            $keys = [];
            for ($i=0; $i < count($array); $i++) { 
                if($array[$i][$clave] == $valor){
                    $keys[] = $i;
                }
            }
            return $keys;
        }

        function buscarFuncionesMismoIdioma($idioma, $funciones){
            $keys = [];
            for ($i=0; $i < count($funciones); $i++) { 
                if($funciones[$i]["pelicula-asociada-funcion"] == $idioma){
                    $keys[] = $i;
                }
            }
            return $keys;
        }

        function obtenerFuncionesMismaSala(&$funcionesMismoIdioma, &$nuevaFuncion, $llaveIdiomas){
            $nuevaFuncion["idiomas"][$llaveIdiomas]["salas"] = [];
            for($llaveSalas = 0; count($funcionesMismoIdioma) > 0; $llaveSalas++) {
                $funcionesMismaSala = buscarLlavesPorValorDeLlave($funcionesMismoIdioma[0]["nombre_sala_funcion"], "nombre_sala_funcion", $funcionesMismoIdioma);
                $funcionesMismaSala = array_splice($funcionesMismoIdioma, $funcionesMismaSala[0], count($funcionesMismaSala));
                $nuevaFuncion["idiomas"][$llaveIdiomas]["salas"][$llaveSalas]["nombre_sala_funcion"] = $funcionesMismaSala[0]["nombre_sala_funcion"];
                $nuevaFuncion["idiomas"][$llaveIdiomas]["salas"][$llaveSalas]["horas_funcion"] = [];
                for ($j=0; $j < count($funcionesMismaSala); $j++) {
                    $nuevaFuncion["idiomas"][$llaveIdiomas]["salas"][$llaveSalas]["horas_funcion"] = array_merge ($nuevaFuncion["idiomas"][$llaveIdiomas]["salas"][$llaveSalas]["horas_funcion"], $funcionesMismaSala[$j]["horas_funcion"]);
                }    
                sort($nuevaFuncion["idiomas"][$llaveIdiomas]["salas"][$llaveSalas]["horas_funcion"]);
            }    
        }

        function crearFuncionVerdadera(&$funcionesReales, $funciones){
            $nuevaFuncion = [];
            $nuevaFuncion["pelicula-asociada-funcion"] = $funciones[0]["pelicula-asociada-funcion"];
            $nuevaFuncion["datos_pelicula"] = $funciones[0]["datos_pelicula"];
            $nuevaFuncion["idiomas"] = [];
            for($llaveIdiomas = 0; count($funciones) > 0; $llaveIdiomas++) {
                $funcionesMismoIdioma = buscarLlavesPorValorDeLlave($funciones[0]["idioma-funcion"], "idioma-funcion", $funciones);
                $funcionesMismoIdioma = array_splice($funciones, $funcionesMismoIdioma[0], count($funcionesMismoIdioma));
                $nuevaFuncion["idiomas"][$llaveIdiomas]["idioma"] = $funcionesMismoIdioma[0]["idioma-funcion"];

                obtenerFuncionesMismaSala($funcionesMismoIdioma, $nuevaFuncion, $llaveIdiomas);
            }
            $funcionesReales[] = $nuevaFuncion;
        }

        $funcionesReales = [];
        while(count($funciones) > 0) { 
            $pelicula = $funciones[0]["pelicula-asociada-funcion"];
            $funcionesAsociadas = buscarLlavesPorValorDeLlave($pelicula, "pelicula-asociada-funcion", $funciones);
            $funcionesAsociadas = array_splice($funciones, $funcionesAsociadas[0], count($funcionesAsociadas));
            crearFuncionVerdadera($funcionesReales, $funcionesAsociadas);
        }

        return $funcionesReales;
    }

    public function getFechasFuncion($id){
        $this->db->select('*');
        $this->db->from("funciones_page_fechas");
        $this->db->where("id_funcion_assoc = $id");
        $query = $this->db->get();
        $fechasFuncion = $query->result_array();
        
        $diaFuncionFormateado = "";
        foreach($fechasFuncion as $key=>$value){
            if($key == 0){
                $diaFuncionFormateado = $diaFuncionFormateado.ucwords(strftime("%A %d %B %Y", strtotime($value["fecha_funcion"])));
            } else {
                $diaFuncionFormateado = $diaFuncionFormateado.",".ucwords(strftime("%A %d %B %Y", strtotime($value["fecha_funcion"])));
            }
        }

        return $diaFuncionFormateado;
    }

    public function getHorasFuncion($id, $formato = "12"){
        $this->db->select('*');
        $this->db->from("funciones_page_horas");
        $this->db->where("id_funcion_assoc = $id");
        $this->db->order_by('hora_funcion', 'ASC');
        $query = $this->db->get();
        $horasFuncion = $query->result_array();
        
        foreach($horasFuncion as $key=>$value){
            if($formato == "24"){
                $horasFuncion[$key] = date('H:i', strtotime($value["hora_funcion"]));
            }else{
                $horasFuncion[$key] = date('h:i A', strtotime($value["hora_funcion"]));
            }
        }

        return $horasFuncion;
    }

    public function guardarFuncion($data, $update, $id = NULL){
        $fechasFuncion = $data["dia_funcion"];
        unset($data["dia_funcion"]);
        $horasFuncion = $data["fecha_funcion"];
        unset($data["fecha_funcion"]);

        if($update){
            $this->db->where($this->llave_primaria, $id);
            $resultado = $this->db->update($this->table, $data);
            $idNuevo = $id;
        }else{
            $resultado = $this->db->insert($this->table, $data);
            $idNuevo = $this->db->insert_id();
        }

        $this->guardarFechasFuncion($idNuevo, $fechasFuncion);
        $this->guardarHorasFuncion($idNuevo, $horasFuncion);

        return $resultado;
    }

    private function guardarFechasFuncion($id, $fechasFuncion){
        $fechasFuncion = explode(",", $fechasFuncion);

        $this->db->delete("funciones_page_fechas", array('id_funcion_assoc' => $id));

        foreach($fechasFuncion as $fecha){
            $data = array(
                "id_funcion_assoc" => $id,
                "fecha_funcion" => $fecha
            );
            $resultado = $this->db->insert("funciones_page_fechas", $data);
        }
    }

    private function guardarHorasFuncion($id, $horasFuncion){
        $horasFuncion = explode(",", $horasFuncion);

        $this->db->delete("funciones_page_horas", array('id_funcion_assoc' => $id));

        foreach($horasFuncion as $hora){
            $data = array(
                "id_funcion_assoc" => $id,
                "hora_funcion" => $hora
            );
            $resultado = $this->db->insert("funciones_page_horas", $data);
        }
    }

    public function obtenerFuncionesSala($idSala, $diaFuncion, $update, $idEntrada){
        $dias = explode(",",$diaFuncion);
        $dias = implode("','", $dias);

        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('funciones_page_fechas', 'funciones_page_fechas.id_funcion_assoc = funciones_page_data.id_funcion');
        $this->db->join('funciones_page_horas', 'funciones_page_horas.id_funcion_assoc = funciones_page_data.id_funcion');
        $this->db->where("sala-asociada-funcion = $idSala");

        if($update){
            $this->db->where("id_funcion != $idEntrada");
        }

        $this->db->where("fecha_funcion IN ('".$dias."')");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function eliminarEntradaPorId($id){
        $this->db->where('id_catalogo', $id);
        $resultado = $this->db->delete('catalogo');
        return $resultado;
    }

    public function existeEntradaId($idEntrada){
        $this->db->select('*');
        $this->db->from('catalogos_page_data');
        $this->db->where("id_catalogo = '$idEntrada'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function obtenerEntradasTabla(){
        $this->db->select('*');
        $this->db->from('catalogos_page_data');
        $this->db->order_by("id_catalogo", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoEntradaModificarPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('catalogos_page_data');
        $this->db->where("id_catalogo = '$idEntrada'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countentradas(){
        $this->db->select('COUNT(*)');
        $this->db->from('catalogos_page_data');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetEntradas(){
        $query = $this->db->query("ALTER TABLE catalogos_page_data AUTO_INCREMENT =  1");
    }

}    