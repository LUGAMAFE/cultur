<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promociones_Model extends MY_Model {

	public function __construct(){
        parent::__construct("promociones_page_data");	
    }

    public function get_banner(){
        $banners = $this->db
                        ->join('archivos as img', "img.id_file = $this->table.id_file_img_promocion", 'left')
                        ->get($this->table);

        $result = $banners->result();
        return $result;
    }

    public function guardarImagenes($data, $jsonInputExtras){
        if($this->count() > 0){
            $this->emptyTable();
        }
        
        foreach ($data as $key => $value ) {
            $dato = ["id_file_img_promocion" => $value,
                    "json_input_extras" => json_encode($jsonInputExtras[$key]),
                    "is_activo" => $jsonInputExtras[$key]["activo_promocion"]];
            $this->insert($dato);
        }
    }

    public function obtenerImagenes(){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('archivos as img', "img.id_file = $this->table.id_file_img_promocion", 'left');
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function guardarIsActivo($data){
        $this->db->select('*');
        $this->db->from('promociones_page_info');
        $this->db->where('id_promocion', 1);
        $query = $this->db->get();  
        $datos = array(
            "is_activo" => $data,
        );
        if($result = $query->result_array() != null){
            $this->db->where('id_promocion', 1);
            $this->db->update('promociones_page_info', $datos);
        } else {
            $this->db->insert('promociones_page_info', $datos);
        }
    }

    public function obtenerIsActivo(){
        $this->db->select('is_activo');
        $this->db->from('promociones_page_info');
        $this->db->where('id_promocion', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        if($result != null){
            return $result[0]["is_activo"];
        } else {
            return 0;
        }
    }

    public function obtenerImagenesActivasVentana(){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('archivos as img', "img.id_file = $this->table.id_file_img_promocion", 'left');
        $this->db->where("$this->table.is_activo", 1);
        $query = $this->db->get();  
        return $query->result_array();
    }
}    