<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin <?= $singular ?> <i class="<?= $iconoFA?>"></i> <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines") ?>">Administración Cines</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines/peliculas") ?>"> <?= $plural ?> </a></li>
            <li class="breadcrumb-item active">Admin <?= $singular ?> <?= $accion ?> </li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Datos <?= $singular ?> </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <!-- aqui va la imagen del ccatalogo -->
                            <div class="form-group">
                                <label>Nombre de <?= $singular ?></label>
                                <input type="text" class="form-control" name="nombre_pelicula" placeholder="Nombre de la pelicula..." autocomplete="off" required value="<?= isset($datos_entrada["nombre_pelicula"]) ? $datos_entrada["nombre_pelicula"] : ""; ?>">
                            </div>
                            <div class="form-group">
                                <label>Clasificacion <?= $singular ?></label>
                                <select class="form-control" name="clasificacion_pelicula" required> 
                                  <?php if(isset($datos_entrada["clasificacion_pelicula"])): $valorClasificacion = $datos_entrada["clasificacion_pelicula"]?>
                                    <option value="AA" <?= $valorClasificacion == "AA" ? "selected" : ""; ?>>AA</option>
                                    <option value="A" <?= $valorClasificacion == "A" ? "selected" : ""; ?>>A</option>
                                    <option value="B" <?= $valorClasificacion == "B" ? "selected" : ""; ?>>B</option>
                                    <option value="B15" <?= $valorClasificacion == "B15" ? "selected" : ""; ?>>B15</option>
                                    <option value="C" <?= $valorClasificacion == "C" ? "selected" : ""; ?>>C</option>
                                    <option value="D" <?= $valorClasificacion == "D" ? "selected" : ""; ?>>D</option>  
                                  <?php else:?>    
                                    <option value="AA">AA</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="B15">B15</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option> 
                                  <?php endif;?>  
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Duración <?= $singular ?> (Una vez guardada solo se puede reducir)</label>
                                <input type="text" class="form-control" id="duracion_pelicula" name="duracion_pelicula" placeholder="Duracion de la pelicula..." autocomplete="off" required value="<?= isset($datos_entrada["duracion_pelicula"]) ? strtotime("1970-01-01 {$datos_entrada["duracion_pelicula"]} UTC") : ""; ?>">
                            </div>
                            <div class="form-group">
                                <label>Sinopsis <?= $singular ?></label>
                                <textarea class="form-control" rows="3" name="sinopsis_pelicula" placeholder="Sinopsis de la pelicula..." required><?= isset($datos_entrada["sinopsis_pelicula"]) ? $datos_entrada["sinopsis_pelicula"] : ""; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Url Trailer <?= $singular ?></label>
                                <input type="text" class="form-control" id="trailer_pelicula" name="trailer_pelicula" placeholder="Trailer de la pelicula..." autocomplete="off" required value="<?= isset($datos_entrada["trailer_pelicula"]) ? $datos_entrada["trailer_pelicula"] : ""; ?>">
                            </div>
                            <div class="form-group">
                                <label>Imagen <?= $singular ?> <small>Tamaño Minimo: 400 x 650 pixeles</small></label>
                                <div class="upload-area individual imagen requerido" id="imagen-pelicula"></div>
                            </div>
                        </div> 
                        <!-- /.end col -->
                    </div>   
                    <!-- /.end row -->
                </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <a href="<?= $back ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-primary float-right">Guardar <?= $singular ?> </div>
              </div>
              <!-- /.card-footer -->

            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  const idPelicula = "<?=isset($datos_entrada["id_pelicula"]) ? $datos_entrada["id_pelicula"] : "" ?>" ; 
  const duracionPelicula = "<?=isset($datos_entrada["duracion_pelicula"]) ? strtotime("1970-01-01 {$datos_entrada["duracion_pelicula"]} UTC") : "" ?>" ;
</script>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
