<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin <?= $plural ?> <i class="<?= $iconoFA?>"></i> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines") ?>">Administración Cines</a></li>
            <li class="breadcrumb-item active">Admin <?= $plural ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Editar <?= $plural ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                  <div class="row justify-content-center">
                      <div class="col-12">

                          <div class="form-group">
                              <div class="form-group checkbox-options">
                                <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" name="all-activo" id="all-activo" <?= ($is_activo == 1 ) ? "checked": "" ?>> 
                                <label class="custom-control-label" for="all-activo">Activar/Desactivar <?= $plural ?></label>
                                </div>
                              </div>
                              <label>Imagen <?= $singular ?> <small>Tamaño: 1000 x 400 pixeles</small></label>
                              <div class="upload-area multiple requerido" id="imagen-promocion">
                              </div>
                          </div>

                      </div>   
                  </div>   
                </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= $back ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-primary float-right">Guardar <?= $plural ?> </div>
              </div>
            </form>
            <div class="overlay">
              <div class="wrap">
                <i class="loader rotating-infinite fas fa-sync-alt"></i>
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>