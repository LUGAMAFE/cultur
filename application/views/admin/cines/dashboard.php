<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard Administración Cines</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Administración Cines</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
 
  <!-- Main content -->
  <section class="content" >
    <div class="container-fluid">
    <!--<iframe src="https://h5.veer.tv/player?vid=276775&utm_medium=embed" frameborder="0"    allowfullscreen="true" style="height: 500px; width: 100%;" ></iframe> -->
      <!-- Small boxes (Stat box) -->
      <div class="row d-flex justify-content-center justify-content-md-start">


        
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <a href="<?= site_url("administracion/cines/peliculas") ?>" class="small-box bg-info">
            <div class="inner">
              <h3>Peliculas</h3>

              <p>Creación, Eliminación, Modificación de información de las películas en cartelera.</p>
            </div>
            <div class="icon">
              <i class="fas fa-film"></i>
            </div>
            <div  class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <a href="<?= site_url("administracion/cines/promociones") ?>" class="small-box bg-info">
            <div class="inner">
              <h3>Promociones</h3>

              <p>Creación, Eliminación, Modificación de Banners Con Promociones Actuales</p>
            </div>
            <div class="icon">
              <i class="fas fa-money-check-alt"></i>
            </div>
            <div  class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <a href="<?= site_url("administracion/cines/salas") ?>" class="small-box bg-info">
            <div class="inner">
              <h3>Salas</h3>

              <p>Creación, Eliminación, Modificación de las salas de cines actuales y su respectiva información.</p>
            </div>
            <div class="icon">
              <i class="fas fa-couch"></i>
            </div>
            <div  class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        <!-- ./col -->
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <a href="<?= site_url("administracion/cines/funciones") ?>" class="small-box bg-info">
            <div class="inner">
              <h3>Funciones</h3>

              <p>Creación, Eliminación, Modificación de la información de las funciones. <br>Relaciona: horario, sala, película, idioma.</p>
            </div>
            <div class="icon">
              <i class="fas fa-video"></i>
            </div>
            <div  class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></div>
          </a>
        </div>
        
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php echo $this->load->view('admin/utils/sweetAlerts', '', true); ?>