<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin <?= $singular ?> <i class="<?= $iconoFA?>"></i> <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines") ?>">Administración Cines</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines/salas") ?>"> <?= $plural ?> </a></li>
            <li class="breadcrumb-item active">Admin <?= $singular ?> <?= $accion ?> </li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Datos <?= $singular ?> </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <!-- aqui va la imagen del ccatalogo -->
                            <div class="form-group">
                                <label>Nombre de <?= $singular ?></label>
                                <input type="text" class="form-control" name="nombre_sala" placeholder="Nombre de la sala..." autocomplete="off" required value="<?= isset($datos_entrada["nombre_sala"]) ? $datos_entrada["nombre_sala"] : ""; ?>">
                            </div>
                            <div class="form-group">
                                <label>Asientos Totales <?= $singular ?></label>
                                <input type="number" class="form-control" name="numero_asientos_totales_sala" placeholder="Numero Asientos Totales de Sala.." autocomplete="off" required value="<?= isset($datos_entrada["numero_asientos_totales_sala"]) ? $datos_entrada["numero_asientos_totales_sala"] : ""; ?>">
                            </div>
                        </div> 
                        <!-- /.end col -->
                    </div>   
                    <!-- /.end row -->
                </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <a href="<?= $back ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <button type="submit" class="btn btn-primary float-right">Guardar <?= $singular ?> </button>
              </div>
              <!-- /.card-footer -->

            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
