<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin <?= $singular ?> <i class="<?= $iconoFA?>"></i> <?= $accion ?> </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines") ?>">Administración Cines</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines/funciones") ?>"> <?= $plural ?> </a></li>
            <li class="breadcrumb-item active">Admin <?= $singular ?> <?= $accion ?> </li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Datos <?= $singular ?> </h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="d-block">Sala de la <?= $singular ?></label>
                                <select title="Selecciona la sala asociada" name="sala-asociada-funcion" data-size="5" class="form-data selectpicker-sala" data-width="auto" required> 
                                  <?php if(isset($salas) ): ?>
                                    <?php foreach($salas as $sala): ?>
                                      <option value="<?= $sala["id_sala"] ?>" <?= isset($datos_entrada["sala-asociada-funcion"]) &&  $datos_entrada["sala-asociada-funcion"] == $sala["id_sala"] ? "selected" : "";?> ><?= $sala["nombre_sala"] ?></option>
                                      <?php endforeach; 
                                  endif;?> 
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="d-block">Pelicula de la <?= $singular ?></label>
                                <select title="Selecciona la pelicula asociada" name="pelicula-asociada-funcion" data-size="5" class="form-data selectpicker-pelicula" data-width="auto" required data-live-search="true">
                                  <?php if(isset($peliculas) ): ?>
                                    <?php foreach($peliculas as $pelicula): ?>
                                      <option data-thumbnail="<?= $pelicula["imagen_pelicula"]["full_route_file"] ?>" value="<?= $pelicula["id_pelicula"] ?>" <?= isset($datos_entrada["pelicula-asociada-funcion"]) &&  $datos_entrada["pelicula-asociada-funcion"] == $pelicula["id_pelicula"] ? "selected" : "";?> ><?= $pelicula["nombre_pelicula"] ?></option>
                                      <?php endforeach; 
                                  endif;?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="d-block">Idioma de la <?= $singular ?></label>
                                <select title="Selecciona el idioma de la funcion" name="idioma-funcion" data-size="5" class="form-data selectpicker-idioma" data-width="auto" required> 
                                    <?php if(isset($datos_entrada["idioma-funcion"])): $idiomaFuncion = $datos_entrada["idioma-funcion"]?>
                                      <option value="Español" <?= $idiomaFuncion == "Español" ? "selected" : ""; ?>>Español</option>
                                      <option value="Subtitulada" <?= $idiomaFuncion == "Subtitulada" ? "selected" : ""; ?>>Subtitulada</option>
                                      <option value="Inglés" <?= $idiomaFuncion == "Inglés" ? "selected" : ""; ?>>Inglés</option>
                                    <?php else:?>    
                                      <option value="Español">Español</option>
                                      <option value="Subtitulada">Subtitulada</option>
                                      <option value="Inglés">Inglés</option>
                                  <?php endif;?>  
                                </select>
                            </div>

                            <div class="form-group form_dates">
                                <label class="d-block">Dia/s de la <?= $singular ?></label>
                                <input type="text" class="form-control" name="dia_funcion" required value="<?= isset($datos_entrada["dia_funcion"]) ? $datos_entrada["dia_funcion"] : ""; ?>" readonly>
                            </div>

                                
                            <div class="form-group input-append date form_datetime">
                                <label>Hora/s de la <?= $singular ?></label>
                                <?php if(isset($datos_entrada["fecha_funcion"])):
                                    foreach($datos_entrada["fecha_funcion"] as $key=>$value):?>
                                      <input class="form-control input-hora" size="16" type="text" id="<?='time'.$key?>" name="fecha_funcion[]" required value="<?= $value ?>" readonly>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                  <input class="form-control input-hora" size="16" type="text" id="time0" name="fecha_funcion[]" required readonly>
                                <?php endif; ?>
                                <span class="add-on"><i class="icon-remove"></i></span>
                                <span class="add-on"><i class="icon-th"></i></span>
                                <div id="newRow"></div>
                                <button id="addRow" type="button" class="btn btn-info">Añadir Horario</button>
                            </div>

                        </div> 
                        <!-- /.end col -->
                    </div>   
                    <!-- /.end row -->
                </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <a href="<?= $back ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <button type="submit" class="btn btn-primary float-right">Guardar <?= $singular ?> </button>
              </div>
              <!-- /.card-footer -->

            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  const numHoras = "<?=isset($datos_entrada["fecha_funcion"]) ? count($datos_entrada["fecha_funcion"]) : 1 ?>" ; 
</script>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
