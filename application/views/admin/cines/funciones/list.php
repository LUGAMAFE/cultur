<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $plural ?> <i class="<?= $iconoFA ?>"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/cines") ?>">Administración Cines</a></li>
            <li class="breadcrumb-item active"><?= $plural ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card card-secondary">
                        <div class="card-header" style="marginbottom: 1rem;">
                            <h3 class="card-title">Tabla <?= $plural ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row justify-content-end">
                            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 d-flex justify-content-end">
                                <a href="<?= $action ?>" class="btn btn-info btn-block btn-md text-white"><i class="fas fa-plus mr-1"></i>Crear <?= $singular ?></a>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-12">
                                <table id="tabla-entradas" class="table table-bordered table-striped dt-responsive">
                                <thead>
                                <tr>
                                    <th>ID Funcion</th>
                                    <th>Pelicula Funcion</th>
                                    <th>Sala Funcion</th>
                                    <th>Idioma Funcion</th>
                                    <th>Dia/s Funcion</th>
                                    <th>Hora/s Funcion</th>
                                    <th>Info</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($entradas)): foreach ($entradas as $entrada):?>
                                        <tr>  
                                            <td> <?= $entrada["id_funcion"];?> </td>
                                            <td> <?= $entrada["nombre_pelicula_funcion"];?> </td>
                                            <td> <?= $entrada["nombre_sala_funcion"];?> </td>
                                            <td> <?= $entrada["idioma-funcion"];?> </td>
                                            <td> <?= $entrada["dia_funcion"];?> </td>
                                            <td> <?= $entrada["fecha_funcion"];?> </td>
                                            <td>
                                                <div class="row justify-content-center">
                                                    <div class="col-6 d-flex justify-content-center">
                                                        <a href="<?= $editar . "/{$entrada['id_funcion']}" ?>" class="btn btn-info btn-block d-flex justify-content-center align-items-center p-1"><i style="padding-left: 3px;" class="fas fa-edit pr-xl-1"></i> <span class="d-none d-xl-block">Editar</span></a>
                                                    </div>                                         
                                                    <div class="col-6 d-flex justify-content-center">
                                                        <a href="<?= $eliminar . "/{$entrada['id_funcion']}" ?>" class="btn btn-danger btn-block d-flex justify-content-center align-items-center p-1"><i style="padding-left: 3px;" class="far fa-trash-alt pr-xl-1"></i> <span class="d-none d-xl-block">Eliminar</span></a>
                                                    </div>                                 
                                                </div> 
                                            </td>
                                        </tr>
                                    <?php endforeach; endif;?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID Funcion</th>
                                    <th>Pelicula Funcion</th>
                                    <th>Sala Funcion</th>
                                    <th>Idioma Funcion</th>
                                    <th>Dia/s Funcion</th>
                                    <th>Hora/s Funcion</th>
                                    <th>Info</th>
                                </tr>
                                </tfoot>
                                </table>
                            </div>
                            <!-- /.col -->
                            </div>
                            <!-- /.row --> 
                        </div>
                        <!-- /.card-body -->
                        <!-- <div class="overlay">
                            <div class="wrap">
                                <i class="loader rotating-infinite fas fa-sync-alt"></i>
                            </div>
                        </div> -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>


<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>