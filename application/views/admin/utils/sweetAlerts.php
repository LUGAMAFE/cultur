<script>
  $(document ).ready(function() {
    <?php $mensaje = $this->session->flashdata("admin-message"); ?>

    <?php if( isset( $mensaje) ):?>
        <?php if($mensaje["jBox"]):
            switch ($mensaje["type"]) {
              case "success":
                $color = "green";
                break;
              case "error":
                $color = "red";
                break;
              case "warning":
                $color = "yellow";
                break;  
              default:
                $color = "blue";
                break;
            }
          ?>
            new jBox('Notice', {
              <?= is_null($mensaje["title"]) ? "" : "title: '".$mensaje["description"]."',"; ?>
              content: '<?= $mensaje["message"]; ?>',
              <?= is_null($mensaje["description"]) ? "" : "footer: '".$mensaje["title"]."',"; ?>
              color: '<?= $color?>',
              showCountdown: true,
              autoClose: 3500,
              delayOnHover: true,
              attributes: {y: 'bottom'}});
        <?php else: ?>
            Swal.fire({
              <?= is_null($mensaje["title"]) ? "" : "title: '".$mensaje["title"]."',"; ?>
              type: '<?= $mensaje["type"]; ?>',
              text: '<?= $mensaje["message"]; ?>',
              <?= is_null($mensaje["description"]) ? "" : "footer: '".$mensaje["description"]."',"; ?>
              <?= ($mensaje["type"] === "success") ? "timer: 2800, showConfirmButton: false," : ""; ?>
            })
        <?php $this->session->unset_userdata("admin-message"); ?>
        <?php endif; ?>
    <?php endif; ?>
  });
</script>