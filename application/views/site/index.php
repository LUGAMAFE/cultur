<section class="section video-home-section first-section">
    <video loop muted data-autoplay preload="auto">
        <source src="assets/img/Home/banner princi/Banner.mp4" type="video/mp4" poster="assets/rescaled-img/Home/banner princi/banner-1000px.png">
    </video>
    <div class="layer">
        <div class="img-extra">
            <img src="assets/img/Home/banner princi/1.svg">
        </div>
    </div>
</section>

<section class="section image-home-section">
    <picture>
        <source sizes="100vw" type="image/webp" data-srcset="
            assets/webp-img/Home/patronato/01-300px.webp 320w,
            assets/webp-img/Home/patronato/01-600px.webp 700w,
            assets/webp-img/Home/patronato/01-1000px.webp 1100w,
            assets/webp-img/Home/patronato/01-1400px.webp 1500w,
            assets/webp-img/Home/patronato/01-1900px.webp 1900w" />
        <img sizes="100vw" class="lazyload" data-srcset="
            assets/rescaled-img/Home/patronato/01-300px.jpg 320w,
            assets/rescaled-img/Home/patronato/01-600px.jpg 700w,
            assets/rescaled-img/Home/patronato/01-1000px.jpg 1100w,
            assets/rescaled-img/Home/patronato/01-1400px.jpg 1500w,
            assets/rescaled-img/Home/patronato/01-1900px.jpg 1900w" data-src="assets/img/Home/patronato/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
    </picture>
    <div class="layer">
        <a href="cultur" class="texto">
            <h1>Patronato de las unidades culturales del estado de Yucatán cultur</h1>
        </a>
    </div>
</section>

<section class="section image-home-section">
    <picture>
        <source sizes="100vw" type="image/webp" data-srcset="
            assets/webp-img/Home/siglo%20xxi/1-300px.webp 320w,
            assets/webp-img/Home/siglo%20xxi/1-600px.webp 700w,
            assets/webp-img/Home/siglo%20xxi/1-1000px.webp 1100w,
            assets/webp-img/Home/siglo%20xxi/1-1400px.webp 1500w,
            assets/webp-img/Home/siglo%20xxi/1-1900px.webp 1900w" />
        <img sizes="100vw" data-srcset="
            assets/rescaled-img/Home/siglo%20xxi/1-300px.jpg 320w,
            assets/rescaled-img/Home/siglo%20xxi/1-600px.jpg 700w,
            assets/rescaled-img/Home/siglo%20xxi/1-1000px.jpg 1100w,
            assets/rescaled-img/Home/siglo%20xxi/1-1400px.jpg 1500w,
            assets/rescaled-img/Home/siglo%20xxi/1-1900px.jpg 1900w" data-srcs="assets/img/Home/siglo%20xxi/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
    </picture>
    <div class="layer">
        <a href="centro-convenciones-sigloxxi" class="img-extra">
            <img class="lazyload" data-src="assets/img/Home/siglo%20xxi/1.svg">
        </a>
    </div>
</section>

<section class="section image-home-section">
    <picture>
        <source sizes="100vw" type="image/webp" data-srcset="
            assets/webp-img/Home/cines%20siglo%20xxi/1-300px.webp 320w,
            assets/webp-img/Home/cines%20siglo%20xxi/1-600px.webp 700w,
            assets/webp-img/Home/cines%20siglo%20xxi/1-1000px.webp 1100w,
            assets/webp-img/Home/cines%20siglo%20xxi/1-1400px.webp 1500w,
            assets/webp-img/Home/cines%20siglo%20xxi/1-1900px.webp 1900w" />
        <img sizes="100vw" data-srcset="
            assets/rescaled-img/Home/cines%20siglo%20xxi/1-300px.jpg 320w,
            assets/rescaled-img/Home/cines%20siglo%20xxi/1-600px.jpg 700w,
            assets/rescaled-img/Home/cines%20siglo%20xxi/1-1000px.jpg 1100w,
            assets/rescaled-img/Home/cines%20siglo%20xxi/1-1400px.jpg 1500w,
            assets/rescaled-img/Home/cines%20siglo%20xxi/1-1900px.jpg 1900w" data-srcs="assets/img/Home/cines%20siglo%20xxi/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
    </picture>
    <div class="layer">
        <a href="cines-sigloxxi" class="img-extra">
            <img class="lazyload" data-src="assets/img/Home/cines%20siglo%20xxi/1.svg">
        </a>
        <a class="btn gold-btn btn-effect" href="cines-sigloxxi">
            Ver Cartelera 
            <img class="lazyload" data-src="assets/img/Home/cines%20siglo%20xxi/palomitas.svg">
        </a>
    </div>
</section>

<section class="section video-home-section">
    <video loop muted data-autoplay preload="none" poster="assets/rescaled-img/Home/paradores/paradores-1000px.png">
        <source src="assets/img/Home/paradores/Paradores.mp4" type="video/mp4">
    </video>
    <div class="layer">
        <a href="paradores" class="texto">
            <h1>CONOCE NUESTROS PARADORES TURÍSTICOS</h1>
        </a>
    </div>
</section>

<section class="section image-home-section">
    <picture>
        <source sizes="100vw" type="image/webp" data-srcset="
            assets/webp-img/Home/kukulcan/02-300px.webp 320w,
            assets/webp-img/Home/kukulcan/02-600px.webp 700w,
            assets/webp-img/Home/kukulcan/02-1000px.webp 1100w,
            assets/webp-img/Home/kukulcan/02-1400px.webp 1500w,
            assets/webp-img/Home/kukulcan/02-1900px.webp 1900w" />
        <img sizes="100vw" data-srcset="
            assets/rescaled-img/Home/kukulcan/02-300px.jpg 320w,
            assets/rescaled-img/Home/kukulcan/02-600px.jpg 700w,
            assets/rescaled-img/Home/kukulcan/02-1000px.jpg 1100w,
            assets/rescaled-img/Home/kukulcan/02-1400px.jpg 1500w,
            assets/rescaled-img/Home/kukulcan/02-1900px.jpg 1900w" data-src="assets/img/Home/kukulcan/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
    </picture>
    <div class="layer">
        <a href="https://nochesdekukulkan.com" class="texto">
            <h1>NOCHES DE KUKULCÁN </h1>
        </a>
    </div>
</section>