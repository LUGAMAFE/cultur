<section class="politicas section-padding">
    <div class="titulo-section gold-text">
        Políticas de Privacidad
    </div>
    
    <h3 class="gold-text">Manejo de datos personales</h3>

    <p>En cumplimiento con lo establecido por la Ley Federal de Transparencia y Acceso a la Información Pública Gubernamental, su reglamento, así como lo señalado en los Lineamientos de Protección de Datos Personales, se establece el siguiente compromiso:</p>

    <p>Los datos personales que se ingresen en el formulario de contacto, no serán difundidos, distribuidos o comercializados. Únicamente podrán ser proporcionados a terceros de acuerdo con lo estrictamente señalado en los Art. 23 y 24 de la Ley de Acceso a la Información Pública para el Estado y los Municipios de Yucatán (LAIP), para lo cual, el Patronato de las Unidades de Servicios Culturales y Turísticos del Estado de Yucatán, se compromete a tratar dicha información, de conformidad con los principios de licitud, calidad, acceso y corrección de información, seguridad, custodia, y consentimiento para su transmisión debiendo obedecer exclusivamente:</p>

    <p>La solicitud de corrección parcial o total de datos personales, sólo podrá ser formulada por el interesado titular de los mismos o su representante legal debidamente acreditados, de conformidad con lo establecido en los Lineamientos de Protección de Datos Personales, mismos que deberán observar las dependencias y entidades de la Administración Pública Federal en la recepción, procesamiento, trámite, resolución y notificación de las solicitudes de corrección de datos personales que formulen los particulares.</p>

    <p>La información que nos proporcione mediante el llenado de formularios publicados en esta página, aplicación Redescubre Yucatan o a través de la aplicación denominada Whatsapp, puede ser incluida dentro de los informes que se elaboran para el seguimiento de avances institucionales del Patronato de las Unidades de Servicios Culturales y Turísticos del Estado de Yucatán, los cuales serán meramente estadísticos y no incluirán información que permita identificarle en lo individual. Los datos personales que proporcione en los formularios del sitio web oficial <a href="#">www.culturyucatan.com</a>,  aplicación Redescubre Yucatan o a través de la aplicación denominada Whatsapp del Patronato de las Unidades de Servicios Culturales y Turísticos del Estado de Yucatán, serán protegidos en términos del artículo 4 fracción III, y demás correlativos de la Ley Federal de Transparencia y Acceso a la Información Pública Gubernamental, así como los artículos 37, 38, 40, 41, 47 y 48 de su Reglamento, y los Lineamientos de Protección de Datos Personales; y los Artículos 20, 21 y 22 de la Ley de Acceso a la Información Pública para el Estado y los Municipios de Yucatán (LAIP).</p>

    <p class="strong">DATOS Y FINALIDAD.</p>

    <p>Los Datos personales que le sean solicitados serán tratados con la siguiente finalidad:</p>

    <ol>
        <li> Recibir la Cartelera en el Correo o Número celular que nos proporcione. </li>

        <li> Invitarlo a participar en nuestros Eventos </li>

        <li> Enviarle promociones de los Cines Siglo XXI </li>
    </ol>

    <p>El Patronato compromete a su personal que tiene acceso a datos personales en el ejercicio de sus funciones o intervención en cualquier fase del tratamiento, a mantener confidencialidad respecto de dicha información. El Patronato informará a través del sitio web oficial <a href="#">www.culturyucatan.com</a>, sobre cualquier cambio a la Política de Privacidad y Manejo de Datos Personales. Se sugiere visitar con frecuencia esta área del sitio, a fin de mantenerse oportunamente informado. Si hubiera algún cambio de material en el tipo de información que recogemos o en la manera que utilizamos tal información, obtendremos su permiso antes de realizar cualquier alteración.</p>

    <p>La presente política de privacidad se limita a toda la información publicada bajo el nombre o dominio oficial www.culturyucatan.com, aplicación Redescubre Yucatán o a través de la aplicación Whatsapp.</p>
</section>