<section class="banner-principal banner-70">
    <div class="img-banner">
        <div class="info-banner">
            <div class="info-position">
                <div class="texto">
                    <h1>Salón Uxmal 2</h1>
                </div>
            </div>
        </div>
        <div class="filtro"></div>
        <picture>
            <source sizes="100vw" type="image/webp" data-srcset="
                assets/webp-img/Salones/Uxmal2/1-300px.webp 320w,
                assets/webp-img/Salones/Uxmal2/1-600px.webp 700w,
                assets/webp-img/Salones/Uxmal2/1-1000px.webp 1100w,
                assets/webp-img/Salones/Uxmal2/1-1400px.webp 1500w,
                assets/webp-img/Salones/Uxmal2/1-1900px.webp 1900w" />
            <img sizes="100vw" class="lazyload" data-srcset="
                assets/rescaled-img/Salones/Uxmal2/1-300px.jpg 320w,
                assets/rescaled-img/Salones/Uxmal2/1-600px.jpg 700w,
                assets/rescaled-img/Salones/Uxmal2/1-1000px.jpg 1100w,
                assets/rescaled-img/Salones/Uxmal2/1-1400px.jpg 1500w,
                assets/rescaled-img/Salones/Uxmal2/1-1900px.jpg 1900w" data-src="assets/img/Salones/Uxmal2/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section>

<section class="informacion-salon section-padding">
    <div class="info">
        <p>El salón Uxmal 2 cuenta con grandes espacios ideales para exposiciones y convenciones de gran público.</p>
        <p class="titulo-servicios">SERVICIOS</p> 
        <p>El salón Uxmal 2 cuenta con una altura máxima de 5 metros.</p> 
        <ul>
            <li>Energía eléctrica (iluminación general).</li>
            <li>Aire acondicionado.</li>
            <li>Limpieza y seguridad (áreas comunes).</li>
            <li>Señalamiento institucional.</li>
            <li>Equipo contra incendio.</li>
            <li>Planta de emergencia (luces).</li>
            <li>Estacionamiento.</li>
            <li>Sanitarios.</li>
        </ul>
    </div>

    <div class="imagen-salon">
        <picture>
            <source sizes="100vw" type="image/webp" data-srcset="
                assets/webp-img/Salones/Uxmal2/1-300px.webp 320w,
                assets/webp-img/Salones/Uxmal2/1-600px.webp 700w,
                assets/webp-img/Salones/Uxmal2/1-1000px.webp 1100w,
                assets/webp-img/Salones/Uxmal2/1-1400px.webp 1500w,
                assets/webp-img/Salones/Uxmal2/1-1900px.webp 1900w" />
            <img sizes="100vw" class="lazyload" data-srcset="
                assets/rescaled-img/Salones/Uxmal2/1-300px.jpg 320w,
                assets/rescaled-img/Salones/Uxmal2/1-600px.jpg 700w,
                assets/rescaled-img/Salones/Uxmal2/1-1000px.jpg 1100w,
                assets/rescaled-img/Salones/Uxmal2/1-1400px.jpg 1500w,
                assets/rescaled-img/Salones/Uxmal2/1-1900px.jpg 1900w" data-src="assets/img/Salones/Uxmal2/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section>

<section class="detalles-salon b-gray">
    <div class="contenido section-padding">
        <h2 class="title gold-text">DETALLES DEL SALÓN</h2>
        <div class="detalles">
            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/size.svg">
                </div>
                <p class="titulo-detalle">Área</p>
                <p class="medida-1">738 m2</p>
                <p class="medida-2">7943.7 ft2</p>
            </div>

            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/area.svg">
                </div>
                <p class="titulo-detalle">Dimensiones</p>
                <p class="medida-1">18 x 41 mts</p>
                <p class="medida-2">59 x 134.5 ft</p>
            </div>

            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/hall.svg">
                </div>
                <p class="titulo-detalle">Auditorio</p>
                <p class="medida-1">800</p>
                <p class="medida-2">Personas</p>
            </div>

            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/martini.svg">
                </div>
                <p class="titulo-detalle">Banquete</p>
                <p class="medida-1">600</p>
                <p class="medida-2">Personas</p>
            </div>
        </div>
    </div>
</section>

<section class="slider-section section-padding">
    <div class="slider-container-section">
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Uxmal2/1.jpg">
        </div>
    </div>
</section>

<!-- <section class="zonas-siglo">
    <div class="titulo-section gold-text section-padding-x">Conozca nuestros espacios</div>
    <div class="barra"></div>

    <div class="imagen-siglo">
        <picture>
            <source sizes="75vw" type="image/webp" data-srcset="
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-300px.webp 320w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-600px.webp 700w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1000px.webp 1100w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1400px.webp 1500w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1900px.webp 1900w" />
            <img sizes="75vw" class="js-tilt lazyload" data-srcset="
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-300px.png 320w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-600px.png 700w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1000px.png 1100w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1400px.png 1500w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/captura.png" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section> -->