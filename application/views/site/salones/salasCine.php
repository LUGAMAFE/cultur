<section class="banner-principal banner-70">
    <div class="img-banner">
        <div class="info-banner">
            <div class="info-position">
                <div class="texto">
                    <h1>Salas de cine</h1>
                </div>
            </div>
        </div>
        <div class="filtro"></div>
        <picture>
            <source sizes="100vw" type="image/webp" data-srcset="
                assets/webp-img/Salones/Cines/1-300px.webp 320w,
                assets/webp-img/Salones/Cines/1-600px.webp 700w,
                assets/webp-img/Salones/Cines/1-1000px.webp 1100w,
                assets/webp-img/Salones/Cines/1-1400px.webp 1500w,
                assets/webp-img/Salones/Cines/1-1900px.webp 1900w" />
            <img sizes="100vw" class="lazyload" data-srcset="
                assets/rescaled-img/Salones/Cines/1-300px.jpg 320w,
                assets/rescaled-img/Salones/Cines/1-600px.jpg 700w,
                assets/rescaled-img/Salones/Cines/1-1000px.jpg 1100w,
                assets/rescaled-img/Salones/Cines/1-1400px.jpg 1500w,
                assets/rescaled-img/Salones/Cines/1-1900px.jpg 1900w" data-src="assets/img/Salones/Cines/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section>

<section class="informacion-salon section-padding">
    <div class="info">
        <p>Contamos con seis salas de cine con capacidad de 179, 184 y 202 personas, ideales para presentaciones, conferencias o proyecciones particulares.</p>
        <p class="titulo-servicios">SERVICIOS</p> 
        <p>Las salas de cine incluyen:</p> 
        <ul>
            <li>Energía eléctrica (iluminación general).</li>
            <li>Sistema de sonido.</li>
            <li>Asientos tipo butaca.</li>
            <li>Aire acondicionado.</li>
            <li>Limpieza y seguridad (áreas comunes).</li>
            <li>Señalamiento institucional.</li>
            <li>Equipo contra incendio.</li>
            <li>Planta de emergencia (luces).</li>
            <li>Sanitarios.</li>
        </ul>
    </div>

    <div class="imagen-salon">
        <picture>
            <source sizes="100vw" type="image/webp" data-srcset="
                assets/webp-img/Salones/Cines/1-300px.webp 320w,
                assets/webp-img/Salones/Cines/1-600px.webp 700w,
                assets/webp-img/Salones/Cines/1-1000px.webp 1100w,
                assets/webp-img/Salones/Cines/1-1400px.webp 1500w,
                assets/webp-img/Salones/Cines/1-1900px.webp 1900w" />
            <img sizes="100vw" class="lazyload" data-srcset="
                assets/rescaled-img/Salones/Cines/1-300px.jpg 320w,
                assets/rescaled-img/Salones/Cines/1-600px.jpg 700w,
                assets/rescaled-img/Salones/Cines/1-1000px.jpg 1100w,
                assets/rescaled-img/Salones/Cines/1-1400px.jpg 1500w,
                assets/rescaled-img/Salones/Cines/1-1900px.jpg 1900w" data-src="assets/img/Salones/Cines/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section>

<section class="detalles-salon b-gray">
    <div class="contenido section-padding">
        <h2 class="title gold-text">DETALLES DEL SALÓN</h2>
        <div class="detalles">
            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/size.svg">
                </div>
                <p class="titulo-detalle">Área</p>
                <p class="medida-1">--- m2</p>
                <p class="medida-2">---- ft2</p>
            </div>

            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/area.svg">
                </div>
                <p class="titulo-detalle">Dimensiones</p>
                <p class="medida-1">--- mts</p>
                <p class="medida-2">--- ft</p>
            </div>

            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/hall.svg">
                </div>
                <p class="titulo-detalle">Auditorio</p>
                <p class="medida-1">De 179 hasta 202</p>
                <p class="medida-2">Personas</p>
            </div>

            <div class="detalle">
                <div class="icono">
                    <img class="svg" src="assets/img/Salones/Iconos/martini.svg">
                </div>
                <p class="titulo-detalle">Banquete</p>
                <p class="medida-1">No disponible</p>
                <p class="medida-2">---</p>
            </div>
        </div>
    </div>
</section>

<section class="slider-section section-padding">
    <div class="slider-container-section">
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Cines/1.jpg">
        </div>
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Cines/2.jpg">
        </div>
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Cines/3.jpg">
        </div>
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Cines/4.jpg">
        </div>
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Cines/5.jpg">
        </div>
        <div class="slide-content">
            <img class="parallax-right" src="assets/img/Salones/Cines/6.jpg">
        </div>
    </div>
</section>

<!-- <section class="zonas-siglo">
    <div class="titulo-section gold-text section-padding-x">Conozca nuestros espacios</div>
    <div class="barra"></div>

    <div class="imagen-siglo">
        <picture>
            <source sizes="75vw" type="image/webp" data-srcset="
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-300px.webp 320w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-600px.webp 700w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1000px.webp 1100w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1400px.webp 1500w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1900px.webp 1900w" />
            <img sizes="75vw" class="js-tilt lazyload" data-srcset="
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-300px.png 320w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-600px.png 700w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1000px.png 1100w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1400px.png 1500w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/captura.png" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section> -->