<section class="pelicula-full-info b-gray">
    <div class="informacion section-padding">
        <?php foreach ($funciones as $funcion): ?>
            <div class="poster-container">
                <figure class="poster">
                    <div class="caratula">
                        <img src="<?= $funcion["datos_pelicula"]["imagen_pelicula"]["full_route_file"] ?>">
                    </div>
                </figure>
            </div>

            <div class="info-pelicula">
                    <h3 class="title gold-text"><?= $funcion["datos_pelicula"]["nombre_pelicula"] ?></h3>
                    <p class="clasificacion">Clasificación: <span><?= $funcion["datos_pelicula"]["clasificacion_pelicula"] ?></span></p>

                    <p class="duracion">Duración: <span><?= $funcion["datos_pelicula"]["duracion_pelicula"] ?></span></p>

                    <p class="sinopsis"><?= $funcion["datos_pelicula"]["sinopsis_pelicula"] ?></p>

                    <div class="horarios">
                        <div class="funciones"><span>Funciones</span></div>
                        <div class="bloque">
                            <?php foreach ($funcion["idiomas"] as $idioma): ?>
                                <p class="nombre-idioma"> <?= $idioma["idioma"]?> </p>
                                <?php foreach ($idioma["salas"] as $sala): ?>
                                    <div class="bloque-sala">
                                        <p class="nombre-sala"><?= $sala["nombre_sala_funcion"] ?></p>
                                        <div class="horas">
                                            <?php foreach ($sala["horas_funcion"] as $hora): ?>
                                                <div href="#" class="hora"><?= $hora ?></div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
            </div>

            <div class="youtube-trailer">
                <p class="title gold-text"> Trailer Pelicula</p>
                <div class="iframe-container">
                    <iframe src="https://www.youtube-nocookie.com/embed/<?= $funcion["datos_pelicula"]["id_trailer_pelicula"] ?>?rel=0&iv_load_policy=3&color=white&disablekb=1" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>