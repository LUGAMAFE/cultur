<section class="banner-principal banner-centro">
    <div class="img-banner">
        <div class="info-banner">
            <div class="info-position">
                <div class="img-extra">
                    <img src="assets/img/SVG/Logo Siglo XXI.svg">
                </div>
            </div>
        </div>
        <!-- <div class="filtro"></div> -->

        <picture>
            <source sizes="100vw" type="image/webp" srcset="
                assets/webp-img/Centro%20Siglo%20XXI/Banner/banner-300px.webp 320w,
                assets/webp-img/Centro%20Siglo%20XXI/Banner/banner-600px.webp 700w,
                assets/webp-img/Centro%20Siglo%20XXI/Banner/banner-1000px.webp 1100w,
                assets/webp-img/Centro%20Siglo%20XXI/Banner/banner-1400px.webp 1500w,
                assets/webp-img/Centro%20Siglo%20XXI/Banner/banner-1900px.webp 1900w" />
            <img sizes="100vw" srcset="
                assets/rescaled-img/Centro%20Siglo%20XXI/Banner/banner-300px.jpg 320w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Banner/banner-600px.jpg 700w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Banner/banner-1000px.jpg 1100w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Banner/banner-1400px.jpg 1500w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Banner/banner-1900px.jpg 1900w" src="assets/img/Centro%20Siglo%20XXI/Banner/banner.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section>

<section class="info-centro-siglo section-padding">
    <span class="info">El centro de convenciones y exposiciones Yucatán Siglo XXI está ubicado al norte de la cuidad de Mérida, a 20 minutos del Centro y a 10 minutos de los principales hoteles. Este edificio evoca la grandeza de la arquitectura maya.</span>
    <div class="grecas">
        <img src="assets/img/Centro%20Siglo%20XXI/Descripcion/Grecas.svg">
    </div>
    <div class="full-description">
        <p>Mérida es un centro promotor del desarrollo regional en el sur de México, pocos destinos reúnen tantas ventajas como Mérida: una inmejorable ubicación geográfica, completísimas vías de comunicación y acceso.</p>
        <p>Por ello es importante que la ciudad cuente con infraestructura de servicios y esparcimiento para ofertar un abanico de opciones de recreación que impacten positivamente en la calidad de vida de la gente.</p>
        <p>El Centro de Convenciones y Exposiciones Yucatán Siglo XXI fue inaugurado en el año de 1997 por el ex presidente Ernesto Zedillo Ponce de León durante la gestión del entonces Gobernador Víctor Cervera Pacheco.</p>
        <p>Los eventos que alberga el Centro de Convenciones, incluidas las proyecciones cinematográficas forman parte de la amplia oferta cultural, turística y de entretenimiento que existe en la capital yucateca.</p>
    </div>
</section>

<section class="salones b-gray section-padding">
    <div class="titulo-section"><h2 class="gold-text">Salones</h2></div>
    <div class="salones-list">
        <a href="centro-convenciones-sigloxxi/salon/salon-chichen-itza" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> CHICHÉN ITZÁ</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%16-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2016-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2016.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salon-uxmal" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> UXMAL</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2018-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2018.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salon-uxmal-1" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> UXMAL 1</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2019-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2019.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salon-uxmal-2" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> UXMAL 2</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2020-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2020.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salon-uxmal-3" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> UXMAL 3</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2021-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2021.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salon-uxmal-4" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> UXMAL 4</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2022-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2022.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="centro-convenciones-sigloxxi/salon/salon-ek-balam" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> EK BALAM</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2023-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2023.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salon-dzibilchaltun" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALÓN <br> DZIBILCHALTUN</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2025-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2025.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
         <a href="centro-convenciones-sigloxxi/salon/salas-de-cine" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        <span>SALAS <br> DE CINE</span>
                    </div>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-300px.webp 320w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-600px.webp 700w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-1000px.webp 1100w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-1400px.webp 1500w,
                        assets/webp-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-300px.png 320w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-600px.png 700w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-1000px.png 1100w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-1400px.png 1500w,
                        assets/rescaled-img/Centro%20Siglo%20XXI/Salas/Recurso%2024-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/Recurso%2024.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
    </div>
</section>

<!-- <section class="zonas-siglo">
    <div class="titulo-section gold-text section-padding-x">Conozca nuestros espacios</div>
    <div class="barra"></div>

    <div class="imagen-siglo">
        <picture>
            <source sizes="75vw" type="image/webp" data-srcset="
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-300px.webp 320w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-600px.webp 700w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1000px.webp 1100w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1400px.webp 1500w,
                assets/webp-img/Centro%20Siglo%20XXI/Salas/captura-1900px.webp 1900w" />
            <img sizes="100vw" class="js-tilt lazyload" data-srcset="
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-300px.png 320w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-600px.png 700w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1000px.png 1100w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1400px.png 1500w,
                assets/rescaled-img/Centro%20Siglo%20XXI/Salas/captura-1900px.png 1900w" data-src="assets/img/Centro%20Siglo%20XXI/Salas/captura.png" alt="Aquí va un texto lo suficientemente descriptivo">
        </picture>
    </div>
</section> -->