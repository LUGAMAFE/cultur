<section class="cuenta section-padding">
    <div class="titulo-section gold-text">
        CUENTA PÚBLICA
    </div>

    <div class="anios">
        <div class="ano">
            <div class="titulo gold-text">2019</div>
            <a target="_blank" href="https://drive.google.com/file/d/1E8dgHa3Sa_6MyhmcMbzxGu68G_jidnKL/view">1º Trimestre</a>
            <a target="_blank" href="https://drive.google.com/file/d/1Z4UnLDnk9KxcNhMCH4_1a_nQ8zUm7f6Z/view">2º Trimestre</a>
            <a target="_blank" href="https://drive.google.com/file/d/1oRGPX_E6d4TdYNtQF0asmScYT2xbmZnu/view">3º Trimestre</a>
            <a target="_blank" href="https://drive.google.com/file/d/1EB2xR3fe4Qc6T5-IU3kwysyiErDh5BVC/view">4º Trimestre</a>
        </div>
        <div class="ano">
            <div class="titulo gold-text">2020</div>

        </div>
        <div class="ano">
            <div class="titulo gold-text">2021</div>

        </div>
        <div class="ano">
            <div class="titulo gold-text">2022</div>

        </div>
    </div>
</section>    