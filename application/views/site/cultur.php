<section class="bannerSlider">
    <div class="slider-banner">
        <div class="banner">
            <picture>
                <source sizes="70vw" type="image/webp" srcset="
                    assets/webp-img/Cultur/Galeria/1-300px.webp 320w,
                    assets/webp-img/Cultur/Galeria/1-600px.webp 700w,
                    assets/webp-img/Cultur/Galeria/1-1000px.webp 1100w,
                    assets/webp-img/Cultur/Galeria/1-1400px.webp 1500w,
                    assets/webp-img/Cultur/Galeria/1-1900px.webp 1900w" />
                <img sizes="70vw" srcset="
                    assets/rescaled-img/Cultur/Galeria/1-300px.jpg 320w,
                    assets/rescaled-img/Cultur/Galeria/1-600px.jpg 700w,
                    assets/rescaled-img/Cultur/Galeria/1-1000px.jpg 1100w,
                    assets/rescaled-img/Cultur/Galeria/1-1400px.jpg 1500w,
                    assets/rescaled-img/Cultur/Galeria/1-1900px.jpg 1900w" src="assets/img/Cultur/Galeria/1.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
            </picture>
        </div>
        <div class="banner">
            <picture>
                <source sizes="70vw" type="image/webp" data-srcset="
                    assets/webp-img/Cultur/Galeria/2-300px.webp 320w,
                    assets/webp-img/Cultur/Galeria/2-600px.webp 700w,
                    assets/webp-img/Cultur/Galeria/2-1000px.webp 1100w,
                    assets/webp-img/Cultur/Galeria/2-1400px.webp 1500w,
                    assets/webp-img/Cultur/Galeria/2-1900px.webp 1900w" />
                <img sizes="70vw"  class="lazyload" data-srcset="
                    assets/rescaled-img/Cultur/Galeria/2-300px.jpg 320w,
                    assets/rescaled-img/Cultur/Galeria/2-600px.jpg 700w,
                    assets/rescaled-img/Cultur/Galeria/2-1000px.jpg 1100w,
                    assets/rescaled-img/Cultur/Galeria/2-1400px.jpg 1500w,
                    assets/rescaled-img/Cultur/Galeria/2-1900px.jpg 1900w" data-src="assets/img/Cultur/Galeria/2.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
            </picture>
        </div>
        <div class="banner">
            <picture>
                <source sizes="70vw" type="image/webp" data-srcset="
                    assets/webp-img/Cultur/Galeria/3-300px.webp 320w,
                    assets/webp-img/Cultur/Galeria/3-600px.webp 700w,
                    assets/webp-img/Cultur/Galeria/3-1000px.webp 1100w,
                    assets/webp-img/Cultur/Galeria/3-1400px.webp 1500w,
                    assets/webp-img/Cultur/Galeria/3-1900px.webp 1900w" />
                <img sizes="70vw"  class="lazyload" data-srcset="
                    assets/rescaled-img/Cultur/Galeria/3-300px.jpg 320w,
                    assets/rescaled-img/Cultur/Galeria/3-600px.jpg 700w,
                    assets/rescaled-img/Cultur/Galeria/3-1000px.jpg 1100w,
                    assets/rescaled-img/Cultur/Galeria/3-1400px.jpg 1500w,
                    assets/rescaled-img/Cultur/Galeria/3-1900px.jpg 1900w" data-src="assets/img/Cultur/Galeria/2.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
            </picture>
        </div>
        <div class="banner">
            <picture>
                <source sizes="70vw" type="image/webp" data-srcset="
                    assets/webp-img/Cultur/Galeria/4-300px.webp 320w,
                    assets/webp-img/Cultur/Galeria/4-600px.webp 700w,
                    assets/webp-img/Cultur/Galeria/4-1000px.webp 1100w,
                    assets/webp-img/Cultur/Galeria/4-1400px.webp 1500w,
                    assets/webp-img/Cultur/Galeria/4-1900px.webp 1900w" />
                <img sizes="70vw"  class="lazyload" data-srcset="
                    assets/rescaled-img/Cultur/Galeria/4-300px.jpg 320w,
                    assets/rescaled-img/Cultur/Galeria/4-600px.jpg 700w,
                    assets/rescaled-img/Cultur/Galeria/4-1000px.jpg 1100w,
                    assets/rescaled-img/Cultur/Galeria/4-1400px.jpg 1500w,
                    assets/rescaled-img/Cultur/Galeria/4-1900px.jpg 1900w" data-src="assets/img/Cultur/Galeria/2.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
            </picture>  
        </div>
    </div>
</section>

<section class="patronato section-padding">
    <div class="tamano-banner">
        <div class="titulo-section">
            <h2 class="gold-text">PATRONATO DE LAS UNIDADES CULTURALES Y TURÍSTICAS DEL ESTADO DE YUCATÁN CULTUR</h2>
        </div>
        <p class="desc">Es una entidad y una oficina paraestatal única en su género en el Estado de Yucatán y en la República Mexicana.  Se crea para:</p>

        <div class="razones">
            <div class="razon">
                <div class="icono">
                    <img class="svg gold-icon" src="assets/img/Cultur/iconos/tourism.svg">
                </div>
                <p class="info">
                    Adquirir, edificar y administrar las unidades de servicios turísticos.
                </p>
            </div>
            <div class="razon">
                <div class="icono">
                    <img class="svg gold-icon" src="assets/img/Cultur/iconos/mayan.svg">
                </div>
                <p class="info">
                    La difusión y conocimiento de los valores históricos de la Cultura Maya.
                </p>
            </div>
            <div class="razon">
                <div class="icono">
                    <img class="svg gold-icon" src="assets/img/Cultur/iconos/mexico.svg">
                </div>
                <p class="info">
                    Administra 15 paradores turísticos entre ellos zonas arqueológicas que son Patrimonio de la Humanidad por la UNESCO como Chichén Itzá y Uxmal. Así como centros culturales y ecoturísticos que son reservas naturales protegidas.
                </p>
            </div>
            <div class="razon">
                <div class="icono">
                    <img class="svg gold-icon" src="assets/img/Cultur/iconos/travel.svg">
                </div>
                <p class="info">
                    Trabaja en coordinación con todos los niveles de gobierno para fomentar e impulsar un moderno sistema de promoción y difusión, que permita posicionar a Yucatán y México, a través del turismo cultural.
                </p>
            </div>
            <div class="razon">
                <div class="icono">
                    <img class="svg gold-icon" src="assets/img/Cultur/iconos/Copia de tourism.svg">
                </div>
                <p class="info">
                    Aprovecha los recursos y los espacios arqueológicos y turísticos, con el propósito de establecer unidades de servicios para los visitantes y colaborar en la preservación, conservación y restauración del patrimonio histórico de Yucatán.
                </p>
            </div>
            <div class="razon">
                <div class="icono">
                    <img class="svg gold-icon" src="assets/img/Cultur/iconos/ticket.svg">
                </div>
                <p class="info">
                    Administra el Centro de Convenciones Yucatán Siglo XXI y los cines que este edificio alberga.
                </p>
            </div>
        </div>
    </div>
</section>