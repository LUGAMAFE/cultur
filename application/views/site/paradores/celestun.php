<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO CELESTÚN</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/celestun/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/celestun/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/celestun/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/celestun/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/celestun/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/06-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/06-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/06-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/06-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/celestun/06-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/06-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/06-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/06-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/06-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/celestun/06-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/celestun/06." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
            La Reserva de la Biósfera Celestún se localiza a 109 kilómetros de Mérida, ahí conviven las especies más exóticas de la geografía yucateca, como el flamenco rosa. Los paseos por la ría son una experiencia espectacular que lleva un encuentro con el mar. La playa tiene una belleza incomparable y los restaurantes ofrecen una exquisita variedad de mariscos.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Reserva de la Biosfera Ría Celestún, Municipio de Celestún, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora y 40 minutos.
            <br>Distancia: 110 Km carretera 281 Mérida-Celestún.
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 17:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Embarcaderos.svg">
            </div>
            <p class="info">
                Embarcadero
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paseos en Lancha.svg">
            </div>
            <p class="info">
                Paseos en lancha
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Área de comida rápida
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Área de artesanías
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Interpretacion.svg">
            </div>
            <p class="info">
                Sala de interpretación
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Internet.svg">
            </div>
            <p class="info">
                Internet gratuito
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cajero Automatico.svg">
            </div>
            <p class="info">
                Cajero automático
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paramedicos.svg">
            </div>
            <p class="info">
                Enfermería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Lobby.svg">
            </div>
            <p class="info">
                Vestíbulo
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
    </div>
</section>