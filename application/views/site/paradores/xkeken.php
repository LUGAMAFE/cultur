<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO PARQUE X’KEKÉN</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/06-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/06-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/06-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/06-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/06-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/06-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/06-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/06-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/06-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/06-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/06." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/07-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/07-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/07-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/07-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/07-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/07-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/07-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/07-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/07-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/07-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/07." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/08-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/08-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/08-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/08-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/xkeken/08-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/08-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/08-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/08-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/08-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/xkeken/08-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/xkeken/08." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        A dos kilómetros hacia el oeste de la ciudad heroica de Valladolid se localiza el Parque X´kekén donde puedes disfrutar de un refrescante baño y sumergirte en las milenarias aguas de un cenote maya.
        </p>
        <p>
        El encantador y refrescante Cenote Dzitnup es conocido como La Cueva Azul. El sol atraviesa una pequeña entrada natural en la parte superior de la bóveda, iluminando el interior y creando una imagen sorprendente con el reflejo del agua. Su entrada es estrecha y el descenso es por unas escalinatas bien labradas en roca, las cuales desembocan en un paraje que permite el acceso al único salón de la cavidad. En ella hay un lago de color azul turquesa decorado con estalagmitas, que descienden de la bóveda y duplican su imagen en el agua. La luz que le llega del techo aumenta su majestuosidad. Fue descubierto por un campesino al estar buscando su cerdito que cayó en el cenote. En un principio era conocido como Xkekén, que significa “cerdo” en maya.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Comisaría de Dzitnup, Municipio de Valladolid, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora y 50 minutos
            <br>Distancia: 160 Km carretera 180 Mérida-Cancún
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 19:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paseos a Caballo.svg">
            </div>
            <p class="info">
                Paseo a caballo
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paramedicos.svg">
            </div>
            <p class="info">
                Paramédicos
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cuatrimotos.svg">
            </div>
            <p class="info">
                Paseo en cuatrimoto
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Restaurante
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Bicicletas.svg">
            </div>
            <p class="info">
                Paseo en bicicleta
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Chalecos Salvavidas.svg">
            </div>
            <p class="info">
                Renta de chaleco salvavidas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Lockers.svg">
            </div>
            <p class="info">
                Lockers
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cenote.svg">
            </div>
            <p class="info">
                Cenotes
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Venta de artesanías y ropa típica
            </p>
        </div>
    </div>
</section>