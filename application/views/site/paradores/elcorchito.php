<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO El CORCHITO</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/el%20corchito/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/el%20corchito/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/el%20corchito/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/el%20corchito/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/el%20corchito/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/el%20corchito/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/el%20corchito/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Se encuentra localizado a 36 kilómetros de Mérida, a tan solo 200 metros de la entrada al Puerto de Progreso sobre la carretera que va hacia el Puerto de Chicxulub.
        </p>
        <p>
        Para ingresar se debe tomar  tomar una lancha en parador turístico que te cruzara a través de los manglares. La reserva cuenta con tres cenotes y dos ojos de agua poco profundos ideales para los niños más pequeños. En los alrededores se encuentran áreas para descansar y convivir.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Km. 2 del Boulevard Víctor Manuel Cervera Pacheco, Municipio de Progreso, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 30 minutos
            <br>Distancia: 40 Km carretera 261 Mérida-Progreso
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>09:00 a 17:00 hrs. (Horario de Visita).
            <br>09:00 a 16:00 hrs. (Horario de Taquilla).
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Lobby.svg">
            </div>
            <p class="info">
                Lobby
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paramedicos.svg">
            </div>
            <p class="info">
                Paramédicos
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Mirador.svg">
            </div>
            <p class="info">
                Mirador
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Área de comida rápida
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Internet.svg">
            </div>
            <p class="info">
                Internet gratuito
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Area de Picnic.svg">
            </div>
            <p class="info">
                Área de picnic
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Andadores.svg">
            </div>
            <p class="info">
                Andadores
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Rampas.svg">
            </div>
            <p class="info">
                Rampas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Embarcaderos.svg">
            </div>
            <p class="info">
                Embarcadero
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Senderos Interpretativos.svg">
            </div>
            <p class="info">
                Senderos interpretativos
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cenote.svg">
            </div>
            <p class="info">
                Cenotes costeros
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Área de artesanías
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Areas de descanso.svg">
            </div>
            <p class="info">
                Área de descanso
            </p>
        </div>
    </div>
</section>