<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO CHICHÉN ITZÁ</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chichen%20itza/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chichen%20itza/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chichen%20itza/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chichen%20itza/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chichen%20itza/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chichen%20itza/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chichen%20itza/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        La ciudad es considerada Patrimonio de la Humanidad por la UNESCO en 1988 y una de las Siete Nuevas Maravilla del Mundo desde el 2007. Se localiza al oriente de Yucatán por la carretera a Cancún, a 120 kilómetros de Mérida. La imponencia de la herencia de los itzáes va más allá de la Explanada Principal, el Observatorio, el Cenote Sagrado, el Juego de Pelota y el Templo de las Mil Columnas.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Zona arqueológica de Chichén Itzá. Comisaria Municipal de Pisté, Municipio de Tinum, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora y 30 minutos
            <br>Distancia: 118.7 Km carretera 180 Mérida-Valladolid
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 22:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paqueteria.svg">
            </div>
            <p class="info">
                Paquetería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paramedicos.svg">
            </div>
            <p class="info">
                Enfermería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Guia Turistica.svg">
            </div>
            <p class="info">
                Guías de turistas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Restaurante
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Internet.svg">
            </div>
            <p class="info">
                Internet gratuito
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Modulo de Informacion.svg">
            </div>
            <p class="info">
                Módulo de información turística
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Silla de Ruedas.svg">
            </div>
            <p class="info">
                Servicio de silla de ruedas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Boletos Autobus.svg">
            </div>
            <p class="info">
                Venta de tickets de autobuses
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Area Comercial.svg">
            </div>
            <p class="info">
                Área comercial
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cajero Automatico.svg">
            </div>
            <p class="info">
                Cajero automático
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Telefono Publico.svg">
            </div>
            <p class="info">
                Teléfono Público
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Mercado de artesanías
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taxi.svg">
            </div>
            <p class="info">
                Taxis
            </p>
        </div>
    </div>
</section>