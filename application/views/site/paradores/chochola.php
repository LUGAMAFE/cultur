<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO CHOCHOLÁ</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chochola/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chochola/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chochola/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chochola/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/chochola/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/05-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/05-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/05-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/05-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/chochola/05-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/chochola/05.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
            Chocholá se ubica a 32.5 kilómetros de la ciudad de Mérida y a 40 minutos de Uxmal. En la población se pueden visitar el Templo de la Purísima Concepción y la Ex Hacienda de Chocholá, así como algunos vestigios de asentamientos mayas. Cerca del parador turístico se encuentra el bellísimo cenote San Ignacio o Tuunich Ha. Además, a sólo 20 minutos se ubican las grutas de Calcehtok y la zona arqueológica de Oxkintok.  Del 24 al 30 septiembre se realizan fiestas tradicionales de la localidad.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Municipio de Chocholá, Yucatán.
            <br> Traslado desde la ciudad de Mérida 40 minutos
            <br> Distancia: 40 km carretera 180 Mérida-Campeche
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br> 10:00 a 18:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Área artesanal
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Comida regional
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Lobby.svg">
            </div>
            <p class="info">
                Lobby
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
    </div>
</section>