<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO VALLADOLID</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/valladolid/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/valladolid/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/valladolid/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/valladolid/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/valladolid/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/valladolid/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/valladolid/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Pintoresca ciudad colonial con barrios de gran tradición y belleza. La historia de la ciudad registra eventos de suma importancia como el inicio de la Guerra de Castas en 1847 y la primera chispa de la Revolución Mexicana en 1910. Ahora es conocida como La Capital del Oriente Maya, título merecido por la belleza arquitectónica de sus edificaciones coloniales ancestrales. Sus plazas, barrios y monumentos invitan al visitante a tomarse un tiempo de relajamiento y disfrutar de su deliciosa gastronomía, visitar su mercado de artesanías y a su importante cenote Zací.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Centro Histórico del Pueblo Mágico de Valladolid, Municipio de Valladolid, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora y 45 minutos
            <br>Distancia: 159 Km carretera 180 Mérida-Valladolid
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 21:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Artesanías
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Ropa Tipica.svg">
            </div>
            <p class="info">
                Ropa típica de la región
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Libreria.svg">
            </div>
            <p class="info">
                Librería
            </p>
        </div>
    </div>
</section>