<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO UXMAL</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/06-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/06-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/06-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/06-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/06-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/06-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/06-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/06-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/06-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/06-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/06." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/07-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/07-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/07-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/07-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uxmal/07-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/07-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/07-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/07-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/07-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uxmal/07-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uxmal/07." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Al sur de Yucatán emerge con espectacularidad la zona arqueológica de Uxmal, Declarada Patrimonio de la Humanidad (UNESCO). La arquitectura de “La tres veces construida” es de las más impresionantes de Yucatán.
        </p>
        <p>
        La zona está rodeada de una hilera de cerros que enmarcan la serie de asentamientos caracterizados por la proporción de sus edificios y fachadas bellamente decoradas en filigrana de piedra. Con innegables características estéticas, producto de una cultura que con su sabiduría y extraordinaria precisión del tiempo aún causa fascinación, los mayas de Uxmal muestran cómo la energía social fue canalizada a la construcción con un profundo sentido de la armonía arquitectónica que en esta región se identifica con el nombre de estilo Puuc e incluye a otros sitios como: Kabah, Sayil, Xlapak, Labná y Chacmultún.
        </p>
        <P>
        El núcleo urbano de Uxmal abarca un área de 190 hectáreas y se encuentra rodeado de una muralla. Algunas de las principales edificaciones forman cuadrángulos o plazas cerradas con accesos a través de pasadizos abovedados, como es el caso de los cuadrángulos de Las Monjas, Los Pájaros, El Palomar y El cementerio. Otras construcciones se yerguen sobre plataformas independientes o contiguas, como la Pirámide del Adivino, el Palacio del Gobernador y la Gran Pirámide.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Zona Arqueológica de Uxmal, Municipio de Santa Elena, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora
            <br>Distancia: 83.4 Km carretera 180 Mérida-Campeche y 261 Umán-Muna
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 22:00 hrs.
        </p>
        <p>
            Visita a la Zona Arqueológica
            <br>08:00 a 16:00 hrs.
        </p>
        <p>
            Espectáculo de “Luz y sonido”
            <br>Horario de Invierno: 19:00 hrs
            <br>Horario de Verano: 20:00 hrs
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Interpretacion.svg">
            </div>
            <p class="info">
                Centro de interpretación
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paramedicos.svg">
            </div>
            <p class="info">
                Paramédicos
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cajero Automatico.svg">
            </div>
            <p class="info">
                Cajero automático
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Restaurante
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Mercado de artesanías
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Guia Turistica.svg">
            </div>
            <p class="info">
                Guías de turistas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Area Comercial.svg">
            </div>
            <p class="info">
                Área comercial
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paqueteria.svg">
            </div>
            <p class="info">
                Paquetería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
    </div>
</section>