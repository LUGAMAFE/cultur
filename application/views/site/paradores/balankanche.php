<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO BALANKANCHÉ</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/balankanche/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/balankanche/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/balankanche/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/balankanche/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/balankanche/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/balankanche/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/balankanche/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Es una palabra maya cuyo significado es “Trono del Jaguar”. Para los antiguos mayas era un sitio de mucha importancia, tenía un carácter sagrado porque representaba la entrada al inframundo, lugar donde residían diversas deidades.
        </p>
        <p>
        El pasaje de acceso desciende abruptamente a una profundidad de 10 metros. Las partes explorados de la gruta comprenden más de un kilómetro de senderos.
        </p>
        <p>
        A lo largo de este corredor los pasajes se interrumpen ocasionalmente por grandes cámaras de más o menos 10 metros de altura y 25 de ancho, en las que estalactitas y estalagmitas se unen y forman columnas. En esta gruta durante el recorrido se realiza un espectáculo de luz y sonido para relatar su historia.
        </p>
        <p>
        El Parador Turístico de Balankanché cuenta con un museo con fotografías y cédulas explicativas sobre los rituales sagrados que se practicaban en su interior.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Km 6 de la Carretera Estatal Pisté- Valladolid, Comisaría Municipal de Pisté, Municipio de Tinum, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora y 30 minutos
            <br>Distancia: 124 Km carretera 180 Mérida-Valladolid
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>09:00 a 17:00 hrs.
        </p>
        <p>
            Horarios Ingresos a la Gruta
            <br>- 9:00 hrs Español
            <br>- 10:00 hrs frances
            <br>- 11:00 hrs Inglés
            <br>- 12:00 hrs Español
            <br>- 13:00 hrs Inglés
            <br>- 14:00 hrs Español
            <br>- 15:00 hrs Inglés
            <br>- 16:00 hrs Español
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paqueteria.svg">
            </div>
            <p class="info">
                Paquetería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Museo.svg">
            </div>
            <p class="info">
                Museo
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
    </div>
</section>