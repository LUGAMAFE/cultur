<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO UAYMITÚN</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uaymitun/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uaymitun/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uaymitun/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uaymitun/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/uaymitun/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/uaymitun/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/uaymitun/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        El Parador Turístico de Uaymitún está a 50 Km de la ciudad de Mérida.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Km. 15 Carretera Chicxulub Puerto-Telchac Puerto, Municipio de Ixil, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 40 minutos
            <br>Distancia: 50 Km carretera 261 Mérida-Progreso
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 18:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Renta de Binoculares.svg">
            </div>
            <p class="info">
                Renta de binoculares
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
    </div>
</section>