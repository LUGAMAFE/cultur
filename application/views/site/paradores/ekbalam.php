<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO EK BALAM</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/ek%20balam/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/ek%20balam/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/ek%20balam/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/ek%20balam/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/ek%20balam/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/ek%20balam/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/ek%20balam/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Posee 45 estructuras y está rodeada por dos murallas concéntricas de piedra, más otra que une a los edificios centrales. Cuenta con un juego de pelota y un arco muy hermoso donde desembocaba un sacbé (camino sagrado), que en épocas antiguas conectaba a los reinos mayas; también hay estelas y las llamadas serpientes jeroglíficas, monumentos bellamente labrados en bloques de piedra.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Zona arqueológica de Ek Balam. Comisaria Municipal de Hunukú, Municipio de Temozón, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora y 56 minutos
            <br>Distancia: 175 Km carretera 180 Mérida-Valladolid y 295 Valladolid-Tizimín
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 17:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Guia Turistica.svg">
            </div>
            <p class="info">
                Guías de turistas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Area Comercial.svg">
            </div>
            <p class="info">
                Área comercial
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Artesanias.svg">
            </div>
            <p class="info">
                Área de artesanías
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taxi.svg">
            </div>
            <p class="info">
                Taxis
            </p>
        </div>
    </div>
</section>