<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO DZIBILCHALTUN</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/dzibilchaltun/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/dzibilchaltun/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/dzibilchaltun/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/dzibilchaltun/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/dzibilchaltun/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/dzibilchaltun/06-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/dzibilchaltun/06." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Es considerada una de las ciudades mayas más antiguas su nombre significa “el lugar donde hay escritura en las piedras planas”. Dzibilchaltún cuenta con una capilla franciscana del siglo XVI en medio de la zona arqueológica. El Museo del Pueblo Maya hospeda varios artefactos mayas y españoles, desde objetos de barro hasta pinturas, armaduras y armas españolas, varias estelas mayas, piedras y dinteles tallados en excelentes condiciones. Al interior de la zona hay un bello cenote abierto llamado Xlacah.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Zona arqueológica de Dzibilchaltún. Comisaria Municipal de Dzibilchaltún, Municipio de Mérida, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 30 minutos
            <br>Distancia: 23.2 Km carretera 261 Mérida-Progreso
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>08:00 a 17:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paqueteria.svg">
            </div>
            <p class="info">
                Paquetería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Paramedicos.svg">
            </div>
            <p class="info">
                Enfermería
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Museo.svg">
            </div>
            <p class="info">
                Museo
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Guia Turistica.svg">
            </div>
            <p class="info">
                Guías de turistas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Restaurante
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Internet.svg">
            </div>
            <p class="info">
                Internet gratuito
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Auditorio.svg">
            </div>
            <p class="info">
                Auditorio INAH
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Cenote.svg">
            </div>
            <p class="info">
                Cenote X’laka
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Estacionamiento.svg">
            </div>
            <p class="info">
                Estacionamiento
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Area Comercial.svg">
            </div>
            <p class="info">
                Área comercial
            </p>
        </div>
    </div>
</section>