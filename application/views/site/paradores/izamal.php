<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO IZAMAL</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/izamal/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/izamal/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/izamal/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/izamal/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/izamal/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/izamal/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/izamal/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
        </div>
        <p>
        Está ciudad ha sido desde la época prehispánica un interesante centro ceremonial que según algunos estudiosos, fue fundada en el siglo XIV de nuestra era por el sacerdote Zamná.
        </p>
        <p>
        Está ubicada en el centro de la península a 72 kilómetros de Mérida y a 60 de Chichén Itzá. Es llamada la Ciudad de las Tres Culturas porque en ella conviven tres periodos históricos, los cuales se manifiestan con sus pirámides viendo al cielo en los patios de las casas, testigo de la grandeza de los mayas.
        </p>
        <p>
        El encanto de su Convento deriva del silencio de los majestuosos muros que marcaron por siempre la huella de la influencia española; y con sus calles, edificios, plazoletas, casas e iglesias, todos con un ritmo visual armonioso de un solo color: el amarillo.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Convento de Izamal, Municipio de Izamal, Yucatán.
            <br>Traslado desde la ciudad de Mérida: 1 hora
            <br>Distancia: 67 Km carretera 180 Mérida-Valladolid y 11 Izamal
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            El personal operativo del “Espectáculo de Luz y Sonido” labora de Lunes a Sábado
            <br>16:00 a 22:00 hrs.
        </p>
        <p>
            El espectáculo de luz y sonido “Senderos de Luz” es de Jueves a Sábado
            <br>20:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Taquilla.svg">
            </div>
            <p class="info">
                Taquilla
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
    </div>
</section>