<section class="info-parador section-padding-x">
    <div class="informacion b-gray">
        <h2 class="title gold-text ">PARADOR TURÍSTICO PASAJE PICHETA</h2>
        <div class="imagen-parador">
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/01-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/pasaje%20picheta/01.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/02-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/pasaje%20picheta/02.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/03-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/pasaje%20picheta/03.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-300px.jpg 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-600px.jpg 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-1000px.jpg 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-1400px.jpg 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/04-1900px.jpg 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/pasaje%20picheta/04.jpg" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>
            <!--<div class="imagen-parador-container">
                <picture>
                    <source sizes="(max-width: 1024px) 100vw, 60vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-1900px.webp 1900w" />
                    <img sizes="(max-width: 1024px) 100vw, 60vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-300px. 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-600px. 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-1000px. 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-1400px. 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores/pasaje%20picheta/05-1900px. 1900w" data-src="assets/img/Paradores%20turisticos/Paradores/pasaje%20picheta/05." alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </div>-->
        </div>
        <p>
        El centro comercial Gabriel Gahona “Picheta” está ubicado en los bajos del Palacio de Gobierno en pleno corazón de la ciudad de Mérida.
        </p>
        <p>
        Desde 1639 ya existían estos corredores que rodeaban plaza principal, donde se instauró el primer Ayuntamiento de Mérida y la cárcel publica que funcionó durante 134 años.
        </p>
        <p>
        Litografías del siglo XIX lo muestran como un largo caserón de doble piso con puertas y ventanas parecidas a un castillo por sus remates triangulares.
        </p>
        <p>
        Este pasaje integra la parte turística y de servicios a la comunidad con el patrimonio cultural. En sus espacios se programan eventos artísticos que lo han convertido en punto de encuentro local. Ofreciendo un servicio los 365 días del año.
        </p>
        <h5>UBICACIÓN DEL PARADOR</h5>
        <p>
            Centro Histórico de Mérida, Municipio de Mérida, Yucatán.
        </p>
        <h5>HORARIO DE SERVICIOS</h5>
        <p>
            Lunes a Domingo
            <br>09:00 a 23:00 hrs.
        </p>
    </div>
    <div class="espacio-blanco">
    </div>
</section>

<section class="servicios-section section-padding pb-0">
    <h2 class="titulo-section gold-text">Servicios</h2>
    <div class="servicios">
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Banos.svg">
            </div>
            <p class="info">
                Sanitarios
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Galeria.svg">
            </div>
            <p class="info">
                Galería de exposiciones
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Restaurant.svg">
            </div>
            <p class="info">
                Restaurantes
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Oficinas.svg">
            </div>
            <p class="info">
                Oficinas administrativas
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/Area Comercial.svg">
            </div>
            <p class="info">
                Área comercial
            </p>
        </div>
        <div class="servicio">
            <div class="icono">
                <img class="svg" src="assets/img/Paradores%20turisticos/Servicios/police.svg">
            </div>
            <p class="info">
                Vigilancia
            </p>
        </div>
    </div>
</section>