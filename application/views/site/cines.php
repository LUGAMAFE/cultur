<section class="banner-principal banner-70">
    <div class="img-banner">
        <div class="info-banner">
            <div class="info-position">
                <div class="img-extra">
                    <img class="svg" src="assets/img/Cines Siglo XXI/Banner/1.svg">
                </div>
            </div>
        </div>
        <img src="assets/img/Cines Siglo XXI/Banner/03.jpg">
    </div>
</section>


<section class="promociones-cines section-padding">
    <div class="titulo-section"><h2 class="gold-text">Promociones</h2></div>
    <!-- Swiper -->
    <div class="promociones swiper-container">
        <div class="swiper-wrapper">
        <div class="promocion swiper-slide">
            <img src="assets/img/Cines Siglo XXI/1.png">
        </div>
        <div class="promocion swiper-slide">
            <img src="assets/img/Cines Siglo XXI/1.png">
        </div>
        <div class="promocion swiper-slide">
            <img src="assets/img/Cines Siglo XXI/1.png">
        </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>

<section class="pagina-cartelera section-padding-y">
    <div class="titulo-section"><h2 class="gold-text">Cartelera</h2></div>
    <div class="peliculas">
        <?php foreach ($funciones as $funcion): ?>
            <div class="pelicula b-gray">
                <div class="contenido section-padding">
                    <a href="pelicula<?= "/".$funcion["datos_pelicula"]["id_pelicula"]."/".$funcion["datos_pelicula"]["nombre_pelicula"] ?>" class="poster-container">
                        <div class="clasificacion"> <?= $funcion["datos_pelicula"]["clasificacion_pelicula"] ?> </div>
                        <figure class="poster">
                            <div class="caratula">
                                <img src="<?= $funcion["datos_pelicula"]["imagen_pelicula"]["full_route_file"] ?>">
                            </div>
                        </figure>
                    </a>
                    <a href="pelicula<?= "/".$funcion["datos_pelicula"]["id_pelicula"]."/".$funcion["datos_pelicula"]["nombre_pelicula"] ?>" class="info-pelicula">
                        <div class="clasificacion"> <?= $funcion["datos_pelicula"]["clasificacion_pelicula"] ?></div>
                        <div class="top">
                            <h3 class="title gold-text"><?= $funcion["datos_pelicula"]["nombre_pelicula"] ?></h3>
                            <p class="sinopsis"><?= $funcion["datos_pelicula"]["sinopsis_pelicula"] ?></p>
                        </div>
                        <div href="" class="gold-btn btn-effect vermas">Ver Información pelicula</div>
                        <!-- <p class="idioma-pelicula"><¡= $funcion["idioma-funcion"] ?></p> -->
                    </a>
                    <div class="horarios">
                        <div class="funciones"><span>Funciones</span></div>
                        <div class="bloque">
                            <?php foreach ($funcion["idiomas"] as $idioma): ?>
                                <p class="nombre-idioma"> <?= $idioma["idioma"]?> </p>
                                <?php foreach ($idioma["salas"] as $sala): ?>
                                    <div class="bloque-sala">
                                        <p class="nombre-sala"><?= $sala["nombre_sala_funcion"] ?></p>
                                        <div class="horas">
                                            <?php foreach ($sala["horas_funcion"] as $hora): ?>
                                                <a href="pelicula<?= "/".$funcion["datos_pelicula"]["id_pelicula"]."/".$funcion["datos_pelicula"]["nombre_pelicula"] ?>" class="hora"><?= $hora ?></a>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <!-- <div class="pelicula b-gray">
            <div class="contenido section-padding">
                <div class="clasificacion-container">
                    <div class="clasificacion">B</div>
                </div>
                <div class="poster-container">
                    <figure class="poster">
                        <div class="caratula">
                            <img src="assets/img/Cines Siglo XXI/Peliculas/jojo.jpg">
                        </div>
                    </figure>
                </div>
                <div class="info-pelicula">
                    <h3 class="title gold-text">JoJo Rabbit</h3>
                    <p class="idioma-pelicula">Subtitulada</p>
                </div>
                <div class="horarios">
                    <div class="bloque">
                        <p>Sala 3</p>
                        <div class="horas">
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pelicula b-gray">
            <div class="contenido section-padding">
                <div class="clasificacion-container">
                    <div class="clasificacion">B15</div>
                </div>
                <div class="poster-container">
                    <figure class="poster">
                        <div class="caratula">
                            <img src="assets/img/Cines Siglo XXI/Peliculas/cindy.jpg">
                        </div>
                    </figure>
                </div>
                <div class="info-pelicula">
                    <h3 class="title gold-text">CINDY LA REGIA</h3>
                    <p class="idioma-pelicula">Español</p>
                </div>
                <div class="horarios">
                    <div class="bloque">
                        <p>Sala 4</p>
                        <div class="horas">
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                        </div>
                    </div>
                    <div class="bloque">
                        <p>Sala 5</p>
                        <div class="horas">
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                            <a href="#" class="hora">20:00</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</section>