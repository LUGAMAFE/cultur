<section class="banner-principal banner-70">
    <div class="img-banner">
        <div class="info-banner">
            <div class="info-position">
                <div class="texto">
                    <h1>PARADORES TURÍSTICOS</h1>
                </div>
            </div>
        </div>
        <img class="parallax-right" src="assets/img/Paradores turisticos/Banner/1.jpg">
    </div>
</section>

<section class="info-paradores section-padding">
    <div class="info-texto">
        <p>Mérida es un centro promotor del desarrollo regional en el sur de México, pocos destinos reúnen tantas ventajas como Mérida: una inmejorable ubicación geográfica, completísimas vías de comunicación y acceso.</p>

        <p>Por ello es importante que la ciudad cuente con infraestructura de servicios y esparcimiento para ofertar un abanico de opciones de recreación que impacten positivamente en la calidad de vida de la gente.</p>

        <p>El Centro de Convenciones y Exposiciones Yucatán Siglo XXI fue inaugurado en el año de 1997 por el ex presidente Ernesto Zedillo Ponce de León durante la gestión del entonces Gobernador Víctor Cervera Pacheco.</p>

        <p>Los eventos que alberga el Centro de Convenciones, incluidas las proyecciones cinematográficas forman parte de la amplia oferta cultural, turística y de entretenimiento que existe en la capital yucateca.</p>
    </div>
</section>

<div class="separador section-padding">
    <div class="linea"></div>
</div>

<section class="paradores section-padding">
        <a href="paradores-turisticos/celestun" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>CELESTÚN
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/1-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/1-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/1-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/1-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/1-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/1-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/1-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/1-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/1-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/1-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/1.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/chochola" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>CHOCHOLÁ
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/2-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/2-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/2-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/2-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/2-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/2-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/2-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/2-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/2-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/2-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/2.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/chichen-itza" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>DE <br>CHICHÉN ITZÁ
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/3-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/3-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/3-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/3-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/3-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/3-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/3-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/3-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/3-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/3-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/3.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/izamal" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>DE IZAMAL
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/4-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/4-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/4-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/4-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/4-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/4-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/4-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/4-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/4-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/4-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/4.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/valladolid" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>DE VALLADOLID
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/5-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/5-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/5-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/5-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/5-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/5-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/5-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/5-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/5-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/5-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/5.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/dzibilchaltun" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>DZIBILCHALTUN
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/6-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/6-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/6-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/6-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/6-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/6-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/6-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/6-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/6-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/6-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/6.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/ek-balam" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>EK BALAM
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/7-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/7-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/7-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/7-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/7-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/7-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/7-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/7-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/7-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/7-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/7.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/el-corchito" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>EL CORCHITO
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/8-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/8-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/8-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/8-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/8-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/8-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/8-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/8-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/8-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/8-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/8.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/halacho" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>HALACHÓ
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/9-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/9-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/9-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/9-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/9-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/9-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/9-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/9-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/9-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/9-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/9.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/uaymitun" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>DE UAYMITÚN
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/10-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/10-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/10-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/10-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/10-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/10-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/10-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/10-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/10-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/10-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/10.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/parque-xkeken" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>PARQUE X’KEKÉN
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/11-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/11-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/11-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/11-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/11-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/11-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/11-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/11-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/11-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/11-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/11.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/pasaje-picheta" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>PASAJE <br>PICHETA
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/12-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/12-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/12-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/12-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/12-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/12-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/12-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/12-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/12-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/12-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/12.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/uxmal" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>UXMAL
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/13-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/13-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/13-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/13-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/13-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/13-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/13-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/13-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/13-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/13-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/13.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/loltun" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>DE LOLTÚN
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/14-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/14-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/14-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/14-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/14-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/14-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/14-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/14-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/14-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/14-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/14.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
        <a href="paradores-turisticos/balankanche" class="card">
            <div class="info-card">
                <div class="info-position">
                    <div class="texto">
                        PARADOR <br>TURÍSTICO <br>BALANKANCHÉ
                    </div>
                    <button class="btn-effect vermas">
                        Ver Más</span>    
                    </button>
                </div>
            </div>
            <figure>
                <picture>
                    <source sizes="100vw" type="image/webp" data-srcset="
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/15-300px.webp 320w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/15-600px.webp 700w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/15-1000px.webp 1100w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/15-1400px.webp 1500w,
                        assets/webp-img/Paradores%20turisticos/Paradores%20Card/15-1900px.webp 1900w" />
                    <img sizes="100vw" class="lazyload" data-srcset="
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/15-300px.png 320w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/15-600px.png 700w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/15-1000px.png 1100w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/15-1400px.png 1500w,
                        assets/rescaled-img/Paradores%20turisticos/Paradores%20Card/15-1900px.png 1900w" data-src="assets/img/Paradores%20turisticos/Paradores%20Card/15.png" alt="Aquí va un texto lo suficientemente descriptivo">
                </picture>
            </figure>
        </a>
</section>