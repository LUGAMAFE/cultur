<section class="contacto section <?= (isset($fullpage) && $fullpage) ? 'fullpage-section': 'section-padding'; ?>">
	<div class="full-container">
		<div class="formulario">
			<div class="gold-text title">Contacto</div>
			<form action="#">
				<input type="text" name="nombre" id="nombre"    placeholder="Nombre">
				<input type="tel" name="telefono" id="telefono" placeholder="Teléfono">
				<input type="email" name="email" id="email"     placeholder="Correo">
				<input type="email" name="asunto" id="asunto"   placeholder="Asunto">
				<textarea name="mensaje" id="mensaje"           placeholder="Mensaje"></textarea>
				<button class="gold-btn btn-effect" type="submit"><span>ENVIAR</span></button>
			</form>
		</div>

		<div id="mapa"></div>
	</div>
</section>