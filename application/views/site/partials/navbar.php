<header id="master-header">
  <nav>
    <a class="navbar-imagen" href="#">
      <img src="assets/img/logo-cultur-color.svg" width="200" height="40" alt="" />
    </a>

    <div class="burger">
				<a class="mburger" href="#menu">
					<b></b>
					<b></b>
					<b></b>
				</a>
			</div>

    <div class="menu-colapsable">
      <ul class="navbar-navigator">
        <li class="nav-item <?= ($active === "home") ? "active": ""; ?>">
          <a class="nav-link" href="/">Home</a>
        </li>
        <li class="nav-item <?= ($active === "cultur") ? "active": ""; ?>">
          <a class="nav-link" href="cultur">Cultur</a>
        </li>
        <li class="nav-item <?= ($active === "centroc") ? "active": ""; ?>">
          <a class="nav-link" href="centro-convenciones-sigloxxi">Centro de Convenciones Siglo XXI</a>
        </li>
        <li class="nav-item <?= ($active === "cines") ? "active": ""; ?>">
          <a class="nav-link" href="cines-sigloxxi">Cines Siglo XXI</a>
        </li>
        <li class="nav-item <?= ($active === "paradores") ? "active": ""; ?>">
          <a class="nav-link" href="paradores">Paradores turísticos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Idioma</a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<nav id="menu">
    <ul>
        <li class="nav-item <?= ($active === "home") ? "active": ""; ?>">
          <a href="/">Home</a>
        </li>
        <li class="nav-item <?= ($active === "cultur") ? "active": ""; ?>">
          <a href="cultur">Cultur</a>
        </li>
        <li class="nav-item <?= ($active === "centroc") ? "active": ""; ?>">
          <a href="centro-convenciones-sigloxxi">Centro de Convenciones Siglo XXI</a>
        </li>
        <li class="nav-item <?= ($active === "cines-sigloxxi") ? "active": ""; ?>">
          <a href="cines-sigloxxi">Cines Siglo XXI</a>
        </li>
        <li class="nav-item <?= ($active === "paradores") ? "active": ""; ?>">
          <a href="paradores">Paradores turísticos</a>
        </li>
        <li class="nav-item">
          <a href="#">Idioma</a>
        </li>
    </ul>
</nav>