<footer class="section fp-auto-height <?= (isset($fullpage) && $fullpage) ? 'fullpage-section': 'section-padding'; ?>">
    <div class="full-container">
        <div class="marcas-seguir">
            <a href="https://www.facebook.com/cultur.yucatan" class="marca" title="Facebook Cultur">
                <div class="imagen-marca">
                    <img class="lazyload" data-src="assets/img/Footer/Logo Cultur.svg">
                </div>
                <div class="botones-seguir">
                    <div class="fb-like" data-href="https://www.facebook.com/cultur.yucatan" data-width="1000" data-layout="button_count" data-action="like" data-size="large" data-share="false"></div>
                </div>
            </a>
            <a href="https://www.facebook.com/CentroDeConvencionesYucatanSigloXXI/" class="marca" title="Facebook Centro Convenciones Siglo XXI">
                <div class="imagen-marca">
                    <img class="lazyload" data-src="assets/img/Footer/Logo Centro Siglo XXI.svg">
                </div>
                <div class="botones-seguir">
                    <div class="fb-like" data-href="https://www.facebook.com/CentroDeConvencionesYucatanSigloXXI/" data-width="1000" data-layout="button_count" data-action="like" data-size="large" data-share="false"></div>
                </div>
            </a>
            <a href="https://www.facebook.com/CinesSigloXXI/" class="marca" title="Facebook Cines Siglo XXI">
                <div class="imagen-marca">
                    <img class="lazyload" data-src="assets/img/Footer/Logo Cines Siglo XXI.svg">
                </div>
                <div class="botones-seguir">
                    <div class="fb-like" data-href="https://www.facebook.com/CinesSigloXXI/" data-width="1000" data-layout="button_count" data-action="like" data-size="large" data-share="false"></div>
                </div>
            </a>
        </div>

        <div class="info-footer">
            <div class="bloque">
                <p class="titulo">
                    DIRECCIÓN
                </p>
                <span style="word-break: break-word;">
                    CULTUR <br>
                    C. 60 NORTE NO. 299E EX CORDEMEX, COL. REVOLUCIÓN, MÉRIDA YUCATÁN, MÉXICO. C.P. 97118 <br>
                    CULTUR.DIRGENERAL@YUCATAN.GOB.MX <br> TEL: (999) 942-19-00
                </span>
            </div>

            <div class="bloque">
                <p class="titulo">
                    HORARIO DE OFICINA
                </p>
                <p>
                    LUNES A VIERNES <br> 9:00AM - 6:00PM
                </p>

                <p class="titulo">
                    HORARIO DE CINE
                </p>
                <p>
                    LUNES A DOMINGO
                </p>
            </div>

            <div class="bloque">
                <p class="titulo">
                    SITIOS DE INTERÉS
                </p>

                <p><a href="http://www.yucatan.gob.mx/">GOBIERNO DE YUCATÁN</a></p>
                <p><a href="https://nochesdekukulkan.com/">NOCHES DE KUKULKÁN</a></p>
            </div>
        </div>


        <div class="links-importantes">
            <a class="btn btn-yellow" href="politicas-privacidad">Políticas de privacidad</a>
            <a class="btn btn-yellow" href="http://transparencia.yucatan.gob.mx/cultur">Transparencia</a>
            <a class="btn btn-yellow" href="cuenta-publica">Cuenta pública</a>
            <a class="btn btn-yellow" href="http://www.yucatan.gob.mx/gobierno/detalle.php?id_d=41">Directorio</a>
        </div>
    </div>
</footer>