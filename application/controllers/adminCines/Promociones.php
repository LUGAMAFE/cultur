<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promociones  extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	protected $singular = 'Promocion';
	protected $plural = 'Promociones';
	protected $iconoFA = 'fas fa-money-check-alt';
	
	protected $fineUploaders = array(
		array("name" => "imagen-promocion", "nameDB" => "id_file_img_promocion", "folder" => "../public/assets/img/Cines Siglo XXI/Promociones", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
		$this->load->model("administracion/cines/Promociones_Model");
		$this->load->model("administracion/Archivos_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin $this->plural");
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js("../js/promociones.js");

		$datos["action"] = site_url("administracion/cines/promociones/modificar");
		$datos["back"] = site_url("administracion/cines");

		$datos["is_activo"] = $this->Promociones_Model->obtenerIsActivo();

		$this->template->write_view('content', $this->folder.'/cines/promociones/index', $datos);
		$this->template->render();
	}
	
	public function archivosEntrada(){
		$idUploader = $this->input->get("uploaderId");
		$output = [];

        $imagenes = $this->Promociones_Model->obtenerImagenes();
        foreach($imagenes as $imagen){
            $uuid = $imagen["uuid_file"];
            $name = $imagen["name_file"].".".$imagen["ext_file"];
			$thumb = $imagen["dir_file"].$imagen["name_file"]." (medium)".".".$imagen["ext_file"];
			$inputExtras = $imagen["json_input_extras"];
            $archivo = (object) ["name" => $name, "uuid" => $uuid, "thumbnailUrl" => $thumb, "inputExtras" => $inputExtras];
            $output[] = $archivo;
        }
    
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function modificar(){
		$idsImagenes = $this->Promociones_Model->obtenerImagenes();
		$datos["is_activo"] = $this->input->post("all-activo") === "on" ? 1 : 0;
		$update = true;
		if(is_null($idsImagenes)){
			$update = false;
		}

		$this->Promociones_Model->guardarIsActivo($datos["is_activo"]);

		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$uploaderVariableV = $this->input->post($uploader["name"]."-info");
			if(!empty($uploaderVariableV)){
				$decode = json_decode($uploaderVariableV);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
                }
				$inputExtras = [];
				$jsonInputExtras = [];
                foreach ($decode as $imagen) {
					if(isset($banner[$uploader["nameDB"]])){
						$deleteFiles[] = $banner[$uploader["nameDB"]];
					}
					$inputExtras["activo_promocion"] = $imagen->inputExtras->activo_promocion;
					$jsonInputExtras[] = $inputExtras;
				}
					$this->Promociones_Model->guardarImagenes($ids, $jsonInputExtras);
				}
			}
		}

		foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}

		$this->alertSuccess("Promociones Modificadas Correctamente");
		redirect('administracion/cines');
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}