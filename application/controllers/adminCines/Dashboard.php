<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Cines');
		$this->loadTemplatesComunes($datos);
		$this->template->write_view('content', $this->folder.'/cines/dashboard', $datos);
		$this->template->render();
	}
}