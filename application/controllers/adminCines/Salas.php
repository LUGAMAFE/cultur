<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salas extends MY_Admin {

    protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';

    protected $plural = 'Salas';
    protected $singular = 'Sala';
    protected $iconoFA = 'fas fa-couch';

    protected $mainPage = 'administracion/cines/salas';
    
    public function __construct(){
        parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/cines/Salas_Model");
        $this->load->model("administracion/Archivos_Model");
    }

    public function index(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
        $datos['plural'] = $this->plural;
        $datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin $this->plural");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('tabla-salas.js');

        $datos["action"] = site_url("administracion/cines/salas/crear");
        $datos["editar"] = site_url($this->mainPage);
        $datos["eliminar"] = site_url("administracion/cines/salas/eliminar");

        $datos['entradas'] = $this->Salas_Model->getAll();
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de las $this->plural");
            redirect_back();
            return;
        }

        $this->loadDataTables();
        $this->template->write_view('content', $this->folder.'/cines/salas/list', $datos);
        $this->template->render();
    }

    public function crear(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
        $datos['plural'] = $this->plural;
        $datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin Crear $this->singular");
        $this->loadTemplatesComunes($datos);

        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/cines/salas/crear/formInfo");
        $datos["back"] = site_url($this->mainPage);

        $this->template->write_view('content', $this->folder.'/cines/salas/sala', $datos);
        $this->template->render();
    }

    public function modificar($idEntrada){
        
        if(!$this->Salas_Model->existId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;
		$this->template->write('title', "Admin Modificar $this->singular");
        $this->loadTemplatesComunes($datos);

        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/cines/salas/$idEntrada/formInfo");
        $datos["back"] = site_url($this->mainPage);
        $datos["datos_entrada"] = $this->Salas_Model->getById($idEntrada);
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/cines/salas/sala', $datos);
        $this->template->render();
    }

    public function eliminar($id){
        if(!$this->Salas_Model->existId($id)){
            $this->alertError("La entrada $this->singular que tratas de eliminar no existe");
            redirect_back();
            return;
        }

        $this->Salas_Model->deleteById($id);

        $this->alertSuccess("La Entrada $this->singular ha sido eliminada Correctamente");

        redirect($this->mainPage);
    }

    public function crearEntrada(){
        $datos["nombre_sala"] = $this->input->post('nombre_sala');
        $datos["numero_asientos_totales_sala"] = $this->input->post('numero_asientos_totales_sala');
        $datos["numero_asientos_disponibles_sala"] = $this->input->post('numero_asientos_totales_sala');

        $update = false;

        if(isset($datos)){
			$this->Salas_Model->guardarSala($datos, $update);
        }
        
       
        $this->alertSuccess("$this->singular Guardada Correctamente");
        redirect($this->mainPage);
    }

    public function modificarEntrada($idEntrada){

        if(!$this->Salas_Model->existId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }

        $datos["nombre_sala"] = $this->input->post('nombre_sala');
        $datos["numero_asientos_totales_sala"] = $this->input->post('numero_asientos_totales_sala');
        $datos["numero_asientos_disponibles_sala"] = $this->input->post('numero_asientos_totales_sala');

        $update = true;
        
        if(isset($datos)){
			$this->Salas_Model->guardarSala($datos, $update, $idEntrada);
        }
        
        $this->alertSuccess("$this->singular Modificada Correctamente");
        redirect($this->mainPage);
    }
}