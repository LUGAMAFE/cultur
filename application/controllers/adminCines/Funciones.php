<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class funciones extends MY_Admin {

    protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';

    protected $plural = 'Funciones';
    protected $singular = 'Función';
    protected $iconoFA = 'fas fa-video';

    protected $mainPage = 'administracion/cines/funciones';
    
    public function __construct(){
        parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/cines/Funciones_Model");
        $this->load->model("administracion/cines/Peliculas_Model");
        $this->load->model("administracion/cines/Salas_Model");
        $this->load->model("administracion/Archivos_Model");
        setlocale(LC_ALL,"es_MX.utf8");
    }

    public function index(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
        $datos['plural'] = $this->plural;
        $datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin $this->plural");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('tabla-funciones.js');

        $datos["action"] = site_url("administracion/cines/funciones/crear");
        $datos["editar"] = site_url($this->mainPage);
        $datos["eliminar"] = site_url("administracion/cines/funciones/eliminar");

        $datos['entradas'] = $this->Funciones_Model->getAll();
        for ($i=0; $i < count($datos['entradas']); $i++) { 
            $datos['entradas'][$i]["nombre_pelicula_funcion"] = $this->Peliculas_Model->getById($datos['entradas'][$i]["pelicula-asociada-funcion"])["nombre_pelicula"];
            $datos['entradas'][$i]["nombre_sala_funcion"] = $this->Salas_Model->getById($datos['entradas'][$i]["sala-asociada-funcion"])["nombre_sala"];
            $datos['entradas'][$i]["dia_funcion"] = $this->Funciones_Model->getFechasFuncion($datos['entradas'][$i]["id_funcion"]);
            $datos['entradas'][$i]["fecha_funcion"] = implode(",", $this->Funciones_Model->getHorasFuncion($datos['entradas'][$i]["id_funcion"]));
        }
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de las $this->plural");
            redirect_back();
            return;
        }

        $this->loadDataTables();
        $this->template->write_view('content', $this->folder.'/cines/funciones/list', $datos);
        $this->template->render();
    }

    private function getListPeliculas(){
        $peliculas = $this->Peliculas_Model->getAll();
        for ($i=0; $i < count($peliculas); $i++) { 
            $peliculas[$i]["imagen_pelicula"] = $this->Peliculas_Model->getImagenPelicula($peliculas[$i]["id_pelicula"]);
        }
        return $peliculas;
    } 

    private function getListSalas(){
        $salas = $this->Salas_Model->getAll();
        return $salas;
    } 

    public function crear(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
        $datos['plural'] = $this->plural;
        $datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin Crear $this->singular");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_css('minimal-duration-plugin/bootstrap-duration-picker.css');
        $this->template->asset_js('minimal-duration-plugin/bootstrap-duration-picker.js');
        $this->template->asset_css('bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');
        $this->template->asset_css('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css');
        $this->template->asset_js('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
        $this->template->asset_js('bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js');
        $this->template->asset_js('moment.js');
        $this->template->asset_css('bootstrap-material-datetimepicker-gh-pages/css/bootstrap-material-datetimepicker.css');
        $this->template->asset_js('bootstrap-material-datetimepicker-gh-pages/js/bootstrap-material-datetimepicker.js');
        $this->template->asset_css('bootstrap-select/css/bootstrap-select.min.css');
        $this->template->asset_js('bootstrap-select/js/bootstrap-select.min.js');
        $this->template->asset_js('funciones.js');

        $datos['peliculas'] = $this->getListPeliculas();
        $datos['salas'] = $this->getListSalas();

        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/cines/funciones/crear/formInfo");
        $datos["back"] = site_url($this->mainPage);

        $this->template->write_view('content', $this->folder.'/cines/funciones/funcion', $datos);
        $this->template->render();
    }

    public function modificar($idEntrada){
        
        if(!$this->Funciones_Model->existId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;
		$this->template->write('title', "Admin Modificar $this->singular");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_css('minimal-duration-plugin/bootstrap-duration-picker.css');
        $this->template->asset_js('minimal-duration-plugin/bootstrap-duration-picker.js');
        $this->template->asset_css('bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');
        $this->template->asset_css('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css');
        $this->template->asset_js('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
        $this->template->asset_js('bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js');
        $this->template->asset_js('moment.js');
        $this->template->asset_css('bootstrap-material-datetimepicker-gh-pages/css/bootstrap-material-datetimepicker.css');
        $this->template->asset_js('bootstrap-material-datetimepicker-gh-pages/js/bootstrap-material-datetimepicker.js');
        $this->template->asset_css('bootstrap-select/css/bootstrap-select.min.css');
        $this->template->asset_js('bootstrap-select/js/bootstrap-select.min.js');
        $this->template->asset_js('funciones.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/cines/funciones/$idEntrada/formInfo");
        $datos["back"] = site_url($this->mainPage);
        $datos["datos_entrada"] = $this->Funciones_Model->getById($idEntrada);
        $datos['datos_entrada']["dia_funcion"] = $this->Funciones_Model->getFechasFuncion($datos['datos_entrada']["id_funcion"]);
        $datos['datos_entrada']["fecha_funcion"] = $this->Funciones_Model->getHorasFuncion($datos['datos_entrada']["id_funcion"]);
        $datos['peliculas'] = $this->getListPeliculas();
        $datos['salas'] = $this->getListSalas();
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/cines/funciones/funcion', $datos);
        $this->template->render();
    }

    public function eliminar($id){
        if(!$this->Funciones_Model->existId($id)){
            $this->alertError("La entrada $this->singular que tratas de eliminar no existe");
            redirect_back();
            return;
        }

        $this->Funciones_Model->deleteById($id);

        $this->alertSuccess("La Entrada $this->singular ha sido eliminada Correctamente");

        redirect($this->mainPage);
    }

    public function crearEntrada(){
        $datos["pelicula-asociada-funcion"] = $this->input->post('pelicula-asociada-funcion');
        $datos["sala-asociada-funcion"] = $this->input->post('sala-asociada-funcion');
        $datos["idioma-funcion"] = $this->input->post("idioma-funcion");
        $horasEvento = array_unique($this->input->post("fecha_funcion"));
        $diasEvento = explode(",", $this->input->post("dia_funcion"));

        $datos["fecha_funcion"] = $this->formatearHorasParaGuardar($horasEvento);

        $fechas_format = $this->formatearFechasParaGuardar($diasEvento);
        
        $datos["dia_funcion"] = $fechas_format["dias_evento"];

        $update = false;

        $validarEvento = $this->validarEntrada($datos, $update, Null, $fechas_format["dias_extra"]);

        if(is_string($validarEvento)){
            $this->alertError($validarEvento);
            redirect_back();
            return;
		}
        
        if(isset($datos)){
			$this->Funciones_Model->guardarfuncion($datos, $update);
        }
       
        $this->alertSuccess("$this->singular Guardada Correctamente");
        redirect($this->mainPage);
    }

    public function modificarEntrada($idEntrada){

        if(!$this->Funciones_Model->existId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }

        $datos["pelicula-asociada-funcion"] = $this->input->post('pelicula-asociada-funcion');
        $datos["sala-asociada-funcion"] = $this->input->post('sala-asociada-funcion');
        $datos["idioma-funcion"] = $this->input->post("idioma-funcion");
        $horasEvento = array_unique($this->input->post("fecha_funcion"));
        $diasEvento = explode(",", $this->input->post("dia_funcion"));

        $datos["fecha_funcion"] = $this->formatearHorasParaGuardar($horasEvento);

        $fechas_format = $this->formatearFechasParaGuardar($diasEvento);
        
        $datos["dia_funcion"] = $fechas_format["dias_evento"];

        $update = true;

        $validarEvento = $this->validarEntrada($datos, $update, $idEntrada, $fechas_format["dias_extra"]);

        if(is_string($validarEvento)){
            $this->alertError($validarEvento);
            redirect_back();
            return;
		}
        
        if(isset($datos)){
			$this->Funciones_Model->guardarFuncion($datos, $update, $idEntrada);
        }
        
        $this->alertSuccess("$this->singular Modificada Correctamente");
        redirect($this->mainPage);
    }

    private function formatearHorasParaGuardar($horasEvento){
        $horas_evento = "";
        foreach($horasEvento as $key=>$value){
            if($key == 0){
                $horas_evento = $horas_evento.date("H:i:s", strtotime($value));
            } else {
                $horas_evento = $horas_evento.",".date("H:i:s", strtotime($value));
            }
        }

        return $horas_evento;
    }

    private function formatearFechasParaGuardar($fechasEvento){
        $dias_evento = "";
        $dias_extra = "";
        $resultado_formateo = [];
        foreach($fechasEvento as $key=>$value){
            $fecha = explode(" ", $value);
            $mes = $this->getNumeroMes($fecha[2]);
            if($key == 0){
                $dias_evento = $dias_evento.$fecha[3]."-".$mes."-".$fecha[1];
                $dias_extra = $dias_extra.date('Y-m-d', strtotime($mes."/".$fecha[1]."/".$fecha[3].' +1 day'));
                $dias_extra = $dias_extra.",".date('Y-m-d', strtotime($mes."/".$fecha[1]."/".$fecha[3].' -1 day'));
            } else {
                $dias_evento = $dias_evento.",".$fecha[3]."-".$mes."-".$fecha[1];
                $dias_extra = $dias_extra.",".date('Y-m-d', strtotime($mes."/".$fecha[1]."/".$fecha[3].' +1 day'));
                $dias_extra = $dias_extra.",".date('Y-m-d', strtotime($mes."/".$fecha[1]."/".$fecha[3].' -1 day'));
            }
        }
        $resultado_formateo['dias_evento'] = $dias_evento;
        $resultado_formateo['dias_extra'] = $dias_extra;

        return $resultado_formateo;
    }

    //Función que valida que un evento no choque con otros en fechas y horas
    private function validarEntrada($datos, $update, $idEntrada=Null, $dias_extra){
        $eventosSala = $this->Funciones_Model->obtenerFuncionesSala($datos["sala-asociada-funcion"],$datos["dia_funcion"],$update,$idEntrada);
        $eventosSalaAdicionales = $this->Funciones_Model->obtenerFuncionesSala($datos["sala-asociada-funcion"],$dias_extra,$update,$idEntrada);
        $horasEntrada = explode(",",$datos["fecha_funcion"]);

        /*$eventosSala son los eventos que ocurren en algún dia en común y en el mismo lugar que el evento que se intenta agregar
          $eventosSalaAdicionales son los eventos que ocurren un dia antes o despues y en el mismo lugar que el evento que se intenta agregar */

        foreach($horasEntrada as $key1=>$hora){
            $duracionEventoEntrada = $this->Peliculas_Model->selectById($datos["pelicula-asociada-funcion"])["duracion_pelicula"];
            $horaInicioEntrada = $hora;
            $secs = strtotime($duracionEventoEntrada)-strtotime("00:00:00");
            $horaFinEntrada = date("H:i:s",strtotime($horaInicioEntrada)+$secs);

            if(!empty($eventosSala)||!empty($eventosSalaAdicionales)){
                $diasEntrada = explode(",",$datos["dia_funcion"]);
                $diasExtra = explode(",",$dias_extra);

                //Validación de que no choquen los horarios en eventos en un dia en comun y en el mismo lugar
                if(!empty($eventosSala)){
                    foreach($eventosSala as $evento){
                        $eventoValid = $this->Peliculas_Model->selectById($evento["pelicula-asociada-funcion"]);
                        $duracionEventoValid = $eventoValid["duracion_pelicula"];
                        $diasValid = explode(",",$evento["fecha_funcion"]);
                        $diasComun = array_intersect($diasEntrada, $diasValid);
                        $horaInicioValid = $evento["hora_funcion"];
                        $secs = strtotime($duracionEventoValid)-strtotime("00:00:00");
                        $horaFinValid = date("H:i:s",strtotime($horaInicioValid)+$secs);

                        $hie = DateTime::createFromFormat('H:i:s', $horaInicioEntrada);
                        $hfe = DateTime::createFromFormat('H:i:s', $horaFinEntrada);
                        $hiv = DateTime::createFromFormat('H:i:s', $horaInicioValid);
                        $hfv = DateTime::createFromFormat('H:i:s', $horaFinValid);

                        if (($hfv < $hiv && $hfe < $hie)||
                        ($hfv < $hiv && ($hiv < $hie||$hiv < $hfe))||
                        ($hiv < $hfv && $this->TimeIsBetweenTwoTimes($horaInicioValid,$horaFinValid,$horaInicioEntrada))||
                        ($hiv < $hfv && $hie < $hfe && $this->TimeIsBetweenTwoTimes($horaInicioValid,$horaFinValid,$horaFinEntrada))){
                            foreach($diasComun as $key=>$value){
                                $diasComun[$key] = ucwords(strftime("%A %d %B %Y", strtotime($value)));
                            }
                            return "La funcion choca con la funcion de ".$eventoValid["nombre_pelicula"]." en la/s fecha/s ".implode(",",$diasComun);
                        }
                    }
                }

                //Validación de que no choquen los horarios en eventos que impliquen un cambio de dia en sus horarios
                if(!empty($eventosSalaAdicionales)){
                    foreach($eventosSalaAdicionales as $evento){
                        $eventoValid = $this->Peliculas_Model->selectById($evento["pelicula-asociada-funcion"]);
                        $duracionEventoValid = $eventoValid["duracion_pelicula"];
                        $diasValid = explode(",",$evento["fecha_funcion"]);
                        $diasComun = array_intersect($diasExtra, $diasValid);
                        $horaInicioValid = $evento["hora_funcion"];
                        $secs = strtotime($duracionEventoValid)-strtotime("00:00:00");
                        $horaFinValid = date("H:i:s",strtotime($horaInicioValid)+$secs);

                        $hie = DateTime::createFromFormat('H:i:s', $horaInicioEntrada);
                        $hfe = DateTime::createFromFormat('H:i:s', $horaFinEntrada);
                        $hiv = DateTime::createFromFormat('H:i:s', $horaInicioValid);
                        $hfv = DateTime::createFromFormat('H:i:s', $horaFinValid);

                        $llavesComun = array_keys($diasComun);
                        $esDiaSiguiente = false;
                        $esDiaAnterior = false;
                        foreach($llavesComun as $key=>$value){
                            if($value % 2 == 0){
                                $esDiaSiguiente = true;
                            } else {
                                $esDiaAnterior = true;
                            }
                        }

                        if ($hie > $hfe && $esDiaSiguiente && $hfe > $hiv){
                            foreach($diasComun as $key=>$value){
                                $diasComun[$key] = ucwords(strftime("%A %d %B %Y", strtotime($value)));
                            }
                            return "La funcion choca con la funcion de ".$eventoValid["nombre_pelicula"]." en la/s fecha/s ".implode(",",$diasComun);
                        }
                        if ($hiv > $hfv && $esDiaAnterior && $hfv > $hie){
                            foreach($diasComun as $key=>$value){
                                $diasComun[$key] = ucwords(strftime("%A %d %B %Y", strtotime($value)));
                            }
                            return "La funcion choca con la funcion de ".$eventoValid["nombre_pelicula"]." en la/s fecha/s ".implode(",",$diasComun);
                        }
                    }
                }
            }

            //Validación de que no choquen los horarios del evento a agregar entre sí
            foreach($horasEntrada as $key2=>$hora2){
                if($key1 < $key2){
                    $horaInicioEntradaValid = $hora2;
                    $secs = strtotime($duracionEventoEntrada)-strtotime("00:00:00");
                    $horaFinEntradaValid = date("H:i:s",strtotime($horaInicioEntradaValid)+$secs);

                    $hie = DateTime::createFromFormat('H:i:s', $horaInicioEntrada);
                    $hfe = DateTime::createFromFormat('H:i:s', $horaFinEntrada);
                    $hiev = DateTime::createFromFormat('H:i:s', $horaInicioEntradaValid);
                    $hfev = DateTime::createFromFormat('H:i:s', $horaFinEntradaValid);

                    if (($hfev < $hiev && $hfe < $hie)||
                    ($hfev < $hiev && ($hiev < $hie||$hiev < $hfe))||
                    ($hiev < $hfev && $this->TimeIsBetweenTwoTimes($horaInicioEntradaValid,$horaFinEntradaValid,$horaInicioEntrada))||
                    ($hiev < $hfev && $hie < $hfe && $this->TimeIsBetweenTwoTimes($horaInicioEntradaValid,$horaFinEntradaValid,$horaFinEntrada))){
                        return "El horario de esta funcion a la/s ".date('h:i A', strtotime($horaInicioEntrada))
                        ." choca con el horario a la/s ".date('h:i A', strtotime($horaInicioEntradaValid));
                    }
                }
            }
        }
        return true;
    }

    private function getNumeroMes($mes){
        switch($mes){
            case "Enero":
                return "01";
                break;
            case "Febrero":
                return "02";
                break;
            case "Marzo":
                return "03";
                break;
            case "Abril":
                return "04";
                break;
            case "Mayo":
                return "05";
                break;
            case "Junio":
                return "06";
                break;
            case "Julio":
                return "07";
                break;
            case "Agosto":
                return "08";
                break;
            case "Septiembre":
                return "09";
                break;
            case "Octubre":
                return "10";
                break;
            case "Noviembre":
                return "11";
                break;
            case "Diciembre":
                return "12";
                break;
        }
    }

    private function TimeIsBetweenTwoTimes($from, $till, $input) {
        $f = DateTime::createFromFormat('H:i:s', $from);
        $t = DateTime::createFromFormat('H:i:s', $till);
        $i = DateTime::createFromFormat('H:i:s', $input);
        if ($f > $t) $t->modify('+1 day');
        return ($f < $i && $i < $t) || ($f < $i->modify('+1 day') && $i < $t);
    }
}