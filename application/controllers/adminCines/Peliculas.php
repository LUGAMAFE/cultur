<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peliculas extends MY_Admin {

    protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';

    protected $plural = 'Peliculas';
    protected $singular = 'Pelicula';
    protected $iconoFA = 'fas fa-film';

    protected $mainPage = 'administracion/cines/peliculas';

    protected $fineUploaders = array(
        array("name" => "imagen-pelicula", "nameDB" => "id_file_img_pelicula", "folder" => "../public/assets/img/Cines Siglo XXI/Peliculas", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 )
	);
    
    public function __construct(){
        parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/cines/Peliculas_Model");
        $this->load->model("administracion/Archivos_Model");
    }

    public function index(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
        $datos['plural'] = $this->plural;
        $datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin $this->plural");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_js('tabla-peliculas.js');

        $datos["action"] = site_url("administracion/cines/peliculas/crear");
        $datos["editar"] = site_url($this->mainPage);
        $datos["eliminar"] = site_url("administracion/cines/peliculas/eliminar");

        $datos['entradas'] = $this->Peliculas_Model->getAll();
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de las $this->plural");
            redirect_back();
            return;
        }

        $this->loadDataTables();
        $this->template->write_view('content', $this->folder.'/cines/peliculas/list', $datos);
        $this->template->render();
    }

    public function archivosEntrada(){
        $idUploader = $this->input->get("uploaderId");
        $idPelicula = $this->input->get("idPelicula");
        $output = [];
        switch ($idUploader) {
            case 'imagen-pelicula':
                $archivo = $this->Peliculas_Model->getImagenPelicula($idPelicula);
                break;
        }
		$ruta = $archivo["dir_file"] . $archivo["name_file"] . ".". $archivo["ext_file"];
        $file = (object) ["name" => $archivo["name_file"].".".$archivo["ext_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta];
        $output[] = $file;
        $json = json_encode($output, JSON_UNESCAPED_UNICODE);
        echo $json;
    }

    public function crear(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
        $datos['plural'] = $this->plural;
        $datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin Crear $this->singular");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_css('minimal-duration-plugin/bootstrap-duration-picker.css');
        $this->template->asset_js('minimal-duration-plugin/bootstrap-duration-picker.js');
        $this->template->asset_css('bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
        $this->template->asset_js('bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
        $this->template->asset_js('peliculas.js');

        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/cines/peliculas/crear/formInfo");
        $datos["back"] = site_url($this->mainPage);

        $this->template->write_view('content', $this->folder.'/cines/peliculas/pelicula', $datos);
        $this->template->render();
    }

    public function modificar($idEntrada){
        
        if(!$this->Peliculas_Model->existId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;
		$this->template->write('title', "Admin Modificar $this->singular");
        $this->loadTemplatesComunes($datos);
        $this->template->asset_css('minimal-duration-plugin/bootstrap-duration-picker.css');
        $this->template->asset_js('minimal-duration-plugin/bootstrap-duration-picker.js');
        $this->template->asset_css('bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
        $this->template->asset_js('bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
        $this->template->asset_js('peliculas.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/cines/peliculas/$idEntrada/formInfo");
        $datos["back"] = site_url($this->mainPage);
        $datos["datos_entrada"] = $this->Peliculas_Model->getById($idEntrada);
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/cines/peliculas/pelicula', $datos);
        $this->template->render();
    }

    public function eliminar($id){
        if(!$this->Peliculas_Model->existId($id)){
            $this->alertError("La entrada $this->singular que tratas de eliminar no existe");
            redirect_back();
            return;
        }
        $archivos = [];
        $archivos[] = $this->Peliculas_Model->getImagenPelicula($id);

        foreach ($archivos as $archivo){
            $this->Archivos_Model->eliminarCarpetaArchivoPorId($archivo["id_file"]);
            $this->Archivos_Model->eliminarArchivoPorId($archivo["id_file"]);
        }

        $this->Peliculas_Model->deleteById($id);

        $this->alertSuccess("La Entrada $this->singular ha sido eliminada Correctamente");

        redirect($this->mainPage);
    }

    public function crearEntrada(){
        $datos["nombre_pelicula"] = $this->input->post('nombre_pelicula');
        $datos["clasificacion_pelicula"] = $this->input->post('clasificacion_pelicula');
        $datos["duracion_pelicula"] = gmdate('H:i:s',$this->input->post('duracion_pelicula'));
        $datos["sinopsis_pelicula"] = $this->input->post('sinopsis_pelicula');
        $url_youtube = $this->input->post('trailer_pelicula');


        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url_youtube, $matches);
        
        if(isset($matches[1])){
            $datos["trailer_pelicula"] = $url_youtube;
        }else{
            $this->alertError("Url Youtube Invalida. No se ha podido identificar el id del video de youtube seleccionado para el trailer de la pelicula.");
            redirect_back();
            return;
        }

        $update = false;

        foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
                $ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if(isset($idsImagenBanner[$uploader["nameDB"]])){
					$deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
				}
				$datos[$uploader["nameDB"]] = $ids;
				}
			}
        }
        
        if(isset($datos)){
			$this->Peliculas_Model->guardarPelicula($datos, $update);
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
       
        $this->alertSuccess("$this->singular Guardada Correctamente");
        redirect($this->mainPage);
    }

    public function modificarEntrada($idEntrada){

        if(!$this->Peliculas_Model->existId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }

        $datos["nombre_pelicula"] = $this->input->post('nombre_pelicula');
        $datos["clasificacion_pelicula"] = $this->input->post('clasificacion_pelicula');
        $datos["duracion_pelicula"] = gmdate('H:i:s',$this->input->post('duracion_pelicula'));
        $datos["sinopsis_pelicula"] = $this->input->post('sinopsis_pelicula');
        $url_youtube = $this->input->post('trailer_pelicula');

        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url_youtube, $matches);
        
        if(isset($matches[1])){
            $datos["trailer_pelicula"] = $url_youtube;
        }else{
            $this->alertError("Url Youtube Invalida. No se ha podido identificar el id del video de youtube seleccionado para el trailer de la pelicula.");
            redirect_back();
            return;
        }

        $datosAnteriores = $this->Peliculas_Model->getById($idEntrada);
        $duracionAnterior = explode(":",$datosAnteriores["duracion_pelicula"]);
        $duracionNueva = explode(":",$datos["duracion_pelicula"]);

        if((intval($duracionNueva[0])>intval($duracionAnterior[0]))||(intval($duracionNueva[0])==intval($duracionAnterior[0])&&intval($duracionNueva[1])>intval($duracionAnterior[1]))){
            $this->alertError("No se puede aumentar la duración de la pelicula");
            redirect_back();
            return;
        }

        $update = true;
        
        $decodeToDelete = Array();
        foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if(isset($idsImagenBanner[$uploader["nameDB"]])){
					$deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
				}
				$datos[$uploader["nameDB"]] = $ids;
				}
			}
        }
        
        if(isset($datos)){
			$this->Peliculas_Model->guardarPelicula($datos, $update, $idEntrada);
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
        
        $this->alertSuccess("$this->singular Modificada Correctamente");
        redirect($this->mainPage);
    }

    protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
    }
    
    public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}