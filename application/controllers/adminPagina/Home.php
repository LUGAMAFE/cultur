<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
		$this->load->model("administracion/pagina/Home_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$this->template->write('title', 'Admin Pagina Home');
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('homepage.js');
		$datos["action"] = site_url("administracion/pagina/home/modificar");
		$datos["codigo_video"] =  $this->Home_Model->obtenerCodigoVideo();
		$this->template->write_view('content', $this->folder.'/pagina/home', $datos);
		$this->template->render();
	}
	

	public function modificar(){
		$codigoVideo = $this->input->post("codigovideo");

		if(empty($codigoVideo) || !preg_match('/^\d+$/', $codigoVideo) ){
			$this->alertError("El codigo del video tiene un formato invalido o esta vacia");
			redirect_back();
		}

		$datos = array();
		$datos["id_file_video"] = $codigoVideo;

		$this->Home_Model->actualizar($datos);

		$this->alertSuccess("Pagina Home Modificada Correctamente");
		redirect('administracion/pagina');
	}
}