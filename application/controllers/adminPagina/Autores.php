<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autores extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
    protected $folder_set = '/admin/partials/';

    protected $plural = 'Autores';
	protected $singular = 'Autor';
    protected $iconoFA = 'fas fa-user-tie';
    
    protected $fineUploaders = array(
        array("name" => "imagen-autor", "nameDB" => "id_file_img_autor", "folder" => "../public/assets/img/autores/fotos", "multiple" => false, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
        array("name" => "imagenes-ambiente-autor", "nameDB" => "id_file_img_ambiente_autor", "folder" => "../public/assets/img/autores/ambiente", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
        $this->class = strtolower(get_class());
        $this->load->model("administracion/Autores_Model");
        $this->load->model("administracion/Archivos_Model");
    }

    public function index(){
		$datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin $this->plural");
		$this->loadTemplatesComunes($datos);
        $this->loadDataTables();
        $this->template->asset_js('tabla-autores.js');

        $datos["action"] = site_url("administracion/pagina/autores/crear");

        $datos['entradas'] = $this->Autores_Model->getAll();
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de los autores");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/autores/list', $datos);
		$this->template->render();
    }

    public function crear(){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;
		$this->template->write('title', 'Admin Autor Crear');
        $this->loadTemplatesComunes($datos);
        //Summernote 
        $this->template->asset_css('summernote/summernote-bs4.css');
        $this->template->asset_js('summernote/summernote-bs4.min.js');
        $this->template->asset_js('summernote/lang/summernote-es-ES.js');
        $this->template->asset_js('autores.js');

        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/pagina/autores/crear/formInfo");
        $datos["back"] = site_url("administracion/pagina/autores");

        $this->template->write_view('content', $this->folder.'/autores/autor', $datos);
        $this->template->render();
    }

    public function archivosEntrada(){
        $idUploader = $this->input->get("uploaderId");
        $idAutor = $this->input->get("idAutor");
        $output = [];
        switch ($idUploader) {
            case 'imagen-autor':
                $archivo = $this->Autores_Model->getImagenAutor($idAutor);
                $ruta = $archivo["dir_file"] . $archivo["name_file"] . ".". $archivo["ext_file"];
                $file = (object) ["name" => $archivo["name_file"].".".$archivo["ext_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta];
                $output[] = $file;
                $json = json_encode($output, JSON_UNESCAPED_UNICODE);
                break;
            case 'imagenes-ambiente-autor':
                $archivos = $this->Autores_Model->getImagenesAmbienteAutor($idAutor);
                foreach ( $archivos as $archivo) {
                    $ruta = $archivo["dir_file"] . $archivo["name_file"] . ".". $archivo["ext_file"];
                    $file = (object) ["name" => $archivo["name_file"].".".$archivo["ext_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta];
                    $output[] = $file;
                }
                $json = json_encode($output, JSON_UNESCAPED_UNICODE);
                break;
        }
        echo $json;
    }

    public function eliminar($id){
        $this->load->model("administracion/SeleccionAutor_Model");

        if(!$this->Autores_Model->existId($id)){
            $this->alertError("La entrada $this->singular que tratas de eliminar no existe");
            redirect_back();
            return;
        }
        
        $selecciones = $this->SeleccionAutor_Model->select($id, "id_autor_asociado");
        $archivos = [];
        foreach ($selecciones as $seleccion) {
            $archivos[] = $this->SeleccionAutor_Model->getImagenesSelecciones($seleccion["id_seleccion_autor"]);
        }
        
        $archivos = array_merge($archivos, $this->Autores_Model->getImagenesAmbienteAutor($id));
        $archivos[] = $this->Autores_Model->getImagenAutor($id);

        foreach ($archivos as $archivo){
            if(!is_null($archivo)){
                $this->Archivos_Model->eliminarCarpetaArchivoPorId($archivo["id_file"]);
                $this->Archivos_Model->eliminarArchivoPorId($archivo["id_file"]);
            }
        }
        $this->Autores_Model->deleteById($id);

        $this->alertSuccess("La Entrada $this->singular ha sido eliminada Correctamente");

        redirect('administracion/pagina/autores');
    }

    public function crearEntrada(){
        $datos["nombres_autor"] = $this->input->post('nombres_autor');
        $datos["apellidos_autor"] = $this->input->post('apellidos_autor');
        $datos["despacho_autor"] = $this->input->post('despacho_autor');
        $datos["tel_autor"] = $this->input->post('tel_autor');
        $datos["social_face_autor"] = $this->input->post('social_face_autor');
        $datos["social_youtube_autor"] = $this->input->post('social_youtube_autor');
        $datos["social_insta_autor"] = trim($this->input->post('social_insta_autor'));
        //$datos["id_imagen_autor"] = $this->input->post('imagen-autor');

        $update = false;
        $idsAmbiente = [];
        $deleteFiles = [];
		$decodeToDelete = [];
        foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
                }
                if($uploaderVariable === "imagenes_ambiente_autor"){
                    $idsAmbiente = $ids;
                }else{
                    if(isset($idsImagenBanner[$uploader["nameDB"]])){
                        $deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
                    }
                    $datos[$uploader["nameDB"]] = $ids;
                    }
                }
			}
        }
        
        if(isset($datos)){
            $this->Autores_Model->insertUpdate($datos);
            if(!empty($idsAmbiente)){
                $id = $this->db->insert_id();
                $this->Autores_Model->emptyImagenesAmbiente($id);
                $this->Autores_Model->guardarImagenesAmbiente($id, $ids);
            }
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
       
        $this->alertSuccess("Autor Guardado Correctamente");
        redirect('administracion/pagina/autores');
    }

    public function modificarEntrada($idEntrada){

        if(!$this->Autores_Model->existeEntradaId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }

        $datos["nombres_autor"] = $this->input->post('nombres_autor');
        $datos["apellidos_autor"] = $this->input->post('apellidos_autor');
        $datos["despacho_autor"] = $this->input->post('despacho_autor');
        $datos["tel_autor"] = $this->input->post('tel_autor');
        $datos["social_face_autor"] = $this->input->post('social_face_autor');
        $datos["social_youtube_autor"] = $this->input->post('social_youtube_autor');
        $datos["social_insta_autor"] = trim($this->input->post('social_insta_autor'));
        //$datos["id_imagen_autor"] = $this->input->post('imagen-autor');

        $update = true;
        $deleteFiles = [];
        $decodeToDelete = [];
        
        foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if($uploaderVariable === "imagenes_ambiente_autor"){
                    $this->Autores_Model->emptyImagenesAmbiente($idEntrada);
                    $this->Autores_Model->guardarImagenesAmbiente($idEntrada, $ids);
                }else{
                    if(isset($idsImagenBanner[$uploader["nameDB"]])){
                        $deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
                    }
                    $datos[$uploader["nameDB"]] = $ids;
                    }
                }
			}
        }
        
        if(isset($datos)){
			$this->Autores_Model->insertUpdate($datos, $idEntrada);
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
        
        $this->alertSuccess("Autor Modificado Correctamente");
        redirect('administracion/pagina/autores');
    }

    protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
	}
    
    public function modificar($idEntrada){
        
        if(!$this->Autores_Model->existeEntradaId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;
		$this->template->write('title', 'Admin Autor Modificar');
        $this->loadTemplatesComunes($datos);
        //Summernote 
        $this->template->asset_css('summernote/summernote-bs4.css');
        $this->template->asset_js('summernote/summernote-bs4.min.js');
        $this->template->asset_js('summernote/lang/summernote-es-ES.js');
        $this->template->asset_js('autores.js');
        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/pagina/autores/$idEntrada/formInfo");
        $datos["back"] = site_url("administracion/pagina/autores");
        $datos["datos_entrada"] = $this->Autores_Model->obtenerInfoEntradaModificarPorId($idEntrada);
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/autores/autor', $datos);
        $this->template->render();
    }

    public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}

}