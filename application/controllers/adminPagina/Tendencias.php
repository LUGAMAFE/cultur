<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tendencias extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	protected $plural = 'Tendencias';
	protected $singular = 'Tendencia';
	protected $iconoFA = 'fas fa-star';

	protected $fineUploaders = array(
		array("name" => "imagenes-ambiente-tendencia", "nameDB" => "id_file_img_ambiente", "folder" => "../public/assets/img/tendencias", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);

	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
        $this->load->model("administracion/Archivos_Model");
		$this->load->model("administracion/Productos_Model");
		$this->load->model("administracion/Tendencias_Model");
	}

	public function index(){
		$datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin $this->plural");
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('tabla-tendencias.js');
		$datos["action"] = site_url("administracion/pagina/tendencias/crear");
		$datos['entradas'] = $this->Tendencias_Model->getAll();
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de las tendencias");
            redirect_back();
            return;
        }

        $this->loadDataTables();
        $this->template->write_view('content', $this->folder.'/tendencias/list', $datos);
        $this->template->render();
	}

	public function archivosEntrada(){
        $idUploader = $this->input->get("uploaderId");
        $idTendencia = $this->input->get("idTendencia");
        $output = [];
        switch ($idUploader) {
            case 'imagenes-ambiente-tendencia':
                $archivos[] = $this->Tendencias_Model->getImagenesTendencias($idTendencia);
                break;
        }
		foreach ( $archivos as $archivo) {
			$ruta = $archivo["dir_file"] . $archivo["name_file"] . ".". $archivo["ext_file"];
			$file = (object) ["name" => $archivo["name_file"].".".$archivo["ext_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta];
			$output[] = $file;
		}
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function crear(){
		$datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		$datos['productosListaParaRelacionados'] = $this->Productos_Model->obtenerListaProductosRelacionados();

        $datos["accion"] = "CREAR";
		$datos["action"] = site_url("administracion/pagina/tendencias/crear/formInfo");
		$datos["back"] = site_url("administracion/pagina/tendencias");

		$this->template->write('title', "Admin Crear $this->plural");
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('tendencias.js');
		$this->template->asset_js('node_modules/fuse.js/dist/fuse.js');
        $this->template->write_view('content', $this->folder.'/tendencias/tendencia', $datos);
        $this->template->render();
	}

	public function modificar($idEntrada){
        
        if(!$this->Tendencias_Model->existeEntradaId($idEntrada)){
            $this->alertError("No existe la entrada con el id $idEntrada");
            redirect_back();
            return;
        }
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		$this->template->write('title', "Admin Crear $this->plural");
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('tendencias.js');
		$this->template->asset_js('node_modules/fuse.js/dist/fuse.js');

        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/pagina/tendencias/$idEntrada/formInfo");
        $datos["back"] = site_url("administracion/pagina/tendencias");
		$datos["datos_entrada"] = $this->Tendencias_Model->obtenerInfoEntradaModificarPorId($idEntrada);
		$datos['productosListaParaRelacionados'] = $this->Productos_Model->obtenerListaProductosRelacionados();
		$datos['productosRelacionados'] = $this->Tendencias_Model->obtenerProductosRelacionados(json_decode($datos["datos_entrada"]["productos_asociados"]));
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/tendencias/tendencia', $datos);
        $this->template->render();
    }
	
	public function crearEntrada(){
		$datos["nombre_tendencia"] = $this->input->post('nombre_tendencia');
        $datos["descripcion_tendencia"] = $this->input->post('descripcion_tendencia');
        $datos["especificacion_tendencia"] = $this->input->post('especificacion_tendencia');
		
		$update = false;

		$datos["productos_asociados"] = $this->input->post("input-productos-asociados");
		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if($uploaderVariable === "imagenes_ambiente_tendencia"){
					$idsAmbiente = $ids;
                }else{
                    if(isset($idsImagenBanner[$uploader["nameDB"]])){
                        $deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
                    }
                    $datos[$uploader["nameDB"]] = $ids;
                    }
                }
			}
        }
        
        if(isset($datos)){
			$this->Tendencias_Model->guardarTendencia($datos, $update);
			if(!empty($idsAmbiente)){
                $id = $this->db->insert_id();
                $this->Tendencias_Model->emptyImagenesAmbiente($id);
                $this->Tendencias_Model->guardarImagenesAmbiente($id, $ids);
            }
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
       
        $this->alertSuccess("Nueva Tendencia Guardada Correctamente");
        redirect('administracion/pagina/tendencias');
	}
	
	public function eliminar($id){
        if(!$this->Tendencias_Model->existId($id)){
			$this->alertError("La entrada $this->singular que tratas de eliminar no existe");
            redirect_back();
            return;
		}
		$archivos = [];
        $archivos[] = $this->Tendencias_Model->getImagenesTendencias($id);

        foreach ($archivos as $archivo){
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($archivo["id_file"]);
			$this->Archivos_Model->eliminarArchivoPorId($archivo["id_file"]);
		}
		
		$this->Tendencias_Model->deleteById($id);

		$this->alertSuccess("La Entrada $this->singular ha sido eliminada Correctamente");

        redirect('administracion/pagina/tendencias');
	}
	
	public function modificarEntrada($idTendencia){

        if(!$this->Tendencias_Model->existeEntradaId($idTendencia)){
            $this->alertError("No existe la Tendencia con el id $idTendencia");
            redirect_back();
            return;
        }

        $datos["nombre_tendencia"] = $this->input->post('nombre_tendencia');
        $datos["descripcion_tendencia"] = $this->input->post('descripcion_tendencia');
        $datos["especificacion_tendencia"] = $this->input->post('especificacion_tendencia');
		$datos["productos_asociados"] = $this->input->post("input-productos-asociados");

        $update = true;
        $deleteFiles = [];
		$decodeToDelete = [];
        foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if($uploaderVariable === "imagenes_ambiente_tendencia"){
                    $this->Tendencias_Model->emptyImagenesAmbiente($idTendencia);
                    $this->Tendencias_Model->guardarImagenesAmbiente($idTendencia, $ids);
                }else{
                    if(isset($idsImagenBanner[$uploader["nameDB"]])){
                        $deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
                    }
                    $datos[$uploader["nameDB"]] = $ids;
                    }
                }
			}
        }
        
        if(isset($datos)){
			$this->Tendencias_Model->guardarTendencia($datos, $update, $idTendencia);
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
        
        $this->alertSuccess("Tendencia Modificada Correctamente");
        redirect('administracion/pagina/tendencias');
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
    }
    
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}

	
}