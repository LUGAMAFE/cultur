<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SeleccionesAutor extends MY_Admin {

	protected $class = '';
	protected $folder = '/admin';
	protected $folder_set = '/admin/partials/';

	protected $plural = 'Selecciones del Autor';
	protected $singular = 'Selección Autor';
	protected $iconoFA = 'far fa-hand-pointer';

	protected $fineUploaders = array(
		array("name" => "imagenes-ambiente-seleccion", "nameDB" => "id_file_img_ambiente", "folder" => "../public/assets/img/selecciones", "multiple" => true, "extensions" => array("png", "jpeg", "jpg"), "sizeLimit" => 1024 * 1024 * 8 ),
	);
	
	public function __construct(){
		parent::__construct();
		$this->class = strtolower(get_class());
		$this->load->model("administracion/SeleccionAutor_Model");
		$this->load->model("administracion/Productos_Model");
		$this->load->model("administracion/Autores_Model");
        $this->load->model("administracion/Archivos_Model");
	}

	public function archivosEntrada(){
		$idUploader = $this->input->get("uploaderId");
        $idSeleccion = $this->input->get("idSeleccion");
        $output = [];
        switch ($idUploader) {
            case 'imagenes-ambiente-seleccion':
                $archivos[] = $this->SeleccionAutor_Model->getImagenesSelecciones($idSeleccion);
                break;
        }
		foreach ( $archivos as $archivo) {
			$ruta = $archivo["dir_file"] . $archivo["name_file"] . ".". $archivo["ext_file"];
			$file = (object) ["name" => $archivo["name_file"].".".$archivo["ext_file"], "uuid" => $archivo["uuid_file"], "thumbnailUrl" => $ruta];
			$output[] = $file;
		}
		$json = json_encode($output, JSON_UNESCAPED_UNICODE);
		echo $json;
    }

	public function index($idAutor){
		$datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		if(!$this->Autores_Model->existId($idAutor)){
            $this->alertError("No existe el Autor con el id $idAutor");
            redirect_back();
            return;
		}

		$datosAutor = $this->Autores_Model->obtenerInfoEntradaModificarPorId($idAutor);
		$datos['idAutor'] = $idAutor;
		$datos['nombreAutor'] = $datosAutor["nombres_autor"]." ".$datosAutor["apellidos_autor"]; 
		$datos['entradas'] = $this->SeleccionAutor_Model->select($idAutor, "id_autor_asociado");
        if($datos['entradas'] === FALSE || is_null($datos['entradas'])){
            $this->alertError("Error del servidor al tratar de recibir los datos de las {$datos['plural']}");
            redirect_back();
            return;
        }
		$this->template->write('title', "Admin $this->plural");
		$this->loadTemplatesComunes($datos);
		$this->loadDataTables();
		$this->template->asset_js('tabla-selecciones-autor.js');

		$datos["action"] = site_url("administracion/pagina/autores/$idAutor/selecciones-autor/crear");
		$datos["back"] = site_url("administracion/pagina/autores");
		
		$this->template->write_view('content', $this->folder.'/seleccion-autor/list', $datos);
		$this->template->render();
	}
	
	public function crear($idAutor){
        $datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		if(!$this->Autores_Model->existId($idAutor)){
            $this->alertError("No existe el Autor con el id $idAutor");
            redirect_back();
            return;
		}

		$datos['productosListaParaRelacionados'] = $this->Productos_Model->obtenerListaProductosRelacionados();

		$datosAutor = $this->Autores_Model->obtenerInfoEntradaModificarPorId($idAutor);
		$datos['idAutor'] = $idAutor;
		$datos['nombreAutor'] = $datosAutor["nombres_autor"]." ".$datosAutor["apellidos_autor"]; 

        $datos["accion"] = "CREAR";
        $datos["action"] = site_url("administracion/pagina/autores/$idAutor/selecciones-autor/crear/formInfo");
        $datos["back"] = site_url("administracion/pagina/autores/$idAutor/selecciones-autor");
		
		$this->template->write('title', "Admin $this->singular Modificar");
        $this->loadTemplatesComunes($datos);
		$this->template->asset_js('seleccion-autores.js');
		$this->template->asset_js('node_modules/fuse.js/dist/fuse.js');
        $this->template->write_view('content', $this->folder.'/seleccion-autor/seleccion', $datos);
        $this->template->render();
	}
	
	public function modificar($idAutor, $idEntrada){
		$datos['class'] = $this->class;
		$datos['singular'] = $this->singular;
		$datos['plural'] = $this->plural;
		$datos['iconoFA'] = $this->iconoFA;

		if(!$this->Autores_Model->existId($idAutor)){
            $this->alertError("No existe el Autor con el id $idAutor");
            redirect_back();
            return;
		}

        if(!$this->SeleccionAutor_Model->existId($idEntrada)){
            $this->alertError("No existe la seleccion con el id $idEntrada");
            redirect_back();
            return;
		}
		
		$datosAutor = $this->Autores_Model->obtenerInfoEntradaModificarPorId($idAutor);
		$datos['idAutor'] = $idAutor;
		$datos['nombreAutor'] = $datosAutor["nombres_autor"]." ".$datosAutor["apellidos_autor"]; 

		$this->template->write('title', "Admin Crear $this->plural");
		$this->loadTemplatesComunes($datos);
		$this->template->asset_js('seleccion-autores.js');
		$this->template->asset_js('node_modules/fuse.js/dist/fuse.js');

        $datos["accion"] = "MODIFICAR";
        $datos["action"] = site_url("administracion/pagina/autores/$idAutor/selecciones-autor/$idEntrada/formInfo");
        $datos["back"] = site_url("administracion/pagina/autores/$idAutor/selecciones-autor");
		$datos["datos_entrada"] = $this->SeleccionAutor_Model->getById($idEntrada);
		$datos['productosListaParaRelacionados'] = $this->Productos_Model->obtenerListaProductosRelacionados();
		$datos['productosRelacionados'] = $this->SeleccionAutor_Model->obtenerProductosRelacionados(json_decode($datos["datos_entrada"]["productos_asociados"]));
        if($datos["datos_entrada"] === FALSE || is_null($datos["datos_entrada"])){
            $this->alertError("Error del servidor al tratar de recibir los datos de la entrada $idEntrada");
            redirect_back();
            return;
        }
        $this->template->write_view('content', $this->folder.'/seleccion-autor/seleccion', $datos);
        $this->template->render();
    }
	
	public function crearEntrada($idAutor){
		if(!$this->Autores_Model->existId($idAutor)){
            $this->alertError("No existe el Autor con el id $idAutor");
            redirect_back();
            return;
		}

		$datos["nombre_seleccion"] = $this->input->post('nombre_seleccion');
        $datos["especificacion_seleccion"] = $this->input->post('especificacion_seleccion');
        $datos["descripcion_seleccion"] = $this->input->post('descripcion_seleccion');
		$datos["productos_asociados"] = $this->input->post("input-productos-asociados");
		$datos["id_autor_asociado"] = $idAutor;

		$update = false;
		$deleteFiles = [];
		$decodeToDelete = [];

		foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if($uploaderVariable === "imagenes_ambiente_seleccion"){
					$idsAmbiente = $ids;
                }else{
                    if(isset($idsImagenBanner[$uploader["nameDB"]])){
                        $deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
                    }
                    $datos[$uploader["nameDB"]] = $ids;
                    }
                }
			}
        }
        
        if(isset($datos)){
			$this->SeleccionAutor_Model->insertUpdate($datos);
			if(!empty($idsAmbiente)){
                $id = $this->db->insert_id();
                $this->SeleccionAutor_Model->emptyImagenesAmbiente($id);
                $this->SeleccionAutor_Model->guardarImagenesAmbiente($id, $ids);
            }
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
       
        $this->alertSuccess("Nueva Seleccion Guardada Correctamente");
        redirect("administracion/pagina/autores/$idAutor/selecciones-autor");
    }
	
	public function modificarEntrada($idAutor, $idEntrada){
		if(!$this->Autores_Model->existId($idAutor)){
            $this->alertError("No existe el Autor con el id $idAutor");
            redirect_back();
            return;
		}

        if(!$this->SeleccionAutor_Model->existId($idEntrada)){
            $this->alertError("No existe la seleccion con el id $idEntrada");
            redirect_back();
            return;
        }

        $datos["nombre_seleccion"] = $this->input->post('nombre_seleccion');
        $datos["especificacion_seleccion"] = $this->input->post('especificacion_seleccion');
        $datos["descripcion_seleccion"] = $this->input->post('descripcion_seleccion');
		$datos["productos_asociados"] = $this->input->post("input-productos-asociados");

		$update = true;
        $deleteFiles = [];
		$decodeToDelete = [];

        foreach ($this->fineUploaders as $uploader) {
			$uploaderVariable = str_replace( "-" , "_", $uploader["name"]);
			$$uploaderVariable = $this->input->post($uploader["name"]."-info");
			if(!empty($$uploaderVariable)){
				$decode = json_decode($$uploaderVariable);
				foreach ($decode as $key => $value) {
					if(isset($value->deleteReal)){
						array_push($decodeToDelete, $value);
						unset($decode[$key]);
					}
				}
				if($decode != []){
				$ids = $this->guardarImagenes($decode, $uploader["folder"], "No se ha podido guardar uno de los archivos o imagenes", $uploader["multiple"]);
				if(!$ids){
					return;
				}
				if($uploaderVariable === "imagenes_ambiente_seleccion"){
                    $this->SeleccionAutor_Model->emptyImagenesAmbiente($idEntrada);
                    $this->SeleccionAutor_Model->guardarImagenesAmbiente($idEntrada, $ids);
                }else{
                    if(isset($idsImagenBanner[$uploader["nameDB"]])){
                        $deleteFiles[] = $idsImagenBanner[$uploader["nameDB"]];
                    }
                    $datos[$uploader["nameDB"]] = $ids;
                    }
                }
			}
        }
        
        if(isset($datos)){
			$this->SeleccionAutor_Model->insertUpdate($datos, $idEntrada);
        }
        
        foreach ($deleteFiles as $fileDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($fileDelete);
			$this->Archivos_Model->eliminarArchivoPorId($fileDelete);
		}

		foreach ($decodeToDelete as $decodeDelete) {
			$this->Archivos_Model->eliminarCarpetaArchivoPorUuid($decodeDelete->uuid);
			$this->Archivos_Model->eliminarArchivoPorUuid($decodeDelete->uuid);
		}
        
        $this->alertSuccess("Seleccion Modificada Correctamente");
        redirect("administracion/pagina/autores/$idAutor/selecciones-autor");
	}

	public function eliminar($idAutor, $idEntrada){
		if(!$this->Autores_Model->existId($idAutor)){
            $this->alertError("No existe el Autor con el id $idAutor");
            redirect_back();
            return;
		}

        if(!$this->SeleccionAutor_Model->existId($idEntrada)){
            $this->alertError("La entrada $this->singular que tratas de eliminar no existe");
            redirect_back();
            return;
		}

		$archivos = [];
        $archivos[] = $this->SeleccionAutor_Model->getImagenesSelecciones($idEntrada);

        foreach ($archivos as $archivo){
			$this->Archivos_Model->eliminarCarpetaArchivoPorId($archivo["id_file"]);
			$this->Archivos_Model->eliminarArchivoPorId($archivo["id_file"]);
		}

        $this->SeleccionAutor_Model->deleteById($idEntrada);

        $this->alertSuccess("La Entrada $this->singular ha sido eliminada Correctamente");

        redirect("administracion/pagina/autores/$idAutor/selecciones-autor");
	}

	protected function guardarImagenes($json, $ruta, $errorMsg, $multiple = false){
		$lenght = 0;
		if($multiple){
			$lenght = count($json)-1;
		}
		$idsInserts = [];
		for ($i=0; $i <= $lenght; $i++) { 
			$idsInserts[$i] = $this->Archivos_Model->guardarArchivo($ruta, $json[$i]->uuid, $json[$i]->name);
			if(!$idsInserts[$i]){
				$this->alertError($errorMsg);
				redirect_back();
				return false;
			}
		}
		if($multiple){
			return $idsInserts;
		}
		return $idsInserts[$lenght];
    }
    
	
	public function subirArchivos(){
		$idUploader = $this->input->post("uploaderId");
		if(is_null($idUploader)){
			$idUploader = $this->input->get("uploaderId");
		}
		foreach ($this->fineUploaders as $uploader) {
			if($uploader["name"] === $idUploader){
				$folder = $uploader["folder"];
				$extensions = $uploader["extensions"];
				$sizeLimit = $uploader["sizeLimit"];
				break;
			} 
		}
		parent::subirArchivo($folder, $extensions, $sizeLimit);
	}
}