<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller {

    protected $class      = '';
    protected $folder     = '/site/';
    protected $folder_set = '/site/partials/';

    public function __construct() {
        parent::__construct();
        $this->class = strtolower(get_class());
    }

    public function index() {
        $datos = [];
        $datos["active"] = "home";
        $datos["fullpage"] = true;
        $this->template->write('title', "Home");
        $this->template->asset_css("fullPage/dist/fullpage.min.css");
        $this->template->asset_js("fullPage/vendors/scrolloverflow.min.js");
        $this->template->asset_js("fullPage/dist/fullpage.min.js");
        $this->template->asset_js("../js/web/home.js");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->write_view('content', $this->folder . 'index', $datos);
        $this->template->render();
    }

    public function cultur(){
        $datos = [];
        $datos["active"] = "cultur";
        $this->template->write('title', "Cultur");
        $this->template->asset_css("slick-1.8.1/slick/slick.css");
        $this->template->asset_js("slick-1.8.1/slick/slick.js");
        $this->template->asset_js("svg converter.js");
        $this->template->asset_js("../js/web/cultur.js");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'cultur', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }

    public function centroConvenciones(){
        $datos = [];
        $datos["active"] = "centroc";
        $this->template->write('title', "Centro Convenciones");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'centroConvenciones', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
        $this->template->asset_js('../js/web/centro-siglo.js');
		$this->template->asset_js('../js/tilter.js');
        $this->template->render();
    }

    public function salon($salon = ''){
        $datos = [];
        $datos["active"] = "centroc";
        $datos["salon"] = $salon;
        $this->template->write('title', "Salon $salon");
        $this->template->asset_css("slick-1.8.1/slick/slick.css");
        $this->template->asset_js("slick-1.8.1/slick/slick.js");
        $this->template->asset_js("svg converter.js");
        $this->template->asset_js('tilt.js-master/src/tilt.jquery.js');
        $this->template->asset_js('../js/web/salon.js');
        $this->template->asset_js('../js/tilter.js');
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'salon', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);;
        $this->template->render();
    }

    public function cines(){
        $datos = [];
        $datos["active"] = "cines";
        $this->template->write('title', "Cines Siglo XXI");
        $this->template->asset_css("swiper/package/css/swiper.css");
        $this->template->asset_js('swiper/package/js/swiper.js');
        $this->template->asset_js('clamp.min.js');
        $this->template->asset_js('../js/web/cines.js');
        // $this->template->asset_js('anime.min.js');
        // $this->template->asset_js('../js/web/prox.js');
        $this->load->model("administracion/cines/Funciones_Model");
        $this->load->model("administracion/cines/Peliculas_Model");
        $this->load->model("administracion/cines/Salas_Model");
        $datos["funciones"] = $this->Funciones_Model->getFuncionesDeHoy();
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        // $this->template->write_view('content', $this->folder . 'proximamenteCines', $datos);
        $this->template->write_view('content', $this->folder . 'cines', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }

    public function pelicula($idPelicula = "", $nombrePelicula = ""){
        $this->load->model("administracion/cines/Funciones_Model");
        $this->load->model("administracion/cines/Peliculas_Model");
        $this->load->model("administracion/cines/Salas_Model");
        $nombrePelicula = urldecode($nombrePelicula);
        if(!$this->Peliculas_Model->existId($idPelicula)){
            show_404();
            return;
        }
        $datos = [];
        $datos["active"] = "";
        $this->template->write('title', "Pelicula");
        $datos["funciones"] = $this->Funciones_Model->getFuncionesDeHoy($idPelicula);
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'pelicula', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }

    public function politicas(){
        $datos = [];
        $datos["active"] = "";
        $this->template->write('title', "Politicas de Privacidad");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'politicas', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }

    public function cuenta(){
        $datos = [];
        $datos["active"] = "";
        $this->template->write('title', "Cuenta Publica");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'cuenta', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }

    // public function pelicula(){
    //     $datos = [];
    //     $datos["active"] = "cines";
    //     $this->template->asset_css("swiper/package/css/swiper.css");
    //     $this->template->asset_js('swiper/package/js/swiper.js');
    //     $this->template->asset_js('../js/web/cines.js');
    //     $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
    //     $this->template->write_view('content', $this->folder . 'cines', $datos);
    //     $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
    //     $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
    //     $this->template->render();
    // }

    public function paradoresTuristicos(){
        $datos = [];
        $datos["active"] = "paradores";
        $this->template->write('title', "Paradores Turisticos");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'paradores', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }

    public function parador($parador = ''){
        $datos = [];
        $datos["active"] = "paradores";
        $datos["parador"] = $parador;
        $this->template->write('title', "Parador $parador");
        $this->template->asset_css("slick-1.8.1/slick/slick.css");
        $this->template->asset_js("slick-1.8.1/slick/slick.js");
        $this->template->asset_js("svg converter.js");
        $this->template->asset_js("../js/web/parador.js");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'parador', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
    }
}
