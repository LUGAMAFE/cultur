<?php if($banner == 1): ?>
	
	<div class="banner-home banner">
		<?php foreach($info_home['banners'] as $row): ?>
		<div class="fondo">
			<img src="<?php echo site_url('assets/img/fondo-banner-home.png');?>" style="background-image: url('<?= $row->dir_file.$row->name_file.' (large).'.$row->ext_file ?>');" alt="" style="width: 100%;">
		</div>
		<?php endforeach?>
	</div>
	
<script>
	$(document)
		.on('ready', function() {
			$('.banner-home').slick({
				dots: true,
				infinite: true,
				speed: 500,
				fade: true,
				cssEase: 'linear',
				autoplay: true,
  				autoplaySpeed: 5000,
			});
		});
</script>
<?php elseif($banner == 2): ?>
<div class="banner-categoria banner">
	<div class="fondo">
		<img src="<?php echo site_url('assets/img/fondo-banner-categoria.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/banner/banner-categoria.jpg');?>');" alt="">
		<div class="cont-titulo">
			<h1 class="titulo">Selecciona tu piso ideal</h1>
		</div>
	</div>
</div>
<?php endif ?>