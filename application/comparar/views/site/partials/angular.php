<script src="<?php echo site_url('assets/js/angular/angular.min.js');?>"></script>
<script src='<?php echo site_url('assets/js/angular/ng-infinite-scroll.js');?>'></script>
<script src="<?php echo site_url('assets/js/angular/angular-router.min.js');?>"></script>
<!--<script src="<?php echo site_url('assets/js/angular/ionic-range-slider.js');?>"></script>-->
<script type="text/javascript" src="<?php echo site_url('assets/plugins/angular-foundation/angular-foundation.js');?>"></script>


<script type="text/javascript">
    var myApp = angular.module('app', ['ngRoute', 'mm.foundation.modal', 'infinite-scroll']); 

    myApp.filter('ceil', function() {
	    return function(input) {
	        return Math.ceil(input);
	    };
	});

	myApp.filter('masuno', function() {
	    return function(input) {
	        return parseFloat(input) + parseFloat(0.1);
	    };
	});
    
	myApp.directive('numLinksDisplay', function ($timeout) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                $timeout(function () {
	                    scope.$emit(attr.numLinksDisplay);
	                });
	            }
	        }
	    }
    });
    
    myApp.directive('onFinishRender', function () {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attr) {
	            if (scope.$last === true) {
	                element.ready(function () {
	                    initFoundation();
	                });
	            }
	        }
	    }
	});

    function initFoundation()
	{
		$(".js-equalizer").attr("data-equalizer", "true").foundation(); 
	}

    myApp.directive('checkImage', function($http) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	        	//console.log(attrs);
	            attrs.$observe('ngSrc', function(ngSrc) {
	            	console.log(ngSrc);
	                $http.get(ngSrc).success(function(){
	                    //alert('image exist');
	                }).error(function(){
	                    //alert('image not exist');
	                    element.attr('src', '<?php echo site_url('assets/img/default.png');?>'); // set default image
	                });
	            });
	        }
	    };
	});
</script>