<header class="header-site" data-sticky-container style="z-index: 100;">
	<div class="header-sticky sticky" data-sticky data-margin-top="0">
		<div class="row header-top-sin-scroll clearfix">
			<div class="large-22 columns">
				<div class="row">
					<div class="large-4 columns logo">
						<a href="<?= base_url();?>"><img src="<?= site_url('assets/img/logo.svg');?>" alt=""></a>
					</div>
					<div class="large-5 columns ubicacion">
						<div class="icon">
							<img src="<?= site_url('assets/img/iconos/point.svg');?>" alt="">
						</div>
						<div class="texto">
							<p>
								Encuentra tu <br>
								sucursal más cercana
							</p>
						</div>
					</div>
					<div class="large-11 columns search-input">
						<form action="">
							<div class="input-group">
								<input class="input-group-field" type="search" placeholder="¿Qué estás buscando?">
								<div class="input-group-button">
									<button type="submit" class="button">
										<i class="fas fa-search"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
					<div class="large-4 columns redes">
						<ul class="menu">
							<li>
								<a href="#" class="facebook">
									<i class="fab fa-facebook-square"></i>
								</a>
							</li>
							<li>
								<a href="#" class="youtube">
									<i class="fab fa-youtube"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="row menu-cont-header">
					<div class="menu-header">
						<ul class="first-list">
							<li class="first-list-item">
								<a tabindex="-1" href="#">Productos</a>
								<img class="downArrow svg" src="<?= site_url('assets/img/flotantes/down-arrow.svg')?>" alt="">
								<ul class="second-list">
									<li><a href="#">Pisos</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Piso Amarello</a></li>
											<li ><a href="#">Piso Roe</a></li>
											<li ><a href="#">Piso Burano</a></li>
											<li ><a href="#">Piso Zeus</a></li>
											<li ><a href="#">Piso Omega Madera</a></li>
											<li ><a href="#">Piso Ivory Cream</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/PISO.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Recubrimientos</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 1.1</a></li>
											<li ><a href="#">Ejemplo 1.2</a></li>
											<li ><a href="#">Ejemplo 1.3</a></li>
											<li ><a href="#">Ejemplo 1.4</a></li>
											<li ><a href="#">Ejemplo 1.5</a></li>
											<li ><a href="#">Ejemplo 1.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/RECUBRIMIENTO.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Porcelanicos</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 2.1</a></li>
											<li ><a href="#">Ejemplo 2.2</a></li>
											<li ><a href="#">Ejemplo 2.3</a></li>
											<li ><a href="#">Ejemplo 2.4</a></li>
											<li ><a href="#">Ejemplo 2.5</a></li>
											<li ><a href="#">Ejemplo 2.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/PORCELANATO.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Decorados</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 3.1</a></li>
											<li ><a href="#">Ejemplo 3.2</a></li>
											<li ><a href="#">Ejemplo 3.3</a></li>
											<li ><a href="#">Ejemplo 3.4</a></li>
											<li ><a href="#">Ejemplo 3.5</a></li>
											<li ><a href="#">Ejemplo 3.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/DECORADOS.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Adhesivos</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 4.1</a></li>
											<li ><a href="#">Ejemplo 4.2</a></li>
											<li ><a href="#">Ejemplo 4.3</a></li>
											<li ><a href="#">Ejemplo 4.4</a></li>
											<li ><a href="#">Ejemplo 4.5</a></li>
											<li ><a href="#">Ejemplo 4.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/ADHESIVOS.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Muebles Sanitarios</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 5.1</a></li>
											<li ><a href="#">Ejemplo 5.2</a></li>
											<li ><a href="#">Ejemplo 5.3</a></li>
											<li ><a href="#">Ejemplo 5.4</a></li>
											<li ><a href="#">Ejemplo 5.5</a></li>
											<li ><a href="#">Ejemplo 5.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/MUEBLE BAÑO.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Grifería</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 6.1</a></li>
											<li ><a href="#">Ejemplo 6.2</a></li>
											<li ><a href="#">Ejemplo 6.3</a></li>
											<li ><a href="#">Ejemplo 6.4</a></li>
											<li ><a href="#">Ejemplo 6.5</a></li>
											<li ><a href="#">Ejemplo 6.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/GRIFERIA.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Tinas y Canceles</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 7.1</a></li>
											<li ><a href="#">Ejemplo 7.2</a></li>
											<li ><a href="#">Ejemplo 7.3</a></li>
											<li ><a href="#">Ejemplo 7.4</a></li>
											<li ><a href="#">Ejemplo 7.5</a></li>
											<li ><a href="#">Ejemplo 7.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/TINAS.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Tinacos y Sisternas</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 8.1</a></li>
											<li ><a href="#">Ejemplo 8.2</a></li>
											<li ><a href="#">Ejemplo 8.3</a></li>
											<li ><a href="#">Ejemplo 8.4</a></li>
											<li ><a href="#">Ejemplo 8.5</a></li>
											<li ><a href="#">Ejemplo 8.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/TINACOS.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Bombas y Calentadores</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 9.1</a></li>
											<li ><a href="#">Ejemplo 9.2</a></li>
											<li ><a href="#">Ejemplo 9.3</a></li>
											<li ><a href="#">Ejemplo 9.4</a></li>
											<li ><a href="#">Ejemplo 9.5</a></li>
											<li ><a href="#">Ejemplo 9.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/CALENTADORES.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Puertas y Cerraduras</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 10.1</a></li>
											<li ><a href="#">Ejemplo 10.2</a></li>
											<li ><a href="#">Ejemplo 10.3</a></li>
											<li ><a href="#">Ejemplo 10.4</a></li>
											<li ><a href="#">Ejemplo 10.5</a></li>
											<li ><a href="#">Ejemplo 10.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/PUERTAS.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Tarjas, Parrillas y Campanas</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 11.1</a></li>
											<li ><a href="#">Ejemplo 11.2</a></li>
											<li ><a href="#">Ejemplo 11.3</a></li>
											<li ><a href="#">Ejemplo 11.4</a></li>
											<li ><a href="#">Ejemplo 11.5</a></li>
											<li ><a href="#">Ejemplo 11.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/TARJAS.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Aires Acondicionados</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 12.1</a></li>
											<li ><a href="#">Ejemplo 12.2</a></li>
											<li ><a href="#">Ejemplo 12.3</a></li>
											<li ><a href="#">Ejemplo 12.4</a></li>
											<li ><a href="#">Ejemplo 12.5</a></li>
											<li ><a href="#">Ejemplo 12.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/AIRE.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Paquetes de Instalación</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 13.1</a></li>
											<li ><a href="#">Ejemplo 13.2</a></li>
											<li ><a href="#">Ejemplo 13.3</a></li>
											<li ><a href="#">Ejemplo 13.4</a></li>
											<li ><a href="#">Ejemplo 13.5</a></li>
											<li ><a href="#">Ejemplo 13.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/PAQ INSTALACION.png')?>" alt="">
										</ul>
									</li>
									<li><a href="#">Herramientas de corte Cerámico</a>
										<img class="rigtArrow svg" src="<?= site_url('assets/img/flotantes/right-arrow.svg')?>" alt="">
										<ul class="last-list">
											<li class="seccion">Articulos</li>
											<li ><a href="#">Ejemplo 14.1</a></li>
											<li ><a href="#">Ejemplo 14.2</a></li>
											<li ><a href="#">Ejemplo 14.3</a></li>
											<li ><a href="#">Ejemplo 14.4</a></li>
											<li ><a href="#">Ejemplo 14.5</a></li>
											<li ><a href="#">Ejemplo 14.6</a></li>
											<img class="flotante" src="<?= site_url('assets/img/flotantes/HERRAMIENTAS.png')?>" alt="">
										</ul>
									</li>
								</ul>
							</li>
							<li class="first-list-item">
								<a href="<?= site_url('categoria/remates');?>">
									Remates 
								</a>
							</li>
							<li class="first-list-item">
								<a href="<?= site_url('bolsa-de-trabajo');?>">
									Bolsa de trabajo
								</a>
							</li>
							<?php /*<li class="first-list-item">
								<a href="">
									Proyectos
								</a>
								<!-- <ul class="second-list">
									<li><a href="#">Pisos</a></li>
									<li><a href="#">Recubrimientos</a></li>
									<li><a href="#">Porcelanicos</a></li>
									<li><a href="#">Decorados</a></li>
									<li><a href="#">Adhesivos</a></li>
									<li><a href="#">Muebles Sanitarios</a></li>
									<li><a href="#">Grifería</a></li>
									<li><a href="#">Tinas y Canceles</a></li>
									<li><a href="#">Tinacos y Sisternas</a></li>
									<li><a href="#">Bombas y Calentadores</a></li>
									<li><a href="#">Puertas y Cerraduras</a></li>
									<li><a href="#">Tarjas, Parrillas y Campanas</a>
										<ul class="last-list">
											<li class="seccion"><a href="#">Articulos</a></li>
											<li ><a href="#">Piso Amarello</a></li>
											<li ><a href="#">Piso Roe</a></li>
											<li ><a href="#">Piso Burano</a></li>
											<li ><a href="#">Piso Zeus</a></li>
											<li ><a href="#">Piso Omega Madera</a></li>
											<li ><a href="#">Piso Ivory Cream</a></li>
										</ul>
									</li>
									<li><a href="#">Aires Acondicionados</a></li>
									<li><a href="#">Paquetes de Instalación</a></li>
									<li><a href="#">Herramientas de corte Cerámico</a></li>
								</ul> -->
							</li>*/ ?>
							<li class="first-list-item">
								<a href="<?= site_url('asociados');?>">
									Subdistribuidores
								</a>
								<!-- <ul class="second-list">
									<li><a href="#">Pisos</a></li>
									<li><a href="#">Recubrimientos</a></li>
									<li><a href="#">Porcelanicos</a></li>
									<li><a href="#">Decorados</a></li>
									<li><a href="#">Adhesivos</a></li>
									<li><a href="#">Muebles Sanitarios</a></li>
									<li><a href="#">Grifería</a></li>
									<li><a href="#">Tinas y Canceles</a></li>
									<li><a href="#">Tinacos y Sisternas</a></li>
									<li><a href="#">Bombas y Calentadores</a></li>
									<li><a href="#">Puertas y Cerraduras</a></li>
									<li><a href="#">Tarjas, Parrillas y Campanas</a>
										<ul class="last-list">
											<li class="seccion"><a href="#">Articulos</a></li>
											<li ><a href="#">Piso Amarello</a></li>
											<li ><a href="#">Piso Roe</a></li>
											<li ><a href="#">Piso Burano</a></li>
											<li ><a href="#">Piso Zeus</a></li>
											<li ><a href="#">Piso Omega Madera</a></li>
											<li ><a href="#">Piso Ivory Cream</a></li>
										</ul>
									</li>
									<li><a href="#">Aires Acondicionados</a></li>
									<li><a href="#">Paquetes de Instalación</a></li>
									<li><a href="#">Herramientas de corte Cerámico</a></li>
								</ul> -->
							</li>
							<li class="first-list-item">
								<a href="<?= site_url('contacto');?>">
									Contacto
								</a>
								<!-- <ul class="second-list">
									<li><a href="#">Pisos</a></li>
									<li><a href="#">Recubrimientos</a></li>
									<li><a href="#">Porcelanicos</a></li>
									<li><a href="#">Decorados</a></li>
									<li><a href="#">Adhesivos</a></li>
									<li><a href="#">Muebles Sanitarios</a></li>
									<li><a href="#">Grifería</a></li>
									<li><a href="#">Tinas y Canceles</a></li>
									<li><a href="#">Tinacos y Sisternas</a></li>
									<li><a href="#">Bombas y Calentadores</a></li>
									<li><a href="#">Puertas y Cerraduras</a></li>
									<li><a href="#">Tarjas, Parrillas y Campanas</a>
										<ul class="last-list">
											<li class="seccion"><a href="#">Articulos</a></li>
											<li ><a href="#">Piso Amarello</a></li>
											<li ><a href="#">Piso Roe</a></li>
											<li ><a href="#">Piso Burano</a></li>
											<li ><a href="#">Piso Zeus</a></li>
											<li ><a href="#">Piso Omega Madera</a></li>
											<li ><a href="#">Piso Ivory Cream</a></li>
										</ul>
									</li>
									<li><a href="#">Aires Acondicionados</a></li>
									<li><a href="#">Paquetes de Instalación</a></li>
									<li><a href="#">Herramientas de corte Cerámico</a></li>
								</ul> -->
							</li>
						</ul>
					</div>
					<div class="atencion">
						<div class="icon">
							<img src="<?= site_url('assets/img/iconos/support.svg');?>" alt="">
						</div>
						<div class="texto">
							<a href="">
								<span>Atención a cliente</span>
								(999) 946 03 15
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="large-2 columns">
				<div class="cart">
					<a href="<?= site_url('carrito');?>">
						<div class="icon">
							<img src="<?= site_url('assets/img/iconos/shopping.svg')?>" alt="">
						</div>
						<span class="cantidad">100</span>
					</a>
				</div>
			</div>
		</div>
		
		<div class="header-footer row expanded clearfix">
			<div class="img marquee">
				<a href="" class="camioncito">
					<video id="camionVideo" class="gif" width="100%" loop autoplay muted>
						<source src="<?= site_url('assets/img/camion.mp4');?>" type="video/mp4">
						Your browser does not support the video tag.
					</video>
					<img class="statica" src="<?= site_url('assets/img/12meses.png');?>" alt="">
				</a>
			</div>
		</div>
	</div>
</header>

<script>
	 jQuery(document).ready(function(){
		/*
		* Replace all SVG images with inline SVG
		*/
		jQuery('img.svg').each(function(){
			var $img = jQuery(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			jQuery.get(imgURL, function(data) {
				// Get the SVG tag, ignore the rest
				var $svg = jQuery(data).find('svg');

				// Add replaced image's ID to the new SVG
				if(typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				// Add replaced image's classes to the new SVG
				if(typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass+' replaced-svg');
				}

				// Remove any invalid XML tags as per http://validator.w3.org
				$svg = $svg.removeAttr('xmlns:a');

				// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
				if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
				}

				// Replace image with new SVG
				$img.replaceWith($svg);

			}, 'xml');
		});
	});
</script>