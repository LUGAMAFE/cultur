<div ng-controller="controllerProductosFiltro">
<div class="row categoria-productos">
	<div class="cont-titulo">
		<h1 class="titulo">El mayorista de pisos más grande del sur este</h1>
	</div>
	<div class="filtros">
		<h3 class="titulo">Filtrar por:</h3>
		<div class="cont-filtros clearfix">
			<form action="">
				<div class="items-filtros clearfix">
					<select name="" id="">
						<option value="">Precio</option>
					</select>
					<select name="" id="">
						<option value="">Marca</option>
					</select>
					<select name="" id="">
						<option value="">Color</option>
					</select>
					<select name="" id="">
						<option value="">Material</option>
					</select>
					<select name="" id="">
						<option value="">Uso</option>
					</select>
					<select name="" id="">
						<option value="">Tipo</option>
					</select>
					<select name="" id="">
						<option value="">Apariencia</option>
					</select>
					<select name="" id="">
						<option value="">Acabado</option>
					</select>
					<label class="check-todos">
						Mostrar todos
						<input id="checkbox1" type="checkbox">
					</label>
				</div>
			</form>
		</div>
		<h3 class="titulo">Ordenar por:</h3>
		<div class="cont-filtros small">
			<form action="">
				<div class="items-filtros clearfix">
					<select name="" id="">
						<option value="">--</option>
					</select>
				</div>
			</form>
		</div>
	</div>
	<div class="lista-productos filtro-producto">
		<div class="row small-up-2 medium-up-3 large-up-4">
			
  			<div class="column column-block" ng-repeat="row in productos">
				
  				<div class="cont-productos">
					<div class="info-producto clearfix">
						<div class="descuento">
							<div class="img">
								<img src="<?php echo site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
							</div>
						</div>
						<div class="corona">
							<div class="img">
								<img src="<?php echo site_url('assets/img/iconos/corona.png'); ?>" alt="">
							</div>
						</div>
						<div class="img">
							<a href="<?php echo site_url('producto/prueba-producto');?>">
								<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
								<div class="info">
									<div class="img-info">
										<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
									</div>
								</div>
							</a>
						</div>
						<div class="texto clearfix">
							<h1 class="titulo">Optima navia</h1>
							<p>
								{{row.Descripcion1}} <br>
								33x33 cm - Semibrillante
							</p>
							<p class="precio">
								Desde: $10,299.00 por m2
							</p>
							<span class="codigo">Art. 5590 / 5563</span>
						</div>
						<div class="text-center btn">
							<a href="<?php echo site_url('producto/prueba-producto');?>" class="button expanded success">Ver detalles</a>
						</div>
					</div>
				</div>
		
  			</div>
  			
  		</div>
	</div>
</div>

<div class="titulo-site">
	<h1 class="titulo">Le recomendamos</h1>
</div>

<div class="row lista-productos slick-productos">
	<?php for($i = 0; $i < 3; $i++): ?>
	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img clearfix">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/basin.png');?>');" alt="">
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>
	<?php endfor ?>
</div>

</div>
<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?php echo site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?php echo site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
			});
		});
</script>

<script>
	myApp.controller('controllerProductosFiltro', function($scope, $http, $filter) {
        $scope.productos = [];
        $http.get('<?php echo site_url('home/get_productos');?>', { cache: false }).success(function(data) {
            if(data){
            	console.log(data);
                $scope.productos = data;
            }
        });
    });
</script>