<script>
  $( document ).ready(function() {
    <?php $mensaje = $this->session->flashdata("admin-message"); ?>

    <?php if( isset( $mensaje) ):?>
      Swal.fire({
        <?= is_null($mensaje["title"]) ? "" : "title: '".$mensaje["title"]."',"; ?>
        type: '<?= $mensaje["type"]; ?>',
        text: '<?= $mensaje["message"]; ?>',
        <?= is_null($mensaje["description"]) ? "" : "footer: '".$mensaje["description"]."',"; ?>
      })
    <?php $this->session->unset_userdata("admin-message"); endif; ?>
  });
</script>