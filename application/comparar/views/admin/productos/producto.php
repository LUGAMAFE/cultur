<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Producto</title>
    <?= link_tag('assets/css/admin.css?v=0.5'); ?>
    <?= link_tag('assets/plugins/scrollbar/jquery.scrollbar.css'); ?>
    <?= link_tag('assets/plugins/jquerymodal/jquery.modal.min.css'); ?>
    <?= link_tag('assets/plugins/custominputfile/css/jquery.Jcrop.min.css'); ?>
    <link href="https://cdn.jsdelivr.net/gh/StephanWagner/jBox@v1.0.3/dist/jBox.all.min.css" rel="stylesheet">

    <?= link_tag('assets/plugins/fine-uploader/fine-uploader-gallery.min.css'); ?>

    <script src="<?= site_url('assets/js/modernizr.js'); ?>"></script>
    <script src="<?= site_url('assets/js/jquery.min.js')?>"></script>

    <script src="<?= site_url('assets/plugins/fine-uploader/fine-uploader.min.js'); ?>"></script>
    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Upload a file</div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <div class="qq-thumbnail-wrapper">
                        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                    </div>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                        <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                        Retry
                    </button>

                    <div class="qq-file-info">
                        <div class="qq-file-name">
                            <span class="qq-upload-file-selector qq-upload-file"></span>
                            <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
                        </div>
                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                            <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                            <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                        </button>
                        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                            <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                        </button>
                    </div>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>

    <script src="https://cdn.jsdelivr.net/gh/StephanWagner/jBox@v1.0.3/dist/jBox.all.min.js"></script>

    <script src="<?= site_url('assets/plugins/jquerymodal/jquery.modal.min.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/sortablejs/Sortable.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/jquery-sortablejs/jquery-sortable.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/scrollbar/jquery.scrollbar.min.js'); ?>"></script>
    <script src="<?= site_url('assets/js/producto.js')?>"></script>
    <script>
        const siteURL = "<?= site_url("adminproducto");?>/";
    </script>
</head>
<body>
    <div class="infoProducto">
        <h2 class="titulo">Administración Producto</h2>
        <div class="opciones scrollbar-inner">
            <div class="botonesControl">
                <button id="btn-guardar" class="btn btn-default">Guardar</button>
                <button id="btn-cancelar" class="btn btn-cancel">Cancelar</button>
            </div>
            <div class="doubleOpcion">
                <div class="opcion red">
                    <div class="header">
                        <h3>Datos Basicos</h3>
                    </div>
                    <div class="body">
                        <div class="form-group">
                            <label>Nombre Producto</label>
                            <input type="text" class="form-control" placeholder="Nombre del producto...">
                        </div>
                        <div class="form-group">
                            <label>Imagenes del Producto</label>
                            <div class="upload-area" id="file-drop-area"></div>
                        </div>
                    </div>
                    <div class="footer">
                        <button class="btn btn-default guardar">Guardar</button>
                        <button class="btn btn-cancel cancelar">Cancelar</button>
                    </div>
                </div>
                <div class="opcion red">
                    <div class="header">
                        <h3>Información Producto</h3>
                    </div>
                    <div class="body">
                        <div class="form-group">
                            <label>Descripción Producto</label>
                            <textarea class="form-control" rows="5" placeholder="Descripción ..."></textarea>
                        </div>
                        <div class="form-group">
                            <label>Información Extra Producto</label>
                            <textarea class="form-control" rows="5" placeholder="Información ..."></textarea>
                        </div>
                    </div>
                    <div class="footer">
                        <button class="btn btn-default guardar">Guardar</button>
                        <button class="btn btn-cancel cancelar">Cancelar</button>
                    </div>
                </div>
            </div>
            <div class="opcion red">
                <div class="header">
                    <h3>Información de la del Tabla Producto</h3>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Especificaciones</label>
                        <textarea class="form-control" rows="3" placeholder="Especificaciones ..."></textarea>
                    </div>
                    <div class="form-group">
                        <label>Usos</label>
                        <div class="upload-area" id="usos-drop-area"></div>
                    </div>
                </div>
                <div class="footer">
                    <button class="btn btn-default guardar">Guardar</button>
                    <button class="btn btn-cancel cancelar">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

