<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function key_compare_func($a, $b){
    $keyA = $a["Articulo"];
    $keyB = $b["Articulo"];

    $isNumericA = is_numeric($keyA);
    $isNumericB = is_numeric($keyB);

    if ( $keyA === $keyB ) {
        return 0;
    }
    
    if(!$isNumericA && !$isNumericB){
        $resul = strnatcmp($keyA, $keyB);
        return $resul;
    }
    
    if(!$isNumericA && $isNumericB){
        return -1;
    }
    
    if($isNumericA && !$isNumericB) {
        return 1;
    }

    return ($keyA > $keyB)? 1:-1;
}

function key_compare_func2($a, $b){
    if ( $a["Articulo"] === $b["Articulo"] && $a["Grupo"] === $b["Grupo"]) {
        return 0;
    }else if(!is_numeric($a["Articulo"])){
        return 1;
    }

    return ($a["Articulo"] > $b["Articulo"])? 1:-1;
}

class Productos extends MY_Admin {

    public function __construct(){
        parent::__construct();
        $this->load->model('Productos_Model');
        $this->load->helper('url_helper');
    }

    public function home($url = ''){
        $productos = @file_get_contents('http://slim.test/productosInfo');
        if($productos == FALSE){
            show_404();
        }else{
            $productos = json_decode($productos, true);
            if($productos["error"] == TRUE){
                show_404();
            }else{
                $productos = $productos["resultado"];
                $productosDB = $this->Productos_Model->getProductosInfo();

                $nuevosProductos = array_udiff($productos, $productosDB, "key_compare_func");
                $this->Productos_Model->insertarNuevosProductos($nuevosProductos);
                
                $interseccion = array_uintersect($productos, $productosDB, "key_compare_func");
                $productosCambiosGrupo = array_udiff($interseccion, $productosDB, "key_compare_func2");

                $this->Productos_Model->actualizarGrupoProductos($productosCambiosGrupo);

                $productosTabla = $this->Productos_Model->obtenerProductosTabla();
                foreach ($productosTabla as $productoKey => $productoValue) {
                    $productosTabla[$productoKey]["Descripcion1"] = $productos[$productoKey]["Descripcion1"];
                }

                $datos["productos"] = $productosTabla;
                $datos['class'] = $this->class;
                $this->template->write('title', 'Admin Home');
                $this->loadTemplatesComunes($datos);
            
                $this->template->asset_css('switcher/css/switcher.css');
                $this->template->asset_js('switcher/js/jquery.switcher.js');
                $this->template->asset_css('scrollbar/jquery.scrollbar.css');
                $this->template->asset_js('scrollbar/jquery.scrollbar.min.js');
                $this->template->asset_js('productos.js');
                
                $this->loadDataTables();
                $this->template->write_view('content', $this->folder.'/productos/list', $datos);
                $this->template->render();
            }
        }
    }

    public function actualizarVisibilidadProducto(){
        $idProducto = $this->input->post('idProducto');
        $estado = $this->input->post('estado');

        $error = $this->Productos_Model->actualizarVisibilidadProducto($idProducto, $estado);

        $output = ["error" => !$error];

        $json = json_encode($output);
        echo $json;
    }
}