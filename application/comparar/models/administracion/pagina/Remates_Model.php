<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Remates_Model extends MY_Model {

	public function __construct(){
        parent::__construct("remates_page_data");	
    }

    public function guardarImagenRemates($data, $update){
        if($update){
            $this->db->where($this->llave_primaria, 1);
            $resultado = $this->db->update($this->table, $data);
        }else{
            $resultado = $this->db->insert($this->table, $data);
        }
        return $resultado;
    }

    public function obtenerImagenRemates(){
        return $this->getFirst();
    }
}    