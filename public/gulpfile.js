const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass',()=> 
    gulp.src('assets/css/scss/*.scss')
    .pipe(sass({
        outputStyle: 'compressed',
        sourceComments: true
    }))
    .pipe(autoprefixer({
        versions:['last 2 browsers']
    }))
    .pipe(gulp.dest('./assets/plugins/css'))
    
);

