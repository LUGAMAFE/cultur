$("body").css("overflow-y", "hidden");

$(window).on("load", function() {
    let timeout = 1000;
    setTimeout(() => {
        $(".loader_main .icon").fadeToggle();
    }, (timeout - 400));
    setTimeout(() => {
        $(".loader_main").fadeToggle();
        $("body").css("overflow-y", "auto");
    }, timeout);
    $("body").css("overflow-y", "auto");
});