function is_touch_device() {
    
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    
    var mq = function (query) {
        return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

const callbackMostrarInfo = (entries, observer) => {
    entries.forEach(entry => {
        entry.target.classList.remove("mostrar-info");
        if (window.matchMedia("(min-width: 950px)").matches) {
            
        } else {
            let touch = is_touch_device()
            if (entry.isIntersecting && touch) {
                if(!entry.target.classList.contains('mostrar-info')){
                    setTimeout(() => {
                        entry.target.classList.add("mostrar-info");
                    }, 500);
                }
            } else {
                entry.target.classList.remove("mostrar-info");
            }
        }
    });
};

var observer = new IntersectionObserver(callbackMostrarInfo, { 
    threshold: [0.9],
    rootMargin: "-70px 0px -10px"
});

let cards = document.querySelectorAll(".salones-list .card");

cards.forEach(card => observer.observe(card));