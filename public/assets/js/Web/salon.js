$(".slider-container-section").slick({
    infinite:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots:true,
    arrows:true,
    prevArrow: '<button type="button" class="slick-prev "><img class="svg white" src="assets/img/iconos/left.svg" alt=""></button>',
    nextArrow: '<button type="button" class="slick-next "><img class="svg white" src="assets/img/iconos/right.svg" alt=""></button>',
  });