$(".slider-banner").slick({
  infinite:true,
  centerMode: true,
  variableWidth: true,
  autoplay: true,
  dots:true,
  arrows:true,
  pauseOnHover: false,
  pauseOnFocus: true,
  pauseOnDotsHover: true,
  prevArrow: '<button type="button" class="slick-prev "><img class="svg white" src="assets/img/iconos/left.svg" alt=""></button>',
  nextArrow: '<button type="button" class="slick-next "><img class="svg white" src="assets/img/iconos/right.svg" alt=""></button>',
});
