/* var owl = $(".owl-carousel");
owl.owlCarousel({
    nav: false,
    loop: true,
    // autoplay: true,
    // autoplayTimeout: 2500,
    // autoplayHoverPause: true,
    responsive: {
        0: {
            items: 1,
            margin: 10
        },
        320: {
            items: 2,
            margin: 10
        },
        640: {
            items: 3,
            margin: 10
        },
        950: {
            items: 4,
            margin: 15,
            nav: true
        },
        1250: {
            items: 5,
            margin: 25,
            nav: true
        }
    },
    navText: [
        "<img class='svg' src='assets/img/iconos/left.svg'>",
        "<img class='svg' src='assets/img/iconos/right.svg'>"
    ]
}); */

// var images = document.querySelectorAll('.img-banner > .parallax');
// new simpleParallax(images, {
//     scale: 1.2
// });

// var images = document.querySelectorAll('.img-banner > .parallax-right');
// new simpleParallax(images, {
//     scale: 1.1,
//     orientation: 'right'
// });

var myFullpage = new fullpage('#fullpage', {
	//options here
    verticalCentered: false,
    scrollingSpeed: 650,
    loopTop: true,
    loopBottom: true,
    scrollOverflow: true,
    fitToSection: false,
    navigation: true,
    navigationPosition: 'right',
    // autoScrolling: false,
    // fitToSection: true,
    // fitToSectionDelay: 400
    afterLoad: function(origin, destination, direction){
		let loadedSection = $(destination.item); 
        loadedSection.find(".layer").children().addClass("scaleHome");
    },
    onLeave: function(origin, destination, direction){
        let loadedSection = $(origin.item); 
        loadedSection.find(".layer").children().removeClass("scaleHome");
    }
    
});

//métodos
fullpage_api.setAllowScrolling(true);
