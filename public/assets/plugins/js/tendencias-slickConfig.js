$(document).ready(function() {
    $(".slider-tendencias").slick({
  
      infinite:true,
      centerMode: true,
      centerPadding: '0px',
      slidesToShow: 3,
      arrows:true,
      autoplay:true,
      autoplaySpeed: 3000,
      /* prevArrow: $('.prev'),
      nextArrow: $('.next'), */
      responsive: [
      
        {
          breakpoint: 1000,
          settings: {
            arrows: false,
            centerMode: false,
            centerPadding: '0px',
            slidesToShow: 2,
            dots:true
          }
        },
        {
          breakpoint: 500,
          settings: {
            arrows: false,
            centerMode: false,
            centerPadding: '0px',
            slidesToShow: 1,
            dots:true
          }
        }
      ]
    });
  });