$(document).ready(function() {
    $(".slider-m2").slick({
      infinite:true,
      arrows:true,
      dots: false,
      slidesToShow: 4,
      autoplay:false,
      autoplaySpeed: 3000,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      responsive: [
      
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            arrows: false           
          }
        },
        {
          breakpoint: 850,
          settings: {
            slidesToShow: 2,
            arrows: false       
          }
        },
        {
          breakpoint: 650,
          settings: {
            slidesToShow: 1,
            arrows: false,
            dots:true
          }
        }
      ]
     
    });
  });