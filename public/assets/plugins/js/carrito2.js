
    $(document).ready(function () {
        
        $(".btnCancelar").click(function (e) { 
            
            e.preventDefault();
            $(this).parent().parent().parent().remove();
        });

        $(".unidadesProducto").each(function (index,element) { 
           

            let sumador = element;
            let menos = $(sumador).find(".btn-menos");
            let mas = $(sumador).find(".btn-mas");
            let cantidad = $(sumador).find(".contadorUnidades");

            let contador = 1,
                min = 1,
                max = 9999;


            cambiarCantidad();

            function cambiarCantidad() {
                $(cantidad).val(contador);
            };

            $(cantidad).on("change", function(){
                let val = $(cantidad).val();
                if(val < min){
                    contador = min;
                }else if(val > max){
                    contador = max;
                }else if(isNaN(val)){
                    contador = min;
                }else{
                    contador = val; 
                }
            
                $(cantidad).val(contador);
            });

            $(mas).on('click touch', function() {
                if(contador < max){
                    contador++;
                    cambiarCantidad();
                }
            });

            $(menos).on('click touch', function() {
                if(contador > min){
                    contador--;
                    cambiarCantidad();
                }
            });




        });
        
    });



