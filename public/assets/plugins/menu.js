$(document).ready(function(){
    let $menu = $("#menu").mmenu({
        extensions 	: [ "position-bottom", "fullscreen",  "border-full"],
        navbar: { add: false },
        // navbars		: [
        //     {
        //         content : [ "title", "prev"]
        //     }
        // ],
        // searchfield: {
        //     panel: true,
        //     noResults: "Sin resultados",
        //     placeholder: "Buscar en Menu",
        //     clear: true
        // },
        iconbar: {
            use: true,
            bottom: [
                '<a href="https://www.facebook.com/mayoreoceramicodelapeninsula" class="facebook icono"><i class="fab fa-facebook-square"></i></a>',
                '<a href="https://www.youtube.com/channel/UCBbSj9LP2avuxdnwapLciFw" class="youtube icono"><i class="fab fa-youtube"></i></a>',
            ]
         }
    }, {
        offCanvas: {
            page: {
                selector: "#my-page"
            }
        }
    });    
    $("#menu").toggle();    
});