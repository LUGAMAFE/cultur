$(document).on('ready', function() {

    let sumador = $("#sumadorProductos");
    let menos = $(sumador).find(".menos");
    let mas = $(sumador).find(".mas");
    let cantidad = $(sumador).find(".cantidad");

    let contador = 1,
        min = 1,
        max = 9999;


    cambiarCantidad();

    function cambiarCantidad() {
        $(cantidad).val(contador);
    };

    $(cantidad).on("change", function(){
        let val = $(cantidad).val();
        if(val < min){
            contador = min;
        }else if(val > max){
            contador = max;
        }else if(!Number.isInteger(val)){
            contador = min;
        }else{
            contador = val; 
        }
    
        $(cantidad).val(contador);
    });

    $(mas).on('click touch', function() {
        if(contador < max){
            contador++;
            cambiarCantidad();
        }
    });

    $(menos).on('click touch', function() {
        if(contador > min){
            contador--;
            cambiarCantidad();
        }
    });

    var swiper = new Swiper('.swiper-container-detalles', {
        slidesPerView: 3,
        spaceBetween: 0,
        slidesPerGroup: 3,
        grabCursor: true,
        loop: false,
        speed: 1000,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        loopFillGroupWithBlank: false,
        pagination: {
            el: '.swiper-pagination-detalles',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next-detalles',
            prevEl: '.swiper-button-prev-detalles',
        },
    });

    var swiperUsos = new Swiper('.swiper-container-usos', {
        autoHeight: true,
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows : true,
        },
        observer: true,
        observeParents: true,
        pagination: {
            el: '.swiper-pagination-usos',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next-usos',
            prevEl: '.swiper-button-prev-usos',
        },
    });

    var swiperImgExtras = new Swiper('.swiper-container-imagenes-extra', {
        slidesPerView: 4,
        spaceBetween: 18,
        slidesPerGroup: 1,
        height: 1000,
        speed: 1800,
        //autoHeight: true,
        grabCursor: true,
        centeredSlides: false,
        observer: true,
        observeParents: true,
        slideToClickedSlide: true,
        pagination: {
            el: '.swiper-pagination-imagenes-extra',
            clickable: true,
        },
        breakpoints: {
            // when window width is >= 320px
            930: {
                slidesPerView: 3,
            },
        }
    });

    $('.imagenes-extra .swiper-slide a').click(function(e){
        e.preventDefault();
        //e.stopImmediatePropagation();
    });

    $('.imagenes-extra .swiper-slide').click(function(e){
        $this = $(this);
        $('.imagenes-extra .swiper-slide').removeClass("is-slide-active");
        $this.addClass("is-slide-active");
        let src = $this.attr('src');
        let fancyboxId = $this.attr('fancybox-id');

        // $("#foto-producto-actual").attr('href', src);
        // $("#foto-producto-actual").find('img').attr('src', src);
        let $imagenActiva = $('[data-fancybox-trigger="imagesExtra"]');
        $imagenActiva.attr('data-fancybox-index',fancyboxId);
        $imagenActiva.find('img').attr('style','background-image: url('+src+');');
        $imagenActiva.find('img').attr('data-zoom', src);
        /*$imagenes.each(function(index, el){
            el = $(el);
            let elsrc = el.attr('href');
            if(elsrc == src){
            if(el.attr("id") != "foto-producto-actual"){
                let newSrc = $("#foto-producto-actual").attr('href');
                el.attr('href', newSrc);
                $("#foto-producto-actual").attr('href', elsrc);
                $("#foto-producto-actual").find('img').attr('src', elsrc);
                return false;
            }
            }
        });*/
    });
    
    $('[data-fancybox="imagesExtra"]').fancybox({
        loop: true,
        buttons: [
            "zoom",
            //"share",
            "slideShow",
            "fullScreen",
            //"download",
            "thumbs",
            "close"
        ],
        autoFocus: false,
        backFocus: false,
    });

    $('[data-fancybox="images"]').fancybox({
        loop: true,
        buttons: [
            "zoom",
            //"share",
            "slideShow",
            "fullScreen",
            //"download",
            "thumbs",
            "close"
        ],
        autoFocus: false,
        backFocus: false,
    });

    const shareDialog = document.querySelector('.share-dialog');
    const closeButton = document.querySelector('.close-button');

    $('.compartir').click(function () {
        if (navigator.share) {
            navigator.share({
                title: 'Comparte Nuestro Producto',
                url: 'https://codepen.io/ayoisaiah/pen/YbNazJ'
                }).then(() => {
                console.log('¡Gracias Por Compartir!');
            })
            .catch(console.error);
        } else {
            $.fancybox.open(shareDialog);
        }
    });

    $('#copy-link').click(function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($('#pen-url').text()).select();
        document.execCommand("copy");
        $temp.remove();
    });

    new Drift(document.querySelector(".contenido .foto .imagen img"), {
        paneContainer: document.querySelector(".contenido .descProducto"),
        inlinePane: 930,
        containInline: true,
        hoverDelay: 100,
        hoverBoundingBox: true,
        touchBoundingBox: false,
        zoomFactor: 4,
    });

    $('.anadir').click(function (){
        let id = $(this).attr('id-prod');
        let cantidad = $("#sumadorProductos").find(".cantidad").val();
        $.ajax({
            async: false,
            url: urlCarrito,
            data: {guardar_carrito:true, id_producto:id, cantidad_producto:cantidad},
            type: 'POST',
            success: function(respuesta) {
                console.log(respuesta);
                result = respuesta;
                //cantidadCarrito = parseInt($('#cantidadCarrito').text());
                $('#cantidadCarrito').text(result);
                new jBox('Notice', {
                    content: 'Producto añadido a cotización', 
                    color: 'green',
                    showCountdown: true,
                    autoClose: 3000,
                    delayOnHover: true,
                    attributes: {y: 'bottom'}}).open();
            },
            error: function() {
                result = "";
                new jBox('Notice', {
                    content: 'Error al añadir producto', 
                    color: 'red',
                    showCountdown: true,
                    autoClose: 3000,
                    delayOnHover: true,
                    attributes: {y: 'bottom'}}).open();
            }
        });
    });
});