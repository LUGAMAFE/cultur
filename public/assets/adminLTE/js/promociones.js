//FINE UPLOADERS LOGIC
let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    multiple: false,
    alertOverride: true,
    filesType: "image",
    validation: {
        allowedExtensions: "jpg jpeg png",
    }
});

uploadersManager.addUploadGroup(".upload-area.multiple", {
    multiple: true,
    validation: {
        itemLimit: 100,
        image:{
            minWidth: 1000,
            minHeight: 350,
            maxWidth: 2000,
            maxHeight: 800,
        }
    },
    callbacks: {
        onSessionRequestComplete: function(response, success, xhrOrXdr){
            let elems = this.getUploads();
            response.forEach((element, index) => {
                let inputExtras = JSON.parse(element.inputExtras);
                fileItemContainer = this.getItemByFileId(index);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group checkbox-options">
                                <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" data-name-extra="activo_promocion" id="activo_promocion-${element.uuid}" ${(inputExtras.activo_promocion != 0) ? "checked" : ""}>
                                <label class="custom-control-label" for="activo_promocion-${element.uuid}">Activo</label>
                                </div>
                            </div>
                        </div>`
                );
            });
        },
        onSubmitted: function(id, name){
            fileItemContainer = this.getItemByFileId(id);
            let uuid = this.getUuid(id);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group checkbox-options">
                                <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" data-name-extra="activo_promocion" id="activo_promocion-${uuid}">
                                <label class="custom-control-label" for="activo_promocion-${uuid}">Activo</label>
                                </div>
                            </div>
                        </div>`
                );
        }
    }
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Una Imagen para el banner",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});
