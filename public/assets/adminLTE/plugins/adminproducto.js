// let itsmeeeDIOOO = JSON.stringify(jsonProductos);
// console.log(itsmeeeDIOOO);
$('.summer').summernote({
    height: "200",                 // set editor height
    minHeight: 100,             // set minimum height of editor
    toolbar: [
        ['style', ['style', 'clear']],
        ['font', ['bold', 'underline', 'italic']],
        //['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', /*'picture',*/ 'video', 'hr']],
        ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
    ],
    codemirror: { // codemirror options
        theme: 'monokai'
    },
    fontNames: ['Arial', 'Raleway'],
    fontNamesIgnoreCheck: ['Raleway'],
    lang: 'es-ES',
    // onKeyup: function(e) {
    //     $('textarea[name="informacion"]').val(this.code());
    // },
    placeholder: 'Escribe las especificaciones del producto aqui!'
});

$('.note-editable').css('font-family','Raleway');

const isset = function(item){
    let type = typeof item;
    return (item != null && type != "undefined" && type != "null");
}

//FINE UPLOADERS LOGIC
let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        getProductosAsoc();
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    alertOverride: true,
    filesType: "image",
    sharedParams : {
        idProducto: idProducto,
    },
    validation: {
        allowedExtensions: "jpg jpeg png",
        itemLimit: 50,
    }
});

uploadersManager.addUploadGroup(".upload-area.img-productos", {
    validation: {
        image: {
            minWidth: 200,
            minHeight: 200,
        }
    },
    callbacks: {
        onSessionRequestComplete: function(response, success, xhrOrXdr){
            let elems = this.getUploads();
            response.forEach((element, index) => {
                let inputExtras = isset(element.inputExtras) ? JSON.parse(element.inputExtras) : null;
                fileItemContainer = this.getItemByFileId(index);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                <input type="radio" class="custom-control-input" data-name-extra="imagen_principal" name="radio-img-principal" id="imagenPrincipal-${element.uuid}" 
                                ${(isset(inputExtras) && isset(inputExtras.imagen_principal) && inputExtras.imagen_principal != 0) ? "checked" : ""} required>
                                <label class="custom-control-label" for="imagenPrincipal-${element.uuid}">Principal</label>
                                </div>
                            </div>
                        </div>`
                );
            });
        },
        onSubmitted: function(id, name){
            fileItemContainer = this.getItemByFileId(id);
            let uuid = this.getUuid(id);
                $(fileItemContainer).find(".qq-file-info")
                .append(`<div class="extras-uploader">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                <input type="radio" class="custom-control-input" data-name-extra="imagen_principal" name="radio-img-principal" id="imagenPrincipal-${uuid}" required>
                                <label class="custom-control-label" for="imagenPrincipal-${uuid}">Principal</label>
                                </div>
                            </div>
                        </div>`
                );
        }
    }
});

uploadersManager.addUploadGroup(".upload-area.img-usos", {
    validation: {
        image: {
            minWidth: 400,
            minHeight: 200,
        }
    }
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;

    let summerempty = $('.summer').summernote('isEmpty') || $('.summer').summernote('code').trim() === "";

    if(summerempty){
        let closestRequerido = $('.summer').closest(".requerido").tooltip({
            title: "El Campo Esta Incompleto",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    }

    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Alguna Imagen para el producto",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        setFilterData();
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});

if($("#input-producto-descuento").prop("checked")){
    $("input[name='descuento']").show();
    $("input[name='descuento']").prop('required',true);
}

$('#input-producto-descuento').on("change", function(){
    let input = $("#input-producto-descuento").prop("checked");
    if(input) {
        $("input[name='descuento']").slideDown();
        $("input[name='descuento']").prop('required',true);
    }else{
        $("input[name='descuento']").slideUp();
        $("input[name='descuento']").removeAttr('required');
    }
});

function pickTextColorBasedOnBgColorAdvanced(bgColor, lightColor, darkColor) {
    var color = (bgColor.charAt(0) === '#') ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map((col) => {
      if (col <= 0.03928) {
        return col / 12.92;
      }
      return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = (0.2126 * c[0]) + (0.7152 * c[1]) + (0.0722 * c[2]);
    return (L > 0.179) ? darkColor : lightColor;
}

function hasStep(element, decimals){
    var step = "1";
    if(decimals!=null){
        step = "0";
        step += ".";
        while(decimals>1){
            step += "0";
            decimals = decimals - 1;
        }
        step += "1";
    }
    $(`${element}`).attr('step',step);
}

function hasBorder(element, color){
    var borderColor = pickTextColorBasedOnBgColorAdvanced(color, '#FFFFFF', '#000000');
    var elementStyle = $(`${element}`).attr('style');
    if(borderColor == '#000000'){
        $(`${element}`).attr('style',elementStyle + 'border:1px solid; border-color:black;');
    }
}

function getFilterInputValue(element,isLast){
    let filterType = element.attr("filterType");
    let inputFilter = '';
    let result = '';
    switch(filterType){
        case"string":
        case"number":
            inputFilter = element.find("input").val();
            result += inputFilter;
            break; 
        case"select":
            inputFilter = element.find("select").val();
            result += inputFilter;
            break;
        case"radio":
        case"color":
            inputFilter = element.find("input:checked").val();
            result += inputFilter;
            break;
        case"checkbox":
            inputFilter = element.find('.checkbox-options :checkbox:checked');
            //let checkboxLength = inputFilter.length;
            result = [];
            inputFilter.each(function(index){
                if($(this).is(':checked')) {
                    result.push($(this).val());
                }
            });
            break;
    }
    return result;
}

function setFilterData() {
    var filtros = $('.filter-item');
    var length = filtros.length;
    var filtrosResultJson = {};
    filtros.each(function(index){
        //filtrosResultJson += '"'+$(this).attr("id")+'":';
        if (index === (length - 1)) {
            filtrosResultJson[$(this).attr("id")] = getFilterInputValue($(this),true);
        } else {
            filtrosResultJson[$(this).attr("id")] = getFilterInputValue($(this),false);
        }
    });
    let json = JSON.stringify(filtrosResultJson);
    $('#datos-filtro').val(json);
};

let timeOutBusqueda;
let lastResults;

const productosSeleccionados = {
    productos: [],
    addProducto: function (id, element) {
        if(this.productos.includes(id)){
            return false;
        }
        this.productos.push(id);
        let template = `
        <div class="producto list-group-item list-group-item-action p-2 px-3 border-success" data-id="${element.id_mcp}">
            <div class="media row align-items-center">
                <div class="col-2">
                    <img src="${element.imagen_producto}" class="img-fluid rounded mx-auto d-block">
                </div>
                <div class="media-body col-10">
                    <div class="d-flex w-100 justify-content-between">
                        <p class="text-muted mb-0"><span class="font-weight-bold">Nombre: </span>${element.nombre_producto}</p>
                        <small class="text-muted"><span class="font-weight-bold">Articulo: </span>${element.id_mcp}</small>
                    </div>
                    <p class="mb-1">${element.desc_mcp}</p>
                    <div class="d-flex w-100 justify-content-between">
                        <div class="d-flex flex-column w-50"> 
                            <small class="text-muted d-block"><span class="font-weight-bold">Categoria: </span>${element.nombre_categoria}</small>
                            <small class="text-muted d-block"><span class="font-weight-bold">Subcategoria: </span>${element.nombre_subcategoria}</small>
                        </div>
                        <div class="d-flex flex-column w-50 justify-content-center align-items-end"> 
                            <button type="button" class="btn btn-danger p-0 rounded-circle delete-product" style="height: 1.7rem;line-height: 1.5rem; width: 1.7rem;"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                </div> 
            </div>
        </div>`;
        let lista = $(".lista-productos.asociados-seleccionados");
        lista.append(template);
        return true;
    },
    deleteProducto: function (id) {
        let index = this.productos.indexOf(id);
        if(index <= -1){
            return false;
        }
        this.productos.splice(index, 1);
        let aborrar = $(".lista-productos.asociados-seleccionados").children().eq(index);
        aborrar.remove();
        return true;
    }
};

function initProductosRelacionados() {
    productosRelacionados.forEach(element => {
        productosSeleccionados.addProducto(element.id_mcp, element);
    });
}

initProductosRelacionados();

//Busqueda de productos relacionados
$("#productos-asociados-input").on("keyup search", function(){
    clearTimeout(timeOutBusqueda);
    let val = $(this).val().trim();
    if(val.length <= 0){
        $(".lista-productos.asociados-busqueda").html("");
        return;
    }
    let template = `
    <div class="list-group-item list-group-item-action p-2">
        <div class="media row align-items-center">
            <div class="media-body col-12">
                <div class="d-flex w-100 align-items-center justify-content-between">
                    <p class="text-muted mb-0"><span class="font-weight-bold">Buscando...</p>
                    <div class="spinner-border text-secondary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div> 
        </div>
    </div>`;
    $(".lista-productos.asociados-busqueda").html(template);
    searchProduct(val);
});
function searchProduct(val){
timeOutBusqueda = setTimeout(() => {
    var options = {
        shouldSort: true,
        tokenize: true,
        threshold: 0,
        location: 0,
        distance: 100,
        maxPatternLength: 100,
        minMatchCharLength: 1,
        keys: [{
            name: 'id_mcp',
            weight: 0.5
            }, {
            name: 'desc_mcp',
            weight: 0.2
            }, {
            name: 'nombre_producto',
            weight: 0.3
        }]
    };
    let fuse = new Fuse(jsonProductos, options), // "list" is the item array
    result = fuse.search(val),
    utilRedults;
    if(result.length < 10){
        utilRedults = result;
    }else{
        utilRedults = result.slice(0, 8);
    }
    lastResults = utilRedults;
    getTemplateForFilterResults();
}, 0);
}

function getTemplateForFilterResults(){
    template = ``;
    if(lastResults.length <= 0){
        template += `
    <div class="list-group-item list-group-item-action p-2">
        <div class="media row align-items-center">
            <div class="media-body col-12">
                <div class="d-flex w-100 align-items-center justify-content-between">
                    <p class="text-muted mb-0"><span class="font-weight-bold">Ningun Resultado Encontrado.</p>
                </div>
            </div> 
        </div>
    </div>`;
    }else{
        lastResults.forEach(element => {
            let encontrado = productosSeleccionados.productos.filter(function(producto){
                return producto == element.id_mcp;
            });
            if(encontrado.length <= 0){
                template += `
                <div class="producto list-group-item list-group-item-action list-group-item-light p-2 px-3" data-id="${element.id_mcp}">
                    <div class="media row align-items-center">
                        <div class="col-2">
                            <img src="${element.imagen_producto}" class="img-fluid rounded mx-auto d-block">
                        </div>
                        <div class="media-body col-10">
                            <div class="d-flex w-100 justify-content-between">
                                <p class="text-muted mb-0"><span class="font-weight-bold">Nombre: </span>${element.nombre_producto}</p>
                                <small class="text-muted"><span class="font-weight-bold">Articulo: </span>${element.id_mcp}</small>
                            </div>
                            <p class="mb-1">${element.desc_mcp}</p>
                            <div class="d-flex w-100 justify-content-between">
                                <div class="d-flex flex-column w-50"> 
                                    <small class="text-muted d-block"><span class="font-weight-bold">Categoria: </span>${element.nombre_categoria}</small>
                                    <small class="text-muted d-block"><span class="font-weight-bold">Subcategoria: </span>${element.nombre_subcategoria}</small>
                                </div>
                                <div class="d-flex flex-column w-50 justify-content-center align-items-end"> 
                                    <button type="button" class="btn btn-success p-0 rounded-circle add-product" style="height: 1.7rem;line-height: 1.5rem; width: 1.7rem;"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>`;
            }
        });
    }
    $(".lista-productos.asociados-busqueda").html(template);

}

//Añadir Producto a lista
$("body").on("click", function(event){
    let esLista = $(event.target).closest(".lista-productos.asociados-busqueda").length;
    if(!esLista && !$(event.target).is("#productos-asociados-input")){
        $(".lista-productos.asociados-busqueda").html("");
    }
});

$("body").on("click", ".lista-productos.asociados-busqueda .add-product", function(){
    let $this = $(this);
    let id = $this.parents(".producto").attr("data-id");
    let element = lastResults.find(function (product) {
        return product.id_mcp == id;
    });
    if(productosSeleccionados.addProducto(id, element)){
        $this.toggleClass("btn-danger btn-success delete-product add-product");
        $this.children().toggleClass("fa-plus fa-minus");
    }
});

$("body").on("click", ".lista-productos .delete-product", function(){
    let $this = $(this);
    let id = $this.parents(".producto").attr("data-id");
    if(productosSeleccionados.deleteProducto(id)){
        $this.toggleClass("btn-danger btn-success delete-product add-product");
        $this.children().toggleClass("fa-plus fa-minus");
    }
});

function getProductosAsoc(){
    let jsonProductos = JSON.stringify(productosSeleccionados.productos);
    let $inputProductos = $(`input[name="input-productos-asociados"]`);
    if($inputProductos.length <= 0){
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "input-productos-asociados").val(jsonProductos);
        $('#form').append(input);
    }else{
        $inputProductos.val(jsonProductos);
    }
    return true;
}