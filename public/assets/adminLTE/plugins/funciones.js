const BASE_URL = $('base[ href ]').attr('href');

const $_SELECT_PICKER = $('.selectpicker-pelicula');

var valor_anterior = "";

$_SELECT_PICKER.find('option').each((idx, elem) => {
    const $OPTION = $(elem);
    const IMAGE_URL = $OPTION.attr('data-thumbnail');

    if (IMAGE_URL) {
        $OPTION.attr('data-content', "<img style='width: 50px; margin-right: 5px;' src='%i'/> %s".replace(/%i/, BASE_URL + IMAGE_URL).replace(/%s/, $OPTION.text()))
    }

    console.warn('option:', idx, $OPTION)
});


$('.selectpicker-sala, .selectpicker-idioma, .selectpicker-dia').selectpicker();
$_SELECT_PICKER.selectpicker();

$('.form_dates input').datepicker({
    language: "es",
    format: "DD d MM yyyy",
    startDate: new Date(),
    todayHighlight: true,
    multidate: true
});

/*inicializar time picker de todos los inputs necesarios 
(solo uno si la funcion es nueva o los que esten guardados si es una funcion a modificar)*/
for (i = 0; i < numHoras; i++) {
    $('#time'+i).bootstrapMaterialDatePicker({ 
        format: 'hh:mm A',
        shortTime: true,
        date: false 
    }).on('change', 
    function(e, date)
    {
        $( ".input-hora" ).each(function( index ) {
            if($(this).val()==$('#time'+i).val()&&$(this).attr('id')!=$('time'+i).attr('id')){
                $('#time'+i).val(valor_anterior);
            }
        });

    });    

    $('#time'+i).bootstrapMaterialDatePicker().on('beforeChange', 
    function(e, date)
    {
        valor_anterior = $('#time'+i).val();
    });
}

// añadir horario
$("#addRow").click(function () {
    var numElement = $("input[name='fecha_funcion[]']").length;
    var valor_anterior = '';
    var html = '';
    html += '<input class="form-control input-hora" size="16" type="text" id="time'+numElement+'" name="fecha_funcion[]" required readonly>';

    $('#newRow').append(html);

    $('#time'+numElement).bootstrapMaterialDatePicker({ 
        format: 'hh:mm A',
        shortTime: true,
        date: false 
    }).on('change', 
    function(e, date)
    {
        $( ".input-hora" ).each(function( index ) {
            if($(this).val()==$('#time'+numElement).val()&&$(this).attr('id')!=$('#time'+numElement).attr('id')){
                $('#time'+numElement).val(valor_anterior);
            }
        });

    });

    $('#time'+numElement).bootstrapMaterialDatePicker().on('beforeChange', 
    function(e, date)
    {
        valor_anterior = $('#time'+numElement).val();
    });
});

