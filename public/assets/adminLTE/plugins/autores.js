//FINE UPLOADERS LOGIC
let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    multiple: false,
    alertOverride: true,
    filesType: "image",
    sharedParams : {
        idAutor: idAutor,
    },
    validation: {
        allowedExtensions: "jpg jpeg png",
    }
});

uploadersManager.addUploadGroup(".upload-area.individual", {
    multiple: false,
    validation: {
        image:{
            minWidth: 500,
            minHeight: 500,
            maxWidth: 3840,
            maxHeight: 1600,
        }
    }
});

uploadersManager.addUploadGroup(".upload-area.multiple", {
    multiple: true,
    validation: {
        itemLimit: 100,
        image:{
            minWidth: 500,
            minHeight: 500,
            maxWidth: 3840,
            maxHeight: 1600,
        }
    }
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Una Imagen para el autor",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});
