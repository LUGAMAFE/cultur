//GENERAL UTILITIES
function isPositiveInteger(n) {
    return 0 === n % (!isNaN(parseFloat(n)) && 0 <= ~~n);
}
function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
function ucFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// Init Filters Array
let filtersConfig = {
    schema: [
    ],
};

let json = JSON.stringify(filtersConfig);

// COLOR PICKER EXAMPLE
$('.colorpicker-example').colorpicker();

$('.colorpicker-example').on('colorpickerChange', function(event) {
    $('.colorpicker-example .fa-square').css('color', event.color.toString());
});

// FILTER UTILITIES
function getFilterNameByType(type){
    let name;
    switch (type) {
        case "string":
            name = "Texto";
            break;
        case "number":
            name = "Numérico";
            break;
        case "select":
            name = "Selección Despliegue";
            break;
        case "radio":
            name = "Selección Menú";
            break;
        case "checkbox":
            name = "Selección Múltiple";
            break;
        case "color":
            name = "Color";
            break;    
    }
    return name;
}
function getFilterNameTypeById(numberFilter){
    let name;
    switch (numberFilter) {
        case "1":
            name = "Texto";
            break;
        case "2":
            name = "Numérico";
            break;
        case "3":
            name = "Selección Despliegue";
            break;
        case "4":
            name = "Selección Menú";
            break;
        case "5":
            name = "Selección Múltiple";
            break;
        case "6":
            name = "Color";
            break;    
    }
    return name;
}
function getFilterTypeById(numberFilter){
    let name;
    switch (numberFilter) {
        case "1":
            name = "string";
            break;
        case "2":
            name = "number";
            break;
        case "3":
            name = "select";
            break;
        case "4":
            name = "radio";
            break;
        case "5":
            name = "checkbox";
            break;
        case "6":
            name = "color";
            break;    
    }
    return name;
}
function getFilterIdByUuid(uuid){
    return filtersConfig.schema.findIndex(function (element){
        if(element.uuid == uuid){
            return true;
        }
    });
}
function checkNameOfCategory(nameFilter){
    let repeatedName = false;
    let schema = filtersConfig.schema;
    schema.forEach(element => {  
        if(element.name.toUpperCase() == nameFilter){
            repeatedName = true;
            return;
        };
    });
    return repeatedName;
}
function getInitConfigForElementByType(type, element){
    switch (type) {
        case "string":
            element["minLength"] = 0;
            element["maxLength"] = 0;
            break;
        case "number":
            element["minLength"] = 5;
            element["maxLength"] = 70;
            element["isFloat"] = true;
            element["numberOfDecimals"] = 2;
            break;
        case "select": 
        case "radio":         
        case "checkbox":
        case "color":
            element["enum"] = [];
            break;                 
        default:
            break;
    }
}
function getSchemaPropertyByUuid(uuid, property){
    return getSchemaPropertyById(getFilterIdByUuid(uuid), property);
}
function getSchemaPropertyById(id, property){
    return filtersConfig.schema[id][property];
}
function changeElementSchemaPropertyByUuid(uuid, property, value){
    return changeElementSchemaPropertyById(getFilterIdByUuid(uuid), property, value);
}
function changeElementSchemaPropertyById(id, property, value){
    filtersConfig.schema[id][property] = value;
}
function getParentFilterItem($element){
    return $element.parents(".filter-item");
}
function getParentElementUuid($element){
    return getParentFilterItem($($element)).attr("uuid");
}

// SELECTION FILTER FUNCTIONALITY
$('.filter-selector select').on('change', function (e) {
    let optionSelected = $("option:selected", this),
        valueSelected = this.value;
    switchFilterExample(valueSelected);
    $(".filter-example").show();
});
function switchFilterExample(numberFilter){
    $(".filter-example .ejemplo").hide();
    let nombreEjemplo;
    switch (numberFilter) {
        case "1":
            nombreEjemplo = "texto";
            break;
        case "2":
            nombreEjemplo = "numerico";
            break;
        case "3":
            nombreEjemplo = "despliegue";
            break;
        case "4":
            nombreEjemplo = "menu";
            break;
        case "5":
            nombreEjemplo = "multiple";
            break;
        case "6":
            nombreEjemplo = "color";
            break;    
    }
    $(".filter-example .type-filter-name").text(getFilterNameTypeById(numberFilter));
    $(".filter-example .ejemplo."+nombreEjemplo).show();
}

//FILTER SELECTION
$('.filter-selector .add').on('click', function (e) {
    let $this = $(this),
    filterSelector = $this.parent().parent().find("select"),
    // optionSelected = $("option:selected", filterSelector),
    valueSelected = filterSelector.val();
    if(valueSelected == null){
        new jBox('Notice', {
            content: "Eligue el tipo de filtro a crear", 
            color: 'red',
            showCountdown: true,
            autoClose: 3500,
            delayOnHover: true,
            attributes: {y: 'bottom'}});
        return;    
    }
    showFilterCreationOptions();
});
function showFilterCreationOptions(){
    let filterSelectorBtns = $(".filter-selector .input-group-append");
    filterSelectorBtns.find(".add").hide();
    filterSelectorBtns.find(".cancel").show();
    $(".filter-example").show();
    $(".filter-name-input").removeClass("hidden");
}
function hideFilterCreationOptions(){
    let filterSelectorBtns = $(".filter-selector .input-group-append");
    filterSelectorBtns.find(".cancel").hide();
    filterSelectorBtns.find(".add").show();
    $(".filter-example").hide();
    $(".filter-name-input").addClass("hidden");
}
$('.filter-selector .cancel').on('click', function (e) {
    hideFilterCreationOptions();
});

// EDIT FILTER NAME
function setEditableFilterNames(){
    $('.name-filter span').editable({
        type: 'text',
        validate: function(value) {
            let uuid = getParentElementUuid($(this));
            let oldvalue = $(this).data('editable').value;
            value = value.toLowerCase().trim();
            if(value == '') return 'El nombre del filtro es requerido';
            if(nombreFiltroNoRepetido(value)) return ` El nombre: ${value}. Ya esta siendo usado por otro filtro`;
            changeElementSchemaPropertyByUuid(uuid, "name", value);
        }
    });
}
function nombreFiltroNoRepetido(nombreNuevo){
    let filtros = filtersConfig.schema;
    let igual = false;
    filtros.forEach(element => {
        if(element.name.toLowerCase() == nombreNuevo){
            igual = true;
            return true;
        }
    });
    return igual;
};

//CHANGE STATUS FILTER
$(document).on("change", ".filter-item-header .input-active-filter", function(){
    let $this = $(this),
    isSelected = $this.prop('checked'),
    uuid = getParentElementUuid($this),
    $activeLabel = $this.parent().find(".active-label");
    changeElementSchemaPropertyByUuid(uuid, "active", isSelected);
    if(isSelected){
        $activeLabel.text("Activo");
    }else{
        $activeLabel.text("Inactivo");
    }
    $activeLabel.toggleClass("text-success text-danger");
});

// INIT FILTER INSERTION
function insertFilters(filtersArray){
    let output = "",
    filtersList = $(".filters-builder > .filter-group-container .filter-group-body .filters-list");
    filtersArray.forEach(element => {
        output += getFilterByTemplate(element);
    });
    filtersList.append(output);
    setColorPickers();
    setEditableFilterNames();
    setEditableSelectOptions();
}

// FILTER DELETION
$(document).on("click", ".filter-item-header .delete", function(){
    let $this = $(this);
    let $parent = $this.parent().parent(),
    uuid = getParentElementUuid($this),
    index = getFilterIdByUuid(uuid);
    filtersConfig.schema.splice(index, 1);
    $parent.parent().remove();
});

// DYNAMIC FILTER INSERTION 
$(".filter-name-input .continue").on('click', function (e) {
    let nameFilter = $(".filter-name-input input").val().trim();
    filterSelector = $('.filter-selector').find("select"),
    // optionSelected = $("option:selected", filterSelector),
    valueSelected = filterSelector.val();
    if(nameFilter == ""){
        new jBox('Notice', {
            content: "Introduce un nombre para el filtro nuevo", 
            color: 'red',
            showCountdown: true,
            autoClose: 3500,
            delayOnHover: true,
            attributes: {y: 'bottom'}});
        return;
    }
    let filtersList = $(".filters-builder > .filter-group-container .filter-group-body .filters-list");

    if(checkNameOfCategory(nameFilter.toUpperCase())){
        new jBox('Notice', {
            content: "El nombre del filtro ya existe", 
            color: 'red',
            showCountdown: true,
            autoClose: 3500,
            delayOnHover: true,
            attributes: {y: 'bottom'}});
        return;
    }
    let  uuid = uuidv4(),
    type = getFilterTypeById(valueSelected);
    let element = {
        type: type,
        name: nameFilter,
        uuid: uuid,
        active: true,
    };
    getInitConfigForElementByType(type, element);
    filtersConfig.schema.push(element);

    hideFilterCreationOptions();
    $(".filter-name-input input").val("");

    let filterTemplate = getFilterByTemplate(element);
    filtersList.append(filterTemplate);
    setEditableFilterNames();
});
function getFilterTemplateByType(type, element){
    let template;
    switch (type) {
        case "string":
            template = `
<div class="filter-desc">
    <p class="text-info m-0">Con el filtro de texto el usuario podrá buscar dentro de los productos de una categoría un texto. Ya que se agregará un campo nuevo de texto a cada producto de la categoría. El texto contendrá información acerca del producto.</p>
</div>
<hr>
<div class="filter-item-config filter-text-item-config">
    <div class="form-row align-items-center">   
        <div class="form-group col-auto">
            <label>Longitud mínima digitos</label>
            <input type="number" class="min-lenght form-control form-control-sm" min="0" value="${element.minLength}">
            <div class="invalid-feedback">
            
            </div>
        </div>
        <div class="form-group col-auto">
            <label>Longitud maxima digitos</label>
            <input type="number" class="max-lenght form-control form-control-sm" min="0" value="${element.maxLength}">
            <div class="invalid-feedback">
            </div>
        </div>
    </div>
</div>
            `;
            break;
        case "number":
            template = `
<div class="filter-desc">
    <p class="text-info m-0">Con el filtro numérico el usuario podrá buscar por algún valor numérico dentro de los productos de una categoría. Ejemplos modelo, precio, km, año etc...</p>
</div>
<hr>
<div class="filter-item-config filter-number-item-config">
    <div class="form-row align-items-center">
        <div class="form-group col-auto">
            <label>Valor mínimo</label>
            <input type="number" class="min-lenght form-control form-control-sm" min="0" value="${element.minLength}">
            <div class="invalid-feedback">
            
            </div>
        </div>
        <div class="form-group col-auto">
            <label>Valor máximo</label>
            <input type="number" class="max-lenght form-control form-control-sm" min="0" value="${element.maxLength}">
            <div class="invalid-feedback">
            </div>
        </div>
    </div>
    <div class="form-group mt-2">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input input-is-float" name="${element.name}-isFloat-check" id="${element.name}-isFloat-check" ${element.isFloat == true ? "checked" : ""}>
            <label class="custom-control-label" for="${element.name}-isFloat-check">Permitir numeros con decimales</label>
        </div>
    </div>
    <div class="form-group">
        <label>Numero de decimales permitidos</label>
        <input type="number" class="form-control form-control-sm input-number-decimals" min="0" ${element.isFloat == true ? "" : "disabled"} value="${element.numberOfDecimals}">
        <div class="invalid-feedback">
        </div>
    </div>
    
</div>
            `;
            break;  
        case "select":
template = `
<div class="filter-desc">
    <p class="text-info m-0">Con el filtro de Selección Despliegue los usuarios podrán buscar por una sola opción de las diferentes opciones asignadas al producto de la categoría, con la forma de un menú desplegable.</p>
</div>
<hr>
<div class="filter-item-config filter-select-item-config">
    <div class="form-row add-element">
        <div class="form-group col-auto anadir">
            <button type="button" class="btn btn-block btn-info btn-xs btn-agregar"><i class="fas fa-plus"></i> Agregar Opción</button>
            <button type="button" class="btn btn-danger btn-xs btn-cancel hidden"><i class="fas fa-times"></i> Cancelar Agregar</button>
        </div>
        <div class="form-group col-auto anadir-nombre hidden">
            <div class="input-group input-group-xs">
                <input class="form-control form-control-sm input-name" type="text" placeholder="Nombre de la nueva opción">
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary btn-xs continue">Continuar <i class="fas fa-arrow-right"></i></button>
                </div>
                <div class="invalid-feedback">
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="option-items"> 
        <p class="font-weight-bold text-primary">Opciones: </p> 
        <ul class="d-inline-block options-list-items select-list-items">   
`;
            element.enum.forEach(option => {
                template += `
    <li class="font-weight-bold font-italic text-lowercase option-list-item"><button type="button" class="btn btn-danger btn-xs delete-option"><i class="fas fa-times"></i></button><span class="option-name">${option}</span></li>  
                `;
            });
            template += `
        </ul>  
    </div>
</div>`      
            break;
        case "radio":
template = `
<div class="filter-desc">
    <p class="text-info m-0">Con el filtro de Selección Menú los usuarios podrán buscar por una única opción de las diferentes opciones de búsqueda asignadas al producto de la categoría.</p>
</div>
<hr>
<div class="filter-item-config filter-radio-item-config">
    <div class="form-row add-element">
        <div class="form-group col-auto anadir">
            <button type="button" class="btn btn-block btn-info btn-xs btn-agregar"><i class="fas fa-plus"></i> Agregar Opción</button>
            <button type="button" class="btn btn-danger btn-xs btn-cancel hidden"><i class="fas fa-times"></i> Cancelar Agregar</button>
        </div>
        <div class="form-group col-auto anadir-nombre hidden">
            <div class="input-group input-group-xs">
                <input class="form-control form-control-sm input-name" type="text" placeholder="Nombre de la nueva opcion">
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary btn-xs continue">Continuar <i class="fas fa-arrow-right"></i></button>
                </div>
                <div class="invalid-feedback">
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="radio-items"> 
        <p class="font-weight-bold text-primary">Opciones: </p> 
        <ul class="d-inline-block options-list-items radio-list-items">   
`;
            element.enum.forEach(option => {
                template += `
    <li class="font-weight-bold font-italic text-lowercase option-list-item"><button type="button" class="btn btn-danger btn-xs delete-option"><i class="fas fa-times"></i></button><span class="option-name">${option}</span></li>  
                `;
            });
            template += `
        </ul>  
    </div>
</div>`            
            break;          
        case "checkbox":
            template = `
<div class="filter-desc">
    <p class="text-info m-0">Con el filtro de Selección Múltiple los usuarios podrán buscar por diferentes opciones de búsqueda asignadas al producto de la categoría.</p>
</div>
<hr>
<div class="filter-item-config filter-checkbox-item-config">
    <div class="form-row add-element">
        <div class="form-group col-auto anadir">
            <button type="button" class="btn btn-block btn-info btn-xs btn-agregar"><i class="fas fa-plus"></i> Agregar Opción</button>
            <button type="button" class="btn btn-danger btn-xs btn-cancel hidden"><i class="fas fa-times"></i> Cancelar Agregar</button>
        </div>
        <div class="form-group col-auto anadir-nombre hidden">
            <div class="input-group input-group-xs">
                <input class="form-control form-control-sm input-name" type="text" placeholder="Nombre de la nueva opcion">
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary btn-xs continue">Continuar <i class="fas fa-arrow-right"></i></button>
                </div>
                <div class="invalid-feedback">
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="checkbox-items"> 
        <p class="font-weight-bold text-primary">Opciones Multiples: </p> 
        <ul class="d-inline-block options-list-items checkbox-list-items">   
`;
            element.enum.forEach(option => {
                template += `
    <li class="font-weight-bold font-italic text-lowercase option-list-item"><button type="button" class="btn btn-danger btn-xs delete-option"><i class="fas fa-times"></i></button><span class="option-name">${option}</span></li>  
                `;
            });
            template += `
        </ul>  
    </div>
</div>`    
            break;
        case "color":
            template = `
<div class="filter-desc">
    <p class="text-info m-0">Con el filtro de tipo color puedes crear difrrentes colores para posteriormente asignarlos a los productos de la categoria</p>
</div>
<hr>
<div class="filter-item-config filter-color-item-config">
    <div class="form-row add-element">
        <div class="form-group col-auto anadir">
            <button type="button" class="btn btn-block btn-info btn-xs btn-agregar"><i class="fas fa-plus"></i> Agregar Color</button>
            <button type="button" class="btn btn-danger btn-xs btn-cancel hidden"><i class="fas fa-times"></i> Cancelar Agregar</button>
        </div>
        <div class="form-group col-auto anadir-nombre hidden">
            <div class="input-group input-group-xs">
                <input class="form-control form-control-sm input-name" type="text" placeholder="Nombre del nuevo color">
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary btn-xs continue">Continuar <i class="fas fa-arrow-right"></i></button>
                </div>
                <div class="invalid-feedback">
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="form-row align-items-center color-items">  
`;
            element.enum.forEach(color => {
                template += `
<div class="form-group col-auto color-item">
    <label>Color: <span class="color-name text-lowercase" data-title="Edita el nombre del color" >${color.name}</span></label>
    <div class="input-group input-group-sm mb-3 colorpicker-element input-color-picker">
        <input type="text" class="form-control form-control-sm" value="${color.color}">
        <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-square" style="color: ${color.color};"></i></span>
            <button type="button" class="btn btn-danger btn-xs btn-delete-color"><i class="fas fa-times"></i></button>
        </div>
    </div>
</div>
                `;
            });
            template += `     </div>
</div>`
            break;    
    }
    return template;
}
function getFilterByTemplate(element){
    let filterType = getFilterNameByType(element.type),
    uuid = element.uuid,
    nameFilter = element.name, 
    active = element.active,
    filterTemplate = getFilterTemplateByType(element.type, element);
    return `
    <!-- START FILTER GROUP -->
    <div class="filter-group-container filter-item" uuid="${uuid}" tabindex="0">
        <div class="filter-group-header filter-item-header">
            <div class="filter-info justify-self-start">
                <p><span class="name-filter" uuid="${uuid}">Nombre Filtro: <span data-title="Edita el nombre del Filtro" class="text-capitalize">${nameFilter}</span></span><span class="text-danger">Tipo de Filtro:&nbsp<span class="name-type text-dark">${filterType}</span></span></p>
            </div>
            <div class="filter-end-info justify-self-end">
                <div class="filter-active-section">
                    <span class="font-weight-bold text-secondary">Estatus:</span>
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" class="custom-control-input input-active-filter" name="${nameFilter}-active-filter" id="${nameFilter}-active-filter" ${active === true ? "checked" : ""}>
                        <label class="custom-control-label" for="${nameFilter}-active-filter"><span class="${active === true ? "text-success" : "text-danger"} active-label">${active === true ? "Activo" : "Inactivo"}</span></label>
                    </div>
                </div>
                <div class="divider"></div>
                <button type="button" class="btn btn-danger btn-xs delete"><i class="fas fa-times"></i> Eliminar</button>
            </div>
        </div>
        <div class="filter-group-body tam">
            <div class="filter-config">
                ${filterTemplate}
            </div>
        </div>
        <!-- END FILTER GROUP BODY -->
    </div> 
    <!-- END FILTER GROUP -->`;
}

////////////////FILTER TYPES VALDATIONS

// STRING TYPE VALIDATIONS
$("body").on("input", ".filters-list input.min-lenght,.filters-list input.max-lenght,.filter-number-item-config .input-number-decimals", function(){
    let $this = $(this);
    let value = $this.val().trim();
    let $feedback = $this.siblings(".invalid-feedback");
    if(!isPositiveInteger(value)){
        $feedback.text("El numero necesita ser un numero positivo");
        $this.addClass("is-invalid");
    }else{
        let uuid = getParentElementUuid($this);
        value = parseInt(value);
        if($this.hasClass("min-lenght")){
            let maxValue = getSchemaPropertyByUuid(uuid, "maxLength");
            if(value >= maxValue){
                if(maxValue >= 1){
                    $this.val(maxValue-1);
                    $feedback.text(`El numero no puede ser mayor o igual a ${maxValue}`);
                }else{
                    $this.val(0);
                    $feedback.text(`El numero no puede ser mayor a ${maxValue}`);
                }
                $this.addClass("is-invalid");
                return
            }
            changeElementSchemaPropertyByUuid(uuid, "minLength", value);
        }else if($this.hasClass("max-lenght")){
            let minValue = getSchemaPropertyByUuid(uuid, "minLength");
            if(value <= minValue){
                if(minValue >= 0){
                    $this.val(minValue+1);
                    $feedback.text(`El numero no puede ser menor o igual a ${minValue}`);
                }else{
                    $this.val(0);
                    $feedback.text(`El numero no puede ser menor o igual a ${minValue}`);
                }
                $this.addClass("is-invalid");
                return
            }
            changeElementSchemaPropertyByUuid(uuid, "maxLength", value);
        }else if($this.hasClass("input-number-decimals")){
            changeElementSchemaPropertyByUuid(uuid, "numberOfDecimals", value);
        }
        $this.removeClass("is-invalid");
    }
});

// NUMBER TYPE VALIDATIONS 
$("body").on("change", ".filter-number-item-config .input-is-float", function(){
    let $this = $(this);
    let value = $this.prop('checked');
    let uuid = getParentElementUuid($this);
    let numDec = $this.parent().parent().parent().find(".input-number-decimals");
    if(value == true){
        numDec.removeAttr("disabled");
    }else{
        numDec.attr("disabled", "true");
    }
    changeElementSchemaPropertyByUuid(uuid, "isFloat", value);
});

//COLOR TYPE VALIDATIONS
function setColorPickers(){
    $('.input-color-picker').colorpicker();

    $('.input-color-picker').on('colorpickerChange', function(event) {
        let $this = $(this);
        $this.find('.input-group-append .fa-square').css('color', event.color.toString());
        let uuid = getParentElementUuid($this);
        let colorName = $this.parent().find(".color-name").text();
        changeColorValueByUuidAndName(uuid, colorName, event.color.toString());
    });
    $('.color-name').editable({
        type: 'text',
        title: 'Enter username',
        validate: function(value) {
            let uuid = getParentElementUuid($(this));
            let oldvalue = $(this).data('editable').value;
            value = value.toLowerCase().trim();
            if(value == '') return 'El nombre del color es requerido';
            if(checkIfNameColorExists(value, uuid)) return ` El nombre ${value}. Ya esta siendo usado por otro color.`;
            changeColorNameByUuid(uuid, oldvalue, value);
        }
    });
}
function changeColorValueByUuidAndName(uuid, name, value){
    let colors = getSchemaPropertyByUuid(uuid, "enum");
    colors.forEach(element => {
        if(element.name == name){
            element.color = value;
            return false;
        }
    });
}
function changeColorNameByUuid(uuid, nombreViejo, nombreNuevo){
    let colors = getSchemaPropertyByUuid(uuid, "enum");
    colors.forEach(element => {
        if(element.name == nombreViejo){
            element.name = nombreNuevo;
            return false;
        }
    });
}
function checkIfNameColorExists(nombreNuevo, uuid){
    let colors = getSchemaPropertyByUuid(uuid, "enum");
    let igual = false;
    colors.forEach(element => {
        if(element.name.toLowerCase() == nombreNuevo.toLowerCase()){
            igual = true;
            return true;
        }
    });
    return igual;
};
function addColorPicker(colorItemFilter, name, uuid){
    let colorsContainer = colorItemFilter.find(".color-items");
    let template = `
    <div class="form-group col-auto">
        <label>Color: <span class="color-name" data-title="Edita el nombre del color" >${name}</span></label>
        <div class="input-group input-group-sm mb-3 colorpicker-element input-color-picker">
            <input type="text" class="form-control form-control-sm" value="#000000">
            <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-square" style="color: #000000"></i></span>
                <button type="button" class="btn btn-danger btn-xs delete-color"><i class="fas fa-times"></i></button>
            </div>
        </div>
    </div>`;
    colorsContainer.append(template);
    setColorPickers();
    let colors = getSchemaPropertyByUuid(uuid, "enum");
    colors.push({name: name, color: "#000000"});
}
function removeColorPicker(coloritem, name, uuid){
    let colors = getSchemaPropertyByUuid(uuid, "enum");
    for (let index = 0; index < colors.length; index++) {
        const element = colors[index];
        if(element.name.toLowerCase() == name.toLowerCase()){
            colors.splice(index, 1);
            break;
        }
    }
    $(coloritem).remove();
}
$("body").on("click", ".filter-color-item-config .btn-delete-color", function(){
    let $this = $(this),
    $colorItem = $this.closest(".color-item"),
    name = $colorItem.find(".color-name").text();
    uuid = getParentElementUuid($this);
    removeColorPicker($colorItem, name.toLowerCase(), uuid);
});

// SHARED TYPE FUNCTIONALITIES
$("body").on("click", ".filter-item-config .anadir .btn-agregar", function(){
    let $this = $(this);
    $this.addClass("hidden");
    $this.siblings(".btn-cancel").removeClass("hidden");
    $this.parent().siblings(".anadir-nombre").removeClass("hidden")
});

$("body").on("click", ".filter-item-config .anadir .btn-cancel", function(){
    let $this = $(this);
    $this.addClass("hidden");
    $this.siblings().removeClass("hidden");
    $this.parent().siblings(".anadir-nombre").addClass("hidden")
});

$("body").on("click", ".filter-item-config .anadir-nombre .continue", function(){
    let $this = $(this), $parent = $this.parent();
    let $input = $parent.siblings(".input-name"),
    uuid = getParentElementUuid($this),
    name = $input.val().trim(),
    typeFilter = $this.closest(".filter-item-config"),
    $feedback = $input.siblings(".invalid-feedback");
    if(name == ""){
        $feedback.text("El campo necesita estar lleno");
        $input.addClass("is-invalid");
    }else{
        let error = false;
        if(typeFilter.hasClass("filter-select-item-config") || typeFilter.hasClass("filter-radio-item-config") || typeFilter.hasClass("filter-checkbox-item-config")){
            if(checkIfSelectionOptionExist(name , uuid)){
                $feedback.text("El nombre de la opcion ya esta siendo usado");
                $input.addClass("is-invalid");
                error = true;
            }else{
                addOptionToFilter(typeFilter, name, uuid);
            }
        }else if(typeFilter.hasClass("filter-color-item-config")){
            if(checkIfNameColorExists(name, uuid)){
                $feedback.text("El Nombre del color ya esta siendo usado");
                $input.addClass("is-invalid");
                error = true;
            }else{
                addColorPicker(typeFilter, name, uuid);
            }
        }
        if(error == false){
            $input.removeClass("is-invalid");
            $this.closest(".anadir-nombre").addClass("hidden")
            let btns = $this.closest(".add-element").find(".anadir");
            btns.find(".btn-agregar").removeClass("hidden");
            btns.find(".btn-cancel").addClass("hidden");
        }
    }
});

function setEditableSelectOptions(){
    $('.options-list-items .option-list-item span').editable({
        type: 'text',
        placement: 'top',
        validate: function(value) {
            let uuid = getParentElementUuid($(this));
            let oldvalue = $(this).data('editable').value;
            value = value.toLowerCase().trim();
            if(value == '') return 'El nombre de la opcion es requerido';
            if(checkIfSelectionOptionExist(value, uuid)) return ` El nombre de la opcion ${value}. Ya esta siendo usado.`;
            changeSelectOptionNameByUuid(uuid, oldvalue, value);
        }
    });
}

function checkIfSelectionOptionExist(value , uuid){
    let options = getSchemaPropertyByUuid(uuid, "enum");
    let igual = false;
    options.forEach(element => {
        if(element.toLowerCase() == value.toLowerCase()){
            igual = true;
            return true;
        }
    });
    return igual;
}
function changeSelectOptionNameByUuid(uuid, oldValue, value){
    let options = getSchemaPropertyByUuid(uuid, "enum");
    options.forEach((element, index) => {
        if(element.toLowerCase() == oldValue.toLowerCase()){
            options[index] = value.toLowerCase();
            return false;
        }
    });
}
function addOptionToFilter(optionItemFilter, name, uuid){
    let optionsList = optionItemFilter.find(".options-list-items");
    let template = `<li class="font-weight-bold font-italic text-lowercase option-list-item"><button type="button" class="btn btn-danger btn-xs delete-option"><i class="fas fa-times"></i></button><span class="option-name">${name}</span></li>`;
    optionsList.append(template);
    setEditableSelectOptions();
    let options = getSchemaPropertyByUuid(uuid, "enum");
    options.push(name); 
}
function removeOption(optionList, uuid, name){
    let options = getSchemaPropertyByUuid(uuid, "enum");
    for (let index = 0; index < options.length; index++) {
        const element = options[index];
        if(element.toLowerCase() == name.toLowerCase()){
            options.splice(index, 1);
            break;
        }
    }
    $(optionList).remove();
}
$("body").on("click", ".options-list-items .delete-option", function(){
    let $this = $(this),
    optionList = $this.closest(".option-list-item"),
    name = $this.siblings(".option-name").text(),
    uuid = getParentElementUuid($this);
    removeOption(optionList, uuid, name);
});

//INIT FILTER INSERTIONS
$(document).ready(function(){
    let $inputJson = $(`input[name="filters-config"]`).val();
    if($inputJson == ""){
        filtersConfig = filtersConfig;
    }else{
        filtersConfig = JSON.parse($inputJson);
    }
    insertFilters(filtersConfig.schema);
});

function validarFiltrosNoVacios(){
    let filtersList = $(".filters-builder > .filter-group-container .filter-group-body .filters-list"),
    $listasDeOpciones = filtersList.find(".filter-item .color-items, .filter-item .options-list-items");
    for (let index = 0; index < $listasDeOpciones.length; index++) {
        const element = $listasDeOpciones[index];
        let $element = $(element);
        let tam = $(element).children().length,
        name = getSchemaPropertyById(getFilterIdByUuid(getParentElementUuid($element)), "name");
        if(tam <= 0){
            Swal.fire({
                title: 'Filtro Vacio',
                type: 'error',
                text: `El filtro: ${name} - No tiene ninguna opcion para seleccionar`,
            });
            let parentFilter = getParentFilterItem($element);
            function toggleClassErrorFilter(){
                parentFilter.addClass("error-filter");
                parentFilter.off("focus", toggleClassErrorFilter);
            }
            function removeError(){
                parentFilter.removeClass("error-filter");
                parentFilter.off("click", removeError);
            }
            parentFilter.on("focus", toggleClassErrorFilter);
            parentFilter.on("click", removeError);
            parentFilter.focus();
            // filterItem.off("focusin", toggleClassErrorFilter);
            // filterItem.off("focusout", toggleClassErrorFilter);
            return false;
        }   
    }
    return true;
}

function getFiltersConfiguration(){
    if(!validarFiltrosNoVacios()){
        return false;
    }
    let jsonConfig = JSON.stringify(filtersConfig);
    let $inputFilters = $(`input[name="filters-config"]`);
    if($inputFilters.length <= 0){
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "filters-config").val(jsonConfig);
        $('#form').append(input);
    }else{
        $inputFilters.val(jsonConfig);
    }
    return true;
}
