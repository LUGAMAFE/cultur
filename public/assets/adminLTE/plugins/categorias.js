//Modal Esitar Categoria
$(document).on("click", 'a[href="#modalEditarCategoria"]', function(event) {
    event.preventDefault();
    let $anchor = $(this),
    //nombreCategoria = $anchor.prev(".nombreCategoria").text().trim(),
    idCategoria = $anchor.parent().parent().find("ul").attr("id-categoria");
    let newAnchor = document.createElement('a');
    newAnchor.href = siteURL + `categoria?id=${idCategoria}`;
    newAnchor.click();
});

function scanScrolls() {
    $('.scrollbar-inner').scrollbar();
    //OverlayScrollbars(document.querySelectorAll("scrollbar-inner"), { });
};

$(window).on("load", function(){
    let $loader = $(".overlay"); 
    $loader.toggleClass("d-none");
});

scanScrolls();

$(document).on("dblclick",".contenedorCate .categoria .nombre .handler", function() {
    $(this).parent().parent().prependTo("#despliegueCategorias");
});

function addCategoria() {
    let $addButton =  $(".agregar .add");
    let $inputAdd = $("#nuevaCategoria");
    let color = $addButton.css("color");
    if(color === "rgb(255, 0, 0)"){
        let nombreCategoria = $inputAdd.val().trim();
        if( nombreCategoria === ""){
            let modal = new jBox('Modal', {
                    // width: 300,
                    // height: 100,
                    showCountdown: true,
                    autoClose: 3500,
                    delayOnHover: true,
                    title: 'Error!',
                    content: "El campo categoria esta vacio", 
                    onClose: function() {
                        setTimeout(() => {
                            modal.destroy();
                        }, 500);
                    }
                }).open();
        }else{
            $inputAdd.val("");
            let url = siteURL + "nuevaCategoria";
            let datos = {
                categoria: {
                    nombre: nombreCategoria,
                }
            };

            let success = function(respuesta){               
                if(respuesta.error === true){
                    if(respuesta.respuesta === "ya existe categoria"){
                        let modal = new jBox('Modal', {
                            // width: 300,
                            // height: 100,
                            showCountdown: true,
                            autoClose: 3500,
                            delayOnHover: true,
                            title: 'Error!',
                            content: "La categoria " + nombreCategoria + " ya existe.",  
                            onClose: function() {
                                setTimeout(() => {
                                    modal.destroy();
                                }, 500);
                            }
                        }).open();
                    }else{
                        errorReload();
                    }
                }else{
                    $inputAdd.val("");
                    let template = `
                    <div class="categoria">
                        <div class="nombre">
                            <img class="handler svg" src="assets/img/iconos/move-arrows.svg" alt="">
                            <p class="nombreCategoria">${respuesta.respuesta.nombre_categoria}</p>
                            <a href="#modalEditarCategoria"><img class="options svg" src="assets/img/iconos/menu-bars.svg" alt=""></a>
                        </div>
                        <ul id-categoria="${respuesta.respuesta.id_cat}" class="grupos scrollbar-inner contenedor-grupos"> 
                        </ul>
                    </div>
                    `;
                    $(template).prependTo("#contenedorCate");
                    scanScrolls();
                    searchSVG();
                    scanSortSubcategorias();
                }
            }
            sendPostJsonAjax(url, datos, success, errorReload);
        }
    }
}

$("#nuevaCategoria").on("keyup", function(event){
    if(event.keyCode == 13){
        addCategoria();
    }
});

$(".agregar .add").click(function(){
    addCategoria();
});

let quit = false;
let timeAdd;

$("#nuevaCategoria").on("focus", function(){
    quit = false;
    clearTimeout(timeAdd)
});

$("#agregarCat").on("mouseenter mouseover", function(){
    quit = false;
    $(this).removeClass("unhover");
    $(this).addClass("hover");
    clearTimeout(timeAdd)
});

$("#agregarCat").on("mouseleave", function(){
    quit = true;
    timeAdd = setTimeout(() => {
        if(quit == true){
            $(this).removeClass("hover");
            $(this).addClass("unhover");
        }
    }, 5000);
});

searchSVG();

Sortable.create(document.getElementById("categoriasBasura"), { 
    group: "categoria",
    draggable: ".categoria",
    animation: 150,

    revertOnSpill: true,

    ghostClass: "categoria-ghost",  // Class name for the drop placeholder
    chosenClass: "categoria-chosen",  // Class name for the chosen item
    dragClass: "categoria-drag",  // Class name for the dragging item

    handle: ".handler",

    onAdd: function (evt) {
        let $categoria = $(evt.item),
        $dondeVenia = $(evt.from),
        anteriorIndex = evt.oldIndex; 

        nombreCategoria = idCategoria = $categoria.find(".nombreCategoria").text();
        idCategoria = $categoria.find("ul").attr("id-categoria");

        new jBox('Confirm', {
            content: 'Estas seguro que realmente deseas borrar la categoria ' + nombreCategoria + '?',
            confirmButton: 'Si Borrala!',
            cancelButton: 'No Cancelar',
            confirm: function(){
                let url = siteURL + "eliminarCategoria";
                let datos = {
                    categoria: {
                        nombre: nombreCategoria,
                        id: idCategoria
                    }
                };

                let success = function(respuesta){
                    if(respuesta.error === true){
                        errorReload();
                    }else{
                        let $gruposCategoria = $categoria.find(".grupo");
                        $("#listaGruposPendientes").prepend($gruposCategoria);
                        $categoria.remove();
                        new jBox('Notice', {
                            content: 'Eliminacion de categoria ' + nombreCategoria + ". ¡Completada!", 
                            color: 'red',
                            showCountdown: true,
                            autoClose: 3000,
                            delayOnHover: true,
                            attributes: {y: 'bottom'}});
                    }
                }
                sendPostJsonAjax(url, datos, success, errorReload);
            },
            cancel: function(){
                new jBox('Notice', {
                    content: 'Eliminacion categoria ' + nombreCategoria + ". ¡Cancelada!", 
                    color: 'yellow',
                    showCountdown: true,
                    autoClose: 3000,
                    delayOnHover: true,
                    attributes: {y: 'bottom'}});
                    
                    if(anteriorIndex === 0) {
                        $dondeVenia.prepend($categoria);        
                    }else{
                        $("#"+$dondeVenia.prop('id')+" > div:nth-child(" + (anteriorIndex) + ")").after($categoria);  
                    }
            }
        }).open();
    },    

    onStart: function (evt) {
        $(".contenedorCate, .despliegueCategorias, .contenidoBasura").addClass("droppablePlace");
        $(".contenedorCategoriasBasura").addClass("mostrar");
    },

    onEnd: function (evt) {
        $(".contenedorCate, .despliegueCategorias, .contenidoBasura").removeClass("droppablePlace");
        $(".contenedorCategoriasBasura").removeClass("mostrar");
    }
});

Sortable.create(document.getElementById("contenedorCate"), {    
    group: "categoria",
    draggable: ".categoria",
    animation: 150,

    revertOnSpill: true,

    ghostClass: "categoria-ghost",  // Class name for the drop placeholder
    chosenClass: "categoria-chosen",  // Class name for the chosen item
    dragClass: "categoria-drag",  // Class name for the dragging item

    handle: ".handler",

    onStart: function (evt) {
        $(".contenedorCate, .despliegueCategorias, .contenidoBasura").addClass("droppablePlace");
        $(".contenedorCategoriasBasura").addClass("mostrar");
    },

    onEnd: function (evt) {
        $(".contenedorCate, .despliegueCategorias, .contenidoBasura").removeClass("droppablePlace");
        $(".contenedorCategoriasBasura").removeClass("mostrar");
    }
});

Sortable.create(document.getElementById("despliegueCategorias"), {
    group: "categoria",
    draggable: ".categoria",
    animation: 150,

    revertOnSpill: true,

    ghostClass: "categoria-ghost-despliegueCategorias",  // Class name for the drop placeholder
    chosenClass: "categoria-chosen-despliegueCategorias",  // Class name for the chosen item
    dragClass: "categoria-drag-despliegueCategorias",  // Class name for the dragging item
    
    handle: ".handler",

    onStart: function (evt) {
        $(".contenedorCate, .despliegueCategorias, .contenidoBasura").addClass("droppablePlace");
        $(".contenedorCategoriasBasura").addClass("mostrar");
    },

    onEnd: function (evt) {
        $(".contenedorCate, .despliegueCategorias, .contenidoBasura").removeClass("droppablePlace");
        $(".contenedorCategoriasBasura").removeClass("mostrar");
    }
});

function scanSortSubcategorias() {
    [].forEach.call( $("#listaGruposPendientes"), function (el) {
        Sortable.create(el, {
            group: {
                name: "grupo",
                put: ["grupos-categoria"]
            },
            animation: 200,

            revertOnSpill: true,

            filter: ".categoria",

            onFilter: function (/**Event*/evt) {
                let itemEl = evt.item;  // HTMLElement receiving the `mousedown|tapstart` event.
                console.log(itemEl);
            },

            ghostClass: "categoria-ghost-grupo",  // Class name for the drop placeholder
            chosenClass: "categoria-chosen-grupo",  // Class name for the chosen item
            dragClass: "categoria-drag-grupo",  // Class name for the dragging item

            multiDrag: true,
            selectedClass: 'selected-grupo',


            onStart: function (evt) {
                $(".despliegueCategorias .grupos").addClass("droppableGroup");
                $(".abajo .grupos").addClass("droppablePlace");
            },

            onEnd: function (evt) {
                $(".despliegueCategorias .grupos").removeClass("droppableGroup");
                $(".abajo .grupos").removeClass("droppablePlace");

                manejarDragGrupo(evt);
            }
        });
    });

    [].forEach.call( $(".contenedor-grupos.scroll-content"), function (el) {
        Sortable.create(el, {
            group: {
                name:"grupos-categoria",
                put: ["grupo", "grupos-categoria"]
            },

            animation: 200,

            revertOnSpill: true,

            filter: ".categoria",

            onFilter: function (/**Event*/evt) {
                let itemEl = evt.item;  // HTMLElement receiving the `mousedown|tapstart` event.
                console.log(itemEl);
            },

            ghostClass: "categoria-ghost-grupo",  // Class name for the drop placeholder
            chosenClass: "categoria-chosen-grupo",  // Class name for the chosen item
            dragClass: "categoria-drag-grupo",  // Class name for the dragging item

            multiDrag: true,
            selectedClass: 'selected-grupo',


            onStart: function (evt) {
                $(".despliegueCategorias .grupos").addClass("droppableGroup");
                $(".abajo .grupos").addClass("droppablePlace");
            },

            onEnd: function (evt) {
                $(".despliegueCategorias .grupos").removeClass("droppableGroup");
                $(".abajo .grupos").removeClass("droppablePlace");
                manejarDragGrupo(evt);
            }
        });
    });
}

scanSortSubcategorias();

function manejarDragGrupo(evt){
    let items = evt.items; //dragged HTMLElements if multiple selections.
    let item = evt.item;  // dragged HTMLElement
    let origen = evt.from, destino = evt.to, elementos = [];

    if(Array.isArray(items) && items.length){
        let nombre, id, element;

        for (let cont = 0; cont < items.length; cont++) {
            element = items[cont];
            nombre = $(element).text().trim();
            id =  $(element).attr('id-grupo');
            if(id === undefined || id == "" || nombre === undefined || nombre == ""){
                errorReload();
                continue;
            }
            elementos[cont] = {nombreSubcategoria: nombre, idSubcategoria: id};
        }
    }else{
        let nombre, id;
        nombre = $(item).text().trim();
        id =  $(item).attr('id-grupo');
        if(id === undefined || id == "" || nombre === undefined || nombre == ""){
            errorReload();
        }
        elementos[0] = {nombreSubcategoria: nombre, idSubcategoria: id};
    }

    let accion = -1;    
    /**
     * Accion Valores:
     * -1 = Inicial.
     * 0 = Error Fatal Ninguno de los de abajo.
     * 1 = Lista de Grupo Pendiente a Categoria.
     * 2 = Lista de Grupo Categoria a Lista de grupos pendientes.
     * 3 = Lista de Grupo Pendiente a Lista de Grupo Pendiente.
     * 4 = Lista de Grupo Categoria a Lista de Grupo Categoria (IGUALES!).
     * 5 = Lista de Grupo Categoria a Lista de Grupo Categoria (DIFERENTES!).
     */

    if($(origen).is("#listaGruposPendientes") && $(destino).hasClass('contenedor-grupos')){
        accion = 1;
    }else if($(origen).hasClass('contenedor-grupos') && $(destino).is("#listaGruposPendientes")){
        accion = 2;
    }else if($(origen).is("#listaGruposPendientes") && $(destino).is("#listaGruposPendientes")){
        accion = 3;
    }else if($(origen).hasClass('contenedor-grupos') && $(destino).hasClass('contenedor-grupos')){
        $origenId = $(origen).attr('id-categoria');
        $destinoId = $(destino).attr('id-categoria');
        accion = 5;
        if($origenId == $destinoId){
            accion = 4;
        }
    } else{
        accion = 0;
    }

    //console.log(accion);

    let nombreCategoria;
    let idCategoria;

    const getCategoriaInfo = (categoria) => {
        nombreCategoria = $(categoria).parent().parent().find(".nombreCategoria").text().trim();
        idCategoria = $(categoria).attr("id-categoria");
    }

    const gruposPendientesACategoria = () =>{
        getCategoriaInfo(destino);
        //console.log(nombreCategoria + " " + idCategoria);

        let url = siteURL + "subcategoriasPendientesACategoria";
        let datos = {
            subcategorias: elementos, 
            categoria: {
                nombre: nombreCategoria,
                id: idCategoria
            }
        };

        let success = function(respuesta){
            if(respuesta.error === true){
                errorReload();
            }
        }

        sendPostJsonAjax(url, datos, success, errorReload);
    }

    const grupoCategoriaAgruposPendientes = () => {
        getCategoriaInfo(origen);
        //console.log(nombreCategoria + " " + idCategoria);

        let url = siteURL + "subcategoriaCategoriaAsubcategoriasPendientes";
        let datos = {
            subcategorias: elementos, 
            categoria: {
                nombre: nombreCategoria,
                id: idCategoria
            }
        };

        let success = function(respuesta){
            if(respuesta.error === true){
                errorReload();
            }
        }

        sendPostJsonAjax(url, datos, success, errorReload);
    }

    const grupoCategoriaAgrupoCategoria = () => {
        getCategoriaInfo(origen);
        let nombreCatOrigen = nombreCategoria;
        let idCatOrigen = idCategoria;
        getCategoriaInfo(destino);
        //console.log("Origen: " + nombreCatOrigen + " " + idCatOrigen);
        //console.log("Destino: " + nombreCategoria + " " + idCategoria);

        let url = siteURL + "subcategoriaCategoriaAsubcategoriaCategoria";
        let datos = {
            subcategorias: elementos, 
            categoriaOrigen: {
                nombre: nombreCatOrigen,
                id: idCatOrigen
            },
            categoriaDestino: {
                nombre: nombreCategoria,
                id: idCategoria
            }
        };

        let success = function(respuesta){
            if(respuesta.error === true){
                errorReload();
            }
        }

        sendPostJsonAjax(url, datos, success, errorReload);
    }

    
    switch (accion) {
        case 1:
                gruposPendientesACategoria();
            break;
        case 2:
                grupoCategoriaAgruposPendientes();
            break;    
        case 5:
                grupoCategoriaAgrupoCategoria();
            break;     
        case 4:
        case 3:   
            break;          
        default:
            errorReload();
            break;
    }
}