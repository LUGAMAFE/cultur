let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        getProductosAsoc();
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    multiple: false,
    alertOverride: true,
    filesType: "image",
    sharedParams : {
        idTendencia: idTendencia,
    },
    validation: {
        allowedExtensions: "jpg jpeg png",
    }
});

uploadersManager.addUploadGroup(".upload-area.multiple", {
    multiple: true,
    validation: {
        itemLimit: 100,
        image:{
            minWidth: 500,
            minHeight: 500,
            maxWidth: 3840,
            maxHeight: 1600,
        }
    }
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;
    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta al menos una imagen de ambiente",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});

let timeOutBusqueda;
let lastResults;

const productosSeleccionados = {
    productos: [],
    addProducto: function (id, element) {
        if(this.productos.includes(id)){
            return false;
        }
        this.productos.push(id);
        let template = `
        <div class="producto list-group-item list-group-item-action p-2 px-3 border-success" data-id="${element.id_mcp}">
            <div class="media row align-items-center">
                <div class="col-1">
                    <img src="${element.imagen_producto}" class="img-fluid rounded mx-auto d-block">
                </div>
                <div class="media-body col-11">
                    <div class="d-flex w-100 justify-content-between">
                        <p class="text-muted mb-0"><span class="font-weight-bold">Nombre: </span>${element.nombre_producto}</p>
                        <small class="text-muted"><span class="font-weight-bold">Articulo: </span>${element.id_mcp}</small>
                    </div>
                    <p class="mb-1">${element.desc_mcp}</p>
                    <div class="d-flex w-100 justify-content-between">
                        <div class="d-flex flex-column w-50"> 
                            <small class="text-muted d-block"><span class="font-weight-bold">Categoria: </span>${element.nombre_categoria}</small>
                            <small class="text-muted d-block"><span class="font-weight-bold">Subcategoria: </span>${element.nombre_subcategoria}</small>
                        </div>
                        <div class="d-flex flex-column w-50 justify-content-center align-items-end"> 
                            <button type="button" class="btn btn-danger p-0 rounded-circle delete-product" style="height: 1.7rem;line-height: 1.5rem; width: 1.7rem;"><i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                </div> 
            </div>
        </div>`;
        let lista = $(".lista-productos.asociados-seleccionados");
        lista.append(template);
        return true;
    },
    deleteProducto: function (id) {
        let index = this.productos.indexOf(id);
        if(index <= -1){
            return false;
        }
        this.productos.splice(index, 1);
        let aborrar = $(".lista-productos.asociados-seleccionados").children().eq(index);
        aborrar.remove();
        return true;
    }
};

const isset = function(item){
    let type = typeof item;
    return (item != null && type != "undefined" && type != "null");
}

function initProductosRelacionados() {
    if( isset( productosRelacionados) ){
        productosRelacionados.forEach(element => {
            productosSeleccionados.addProducto(element.id_mcp, element);
        });
    }
}

initProductosRelacionados();

//Busqueda de productos relacionados
$("#productos-asociados-input").on("keyup search", function(){
    clearTimeout(timeOutBusqueda);
    let val = $(this).val().trim();
    if(val.length <= 0){
        $(".lista-productos.asociados-busqueda").html("");
        return;
    }
    let template = `
    <div class="list-group-item list-group-item-action p-2">
        <div class="media row align-items-center">
            <div class="media-body col-12">
                <div class="d-flex w-100 align-items-center justify-content-between">
                    <p class="text-muted mb-0"><span class="font-weight-bold">Buscando...</p>
                    <div class="spinner-border text-secondary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div> 
        </div>
    </div>`;
    $(".lista-productos.asociados-busqueda").html(template);
    searchProduct(val);
});
function searchProduct(val){
timeOutBusqueda = setTimeout(() => {
    var options = {
        shouldSort: true,
        tokenize: true,
        threshold: 0,
        location: 0,
        distance: 100,
        maxPatternLength: 100,
        minMatchCharLength: 1,
        keys: [{
            name: 'id_mcp',
            weight: 0.5
            }, {
            name: 'desc_mcp',
            weight: 0.2
            }, {
            name: 'nombre_producto',
            weight: 0.3
        }]
    };
    let fuse = new Fuse(jsonProductos, options), // "list" is the item array
    result = fuse.search(val),
    utilRedults;
    if(result.length < 10){
        utilRedults = result;
    }else{
        utilRedults = result.slice(0, 8);
    }
    lastResults = utilRedults;
    getTemplateForFilterResults();
}, 0);
}

function getTemplateForFilterResults(){
    template = ``;
    if(lastResults.length <= 0){
        template += `
    <div class="list-group-item list-group-item-action p-2">
        <div class="media row align-items-center">
            <div class="media-body col-12">
                <div class="d-flex w-100 align-items-center justify-content-between">
                    <p class="text-muted mb-0"><span class="font-weight-bold">Ningun Resultado Encontrado.</p>
                </div>
            </div> 
        </div>
    </div>`;
    }else{
        lastResults.forEach(element => {
            let encontrado = productosSeleccionados.productos.filter(function(producto){
                return producto == element.id_mcp;
            });
            if(encontrado.length <= 0){
                template += `
                <div class="producto list-group-item list-group-item-action list-group-item-light p-2 px-3" data-id="${element.id_mcp}">
                    <div class="media row align-items-center">
                        <div class="col-1">
                            <img src="${element.imagen_producto}" class="img-fluid rounded mx-auto d-block">
                        </div>
                        <div class="media-body col-11">
                            <div class="d-flex w-100 justify-content-between">
                                <p class="text-muted mb-0"><span class="font-weight-bold">Nombre: </span>${element.nombre_producto}</p>
                                <small class="text-muted"><span class="font-weight-bold">Articulo: </span>${element.id_mcp}</small>
                            </div>
                            <p class="mb-1">${element.desc_mcp}</p>
                            <div class="d-flex w-100 justify-content-between">
                                <div class="d-flex flex-column w-50"> 
                                    <small class="text-muted d-block"><span class="font-weight-bold">Categoria: </span>${element.nombre_categoria}</small>
                                    <small class="text-muted d-block"><span class="font-weight-bold">Subcategoria: </span>${element.nombre_subcategoria}</small>
                                </div>
                                <div class="d-flex flex-column w-50 justify-content-center align-items-end"> 
                                    <button type="button" class="btn btn-success p-0 rounded-circle add-product" style="height: 1.7rem;line-height: 1.5rem; width: 1.7rem;"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>`;
            }
        });
    }
    $(".lista-productos.asociados-busqueda").html(template);

}

//Añadir Producto a lista
$("body").on("click", function(event){
    let esLista = $(event.target).closest(".lista-productos.asociados-busqueda").length;
    if(!esLista && !$(event.target).is("#productos-asociados-input")){
        $(".lista-productos.asociados-busqueda").html("");
    }
});

$("body").on("click", ".lista-productos.asociados-busqueda .add-product", function(){
    let $this = $(this);
    let id = $this.parents(".producto").attr("data-id");
    let element = lastResults.find(function (product) {
        return product.id_mcp == id;
    });
    if(productosSeleccionados.addProducto(id, element)){
        $this.toggleClass("btn-danger btn-success delete-product add-product");
        $this.children().toggleClass("fa-plus fa-minus");
    }
});

$("body").on("click", ".lista-productos .delete-product", function(){
    let $this = $(this);
    let id = $this.parents(".producto").attr("data-id");
    if(productosSeleccionados.deleteProducto(id)){
        $this.toggleClass("btn-danger btn-success delete-product add-product");
        $this.children().toggleClass("fa-plus fa-minus");
    }
});

function getProductosAsoc(){
    let jsonProductos = JSON.stringify(productosSeleccionados.productos);
    let $inputProductos = $(`input[name="input-productos-asociados"]`);
    if($inputProductos.length <= 0){
        var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "input-productos-asociados").val(jsonProductos);
        $('#form').append(input);
    }else{
        $inputProductos.val(jsonProductos);
    }
    return true;
}