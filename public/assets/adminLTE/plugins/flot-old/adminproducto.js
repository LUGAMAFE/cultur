$('.summer').summernote({
    height: "200",                 // set editor height
    minHeight: 100,             // set minimum height of editor
    toolbar: [
        ['style', ['style', 'clear']],
        ['font', ['bold', 'underline', 'italic']],
        //['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', /*'picture',*/ 'video', 'hr']],
        ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
    ],
    codemirror: { // codemirror options
        theme: 'monokai'
    },
    fontNames: ['Arial', 'Raleway'],
    fontNamesIgnoreCheck: ['Raleway'],
    lang: 'es-ES',
    // onKeyup: function(e) {
    //     $('textarea[name="informacion"]').val(this.code());
    // },
    placeholder: 'Escribe las especificaciones del producto aqui!'
});

$('.note-editable').css('font-family','Raleway');

//FINE UPLOADERS LOGIC
let $loaders = $(".overlay"); 
let uploadersManager = new FineUploadersManager({
    onLoadingFinished: function(){
        $loaders.toggleClass("d-none");
    },
    onAllUploaded: function () {
        $loaders.toggleClass("d-none");
        document.getElementById("form").submit();
    },
    onUploading: function(){
        $loaders.removeClass("d-none");
    },
    onErrorUploading(){
        $loaders.addClass("d-none");
    }
});

uploadersManager.changeInitConfiguration({
    alertOverride: true,
    filesType: "image",
    sharedParams : {
        idProducto: idProducto,
    },
    validation: {
        allowedExtensions: "jpg jpeg png",
        itemLimit: 50,
    }
});

uploadersManager.addUploadGroup(".upload-area.img-productos", {
    validation: {
        image: {
            minWidth: 200,
            minHeight: 200,
        }
    }
});

uploadersManager.addUploadGroup(".upload-area.img-usos", {
    validation: {
        image: {
            minWidth: 400,
            minHeight: 200,
        }
    }
});

uploadersManager.createUploaders();

$('#submit-form').click(function(e) {
    let form = document.getElementById("form");
    let error = checkInformFormValidation(form);
    if(error){
        return;
    }
    let focusElement = null;

    let summerempty = $('.summer').summernote('isEmpty') || $('.summer').summernote('code').trim() === "";

    if(summerempty){
        let closestRequerido = $('.summer').closest(".requerido").tooltip({
            title: "El Campo Esta Incompleto",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    }

    let uploaders = uploadersManager.getUnfilledUploaders();
    uploaders.forEach(element => {
        let closestRequerido = $("#"+element.id).tooltip({
            title: "Falta Alguna Imagen para el producto",
            trigger: "manual",
            placement: "auto"
        });
        closestRequerido.tooltip('show');
        error = true;
        focusElement = closestRequerido;
    });

    if(!error){
        setFilterData();
        uploadersManager.uploadAll();
    }

    $(".requerido").off("focusin", hideToolTips);

    $(focusElement).focus();

    $(".requerido").on("focusin", hideToolTips);
});

if($("#input-producto-descuento").prop("checked")){
    $("input[name='descuento']").show();
    $("input[name='descuento']").prop('required',true);
}

$('#input-producto-descuento').on("change", function(){
    let input = $("#input-producto-descuento").prop("checked");
    if(input) {
        $("input[name='descuento']").slideDown();
        $("input[name='descuento']").prop('required',true);
    }else{
        $("input[name='descuento']").slideUp();
        $("input[name='descuento']").removeAttr('required');
    }
});

function pickTextColorBasedOnBgColorAdvanced(bgColor, lightColor, darkColor) {
    var color = (bgColor.charAt(0) === '#') ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    var uicolors = [r / 255, g / 255, b / 255];
    var c = uicolors.map((col) => {
      if (col <= 0.03928) {
        return col / 12.92;
      }
      return Math.pow((col + 0.055) / 1.055, 2.4);
    });
    var L = (0.2126 * c[0]) + (0.7152 * c[1]) + (0.0722 * c[2]);
    return (L > 0.179) ? darkColor : lightColor;
}

function hasStep(element, decimals){
    var step = "1";
    if(decimals!=null){
        step = "0";
        step += ".";
        while(decimals>1){
            step += "0";
            decimals = decimals - 1;
        }
        step += "1";
    }
    $(`${element}`).attr('step',step);
}

function hasBorder(element, color){
    var borderColor = pickTextColorBasedOnBgColorAdvanced(color, '#FFFFFF', '#000000');
    var elementStyle = $(`${element}`).attr('style');
    if(borderColor == '#000000'){
        $(`${element}`).attr('style',elementStyle + 'border:1px solid; border-color:black;');
    }
}

function getFilterInputValue(element,isLast){
    let filterType = element.attr("filterType");
    let inputFilter = '';
    let result = '';
    switch(filterType){
        case"string":
            inputFilter = element.find("input").val();
            result += '"'+inputFilter+'"';
            break;
        case"number":
            inputFilter = element.find("input").val();
            result += inputFilter;
            break; 
        case"select":
            inputFilter = element.find("select").val();
            result += '"'+inputFilter+'"';
            break;
        case"radio":
            inputFilter = element.find("input:checked").val();
            result += '"'+inputFilter+'"';
            break;
        case"checkbox":
            inputFilter = element.find('.checkbox-options :checkbox:checked');
            let checkboxLength = inputFilter.length;
            result += '[';
            inputFilter.each(function(index){
                if($(this).is(':checked')) {
                    result += '"'+$(this).val()+'"';
                    if (index != (checkboxLength - 1)) {
                        result += ',';
                    }
                }
            });
            result += ']';
            break;
        case"color":
            inputFilter = element.find("input:checked").val();
            result += '"'+inputFilter+'"';
            break;
    }
    if(!isLast){
        result += ',';
    }
    return result;
}

function setFilterData() {
    var filtros = $('.filter-item');
    var length = filtros.length;
    var filtrosResultJson = "{";
    filtros.each(function(index){
        filtrosResultJson += '"'+$(this).attr("id")+'":';
        if (index === (length - 1)) {
            filtrosResultJson += getFilterInputValue($(this),true);
        } else {
            filtrosResultJson += getFilterInputValue($(this),false);
        }
    });
    filtrosResultJson += "}";
    $('#datos-filtro').val(filtrosResultJson);
};

