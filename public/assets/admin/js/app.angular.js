var myApp = angular.module('app', ['app.controllers']);

myApp.config(['$interpolateProvider', '$httpProvider',function ($interpolateProvider, $httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.cache=true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    
}]);