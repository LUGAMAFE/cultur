angular.module('app.controllers').factory('services', ['$http', function($http) { 

	var urlbase ='';
    var urlPrefix = "/ordenes/";
    var consultHttp = {};

    /*
		Consiltar todos los assets
    */
    consultHttp.getProveedores = function() {
        return $http.get(urlPrefix + 'get_proveedores/', { cache: false });
    };

    consultHttp.getService = function($id) {
        return $http.get(urlPrefix + 'get_servicios/'+$id, { cache: false });
    };

    consultHttp.addService = function($data) {
        return $http.post(urlPrefix + 'add_servicio/', {data: $data}, { cache: false });
    };

    consultHttp.editService = function($data) {
        return $http.post(urlPrefix + 'edit_servicio/', {data: $data}, { cache: false });
    };

    consultHttp.deleteService = function($id) {
        return $http.post(urlPrefix + 'delete_sercicio/'+$id, { cache: false });
    };

    return consultHttp;
}]);
