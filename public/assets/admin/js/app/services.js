angular.module('app.controllers').factory('services', ['$http', function($http) { 

	var urlbase ='';
    var urlPrefix = "/ordenes/";
    var consultHttp = {};

    /*
		Consiltar todos los assets
    */
    consultHttp.getFotos = function($orden) {
        return $http.get(urlPrefix + 'json_fotos/'+$orden, { cache: false });
    };

    return consultHttp;
}]);
