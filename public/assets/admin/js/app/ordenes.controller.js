var ctrl = angular.module('app.controllers', []);
ctrl.directive('stringToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(value) {
				return '' + value;
			});
			ngModel.$formatters.push(function(value) {
				return parseFloat(value, 10);
			});
		}
	};
});
ctrl.controller('ordenesServicio', ['$scope', '$filter', '$element', 'services', function ($scope, $filter, $element, services) {
	$scope.proveedores = [];
	$scope.varServicio = {};
	$scope.servicios = [];
	$scope.editarProveedor = [];
	$scope.costo_publico = 0;
	$scope.costo_total = 0;
	$scope.cantidad_servicio = 0;
	$scope.reset = {};

	services.getProveedores().success(function(response) {
		$scope.proveedores = [];
		$scope.proveedores.push.apply($scope.proveedores, response);
		//console.log($scope.proveedores);
	});

	$scope.reset = function() {
		$scope.varServicio = angular.copy($scope.reset);
	};

	$scope.setTotals = function(item){
        if (item){
        	$scope.cantidad_servicio += parseFloat(item.cantidad_servicio);
        	$scope.costo_publico += parseFloat(item.costo_publico);
            $scope.costo_total += parseFloat(item.costo_total);
        }
    }

	$scope.agregarServicio = function() {
		$scope.varServicio.idRel_orden = angular.element('#idOrden').val();
		console.log($scope.varServicio);
		services.addService($scope.varServicio).success(function(response) {
			$scope.reset();
			loadService();
		});
	}

	$scope.editServicio = function($index) {
		console.log($scope.servicios[$index]);
		services.editService($scope.servicios[$index]).success(function(response) {
			loadService();
		});
	}

	$scope.eliminarServicio = function($id) {
		services.deleteService($id).success(function(response) {
			loadService();
		});
	}

	angular.element(document).ready(function() {
		loadService();
	});

	function loadService(){
		services.getService(angular.element('#idOrden').val()).success(function(response){
			$scope.cantidad_servicio = 0;
        	$scope.costo_publico = 0;
            $scope.costo_total = 0;
			$scope.servicios = [];
			$scope.servicios.push.apply($scope.servicios, response);
			console.log($scope.servicios);
		});
	}
}]);